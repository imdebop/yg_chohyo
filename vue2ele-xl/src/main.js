import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import db from './renderer/datastore';
console.log(db)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

Vue.prototype.$db = db;

console.log('************ aaa ************')
import dbData from '@/components/db/namelist.json'
//this.prototype.$db.insert(dbData)
db.insert(dbData)
console.log('database created?')