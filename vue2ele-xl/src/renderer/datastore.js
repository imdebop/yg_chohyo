import path from 'path';
import { remote } from 'electron';
import Datastore from 'nedb';

const dbPath = path.join(remote.app.getPath('userData'), '/data.db');
console.log(dbPath)

// DB初期化
export default new Datastore({
  autoload: true,
  filename: dbPath,
});
