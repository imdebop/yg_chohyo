"use strict";
 
const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
let mainWindow;
 
// 全てのウィンドウが閉じたら終了
app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
    app.quit();
  }
});
// 実行時イベント
app.on('ready', function() {
  // ウィンドウサイズ
  mainWindow = new BrowserWindow({width: 800, height: 600});
  //初期ウィンドウファイル
  mainWindow.loadURL('file://' + __dirname + '/index.html');
  // 終了イベント
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});
