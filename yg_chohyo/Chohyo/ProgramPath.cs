﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace yg_chohyo.Chohyo
{
    public static class ProgramPath
    {
        static string exPath { get; set; } = "";
        static Dictionary<string, string> dicPaths = new Dictionary<string, string>();

        public static string GyomuDb(){
            return dicPaths[ "gyomuDb" ];
        }
        
        public static string Excel()
        {
            return dicPaths[ "excel"];
        }
        
        public static string MainData()
        {
            return dicPaths[ "mainData"];
        }

        public static string Shoyu()
        {
            return dicPaths[ "shoyu"];
        }

        public static void SetValues()
        {
            foreach(string rec in File.ReadAllLines("exPath.txt")){
                string[] ss = rec.Split("=");
                if( ss[0] != "")
                {
                    dicPaths[ss[0]] = ss[1];
                }
                //if(ss[0] == "excel")
                //{
                //    exPath = ss[1];
                //}
            }
            //System.Windows.MessageBox.Show("initialize program paths.");
        }

    }
}
