﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Linq;
using yg_chohyo.Data;
using yg_chohyo.Repo;
using System.Windows.Input;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using yg_chohyo.panels;

namespace yg_chohyo.Chohyo.KanchiShomei
{
    class ChohyoRows
    {
        public readonly int ZengoStartRow;
        public readonly int SokochiStartRow;
        public readonly int HizukeRow = 37;
        public readonly int HizueCol = 4;
        public static bool MoreThanFive;
        
        public ChohyoRows(bool moreThanFive)
        {
            if (moreThanFive)
            {
                ZengoStartRow = 50;
                SokochiStartRow = 80;
                MoreThanFive = true;
            }
            else
            {
                ZengoStartRow = 21;
                SokochiStartRow = 29;
                MoreThanFive = false;
            }
        }
    }


    public class KanchiShomei : ExIO
    {
        private static ExIO ex;
        private static ChohyoRows chohyoRows;

        public static void Create(string[] kumiCodes, double date)
        {
            //組合せレコードのセット
            List<Repo.RepoKumi> repoKumis = new List<Repo.RepoKumi>();

            string fKanchiName = "";
            int numlines = KumiRecsGet(kumiCodes, repoKumis);
            if (numlines > 20 || repoKumis.Count() > 5)
            {
                MessageBox.Show("換地または底地の行数がオーバーしています。\n下方に表示されたデータを\n編集してください。");
                bool moreThanFive = true;
                chohyoRows = new ChohyoRows(moreThanFive);
                fKanchiName = "c:/yagyu_settings/仮換地証明_別紙あり.xls";
            }
            else if(numlines > 5){
                bool moreThanFive = true;
                chohyoRows = new ChohyoRows(moreThanFive);
                if(repoKumis.Count() > 2)
                {
                    fKanchiName = "c:/yagyu_settings/仮換地証明_別紙あり.xls";
                }
                else
                {
                    fKanchiName = "c:/yagyu_settings/仮換地証明_別紙あり_2.xls";
                }
            }
            else
            {
                fKanchiName =  "c:/yagyu_settings/仮換地証明願（ベース）.xls";
                chohyoRows = new ChohyoRows(false);//5行以下
            }

            ex = new ExIO(fKanchiName);
            EditChohyo(repoKumis, date);

            //出力ファイル名の設定
            string shoCode = repoKumis.First().KumiRec.ShoCode;
            string shimei = Data.CodeTables.DicShoName[shoCode].Replace("　","");
            string f = $"c:/yagyu_chohyo/{shoCode}{shimei}.xls";

            ex.Write(f);

            Debug.WriteLine(ProgramPath.Excel());
            var app = new ProcessStartInfo();
            //app.FileName = @"C:\Program Files (x86)\Microsoft Office\root\Office16\EXCEL.EXE";
            //app.FileName = "Excel.exe";
            app.FileName = ProgramPath.Excel();
            app.Arguments = f.Replace("/",@"\");
            app.UseShellExecute = true;
            Process.Start(app);

        }



        //
        //組合せデータを個数分まとめて取得
        protected static int KumiRecsGet(string[] kumiCodes, List<Repo.RepoKumi> repoKumis)
        {
            int numLines = 0;
            foreach (string kumiKey in kumiCodes)
            {

                repoKumis.Add(Repo.RepoKumiSet.GetOneRec(kumiKey));
                //行数の計算。　前後どちらかは１件なので、１を引く
                numLines += repoKumis.Last().JuzenRecs.Count + repoKumis.Last().KanchiRecs.Count - 1;
            }
            return numLines;
        }

        //
        //帳票の編集
        private static void EditChohyo(List<Repo.RepoKumi> repoKumis, double date)
        {
            int currentRow = chohyoRows.ZengoStartRow;
            ////////////////////////////////////////////////
            //組合せデータ１件毎に処理 /////////////////////
            foreach (Repo.RepoKumi repoKumi in repoKumis)
            {
                //
                //換地組合せの種類で分岐
                //
                int cntJ = repoKumi.JuzenRecs.Count;
                int cntK = repoKumi.KanchiRecs.Count;
                if (cntJ > 1)
                {
                    EditNto1(repoKumi, currentRow, cntJ);
                    currentRow += cntJ;
                } else if (cntK > 1)
                {
                    Edit1toN(repoKumi, currentRow, cntK);
                    currentRow += cntK;
                }
                else
                {
                    EditOne2one(repoKumi, currentRow);
                    currentRow++;
                }

            }
            SokochiEdit.Set(ex, repoKumis, chohyoRows);

            //日付の編集
            ex.SetCellDoubule(date, chohyoRows.HizukeRow, chohyoRows.HizueCol);


        }

        //private static readonly int ZengoStartRow = 21;
        private static readonly int JuzenCol = 1;
        private static readonly int KanchiCol = 7;
        private static readonly int NameCol = 10;

        /// <summary>
        /// 従前地の各項目のセル幅
        /// </summary>
        private static readonly int[] juzenWidths = new int[] { 2, 1, 1, 1, 1 };
        private static readonly int[] kanchiWidths = new int[] { 1, 1, 1, 1, 1 };
        /////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 多対１の換地編集
        /// </summary>
        private static void EditNto1(Repo.RepoKumi repoKumi, int currentRow, int rows)
        {
            Repo.KumiStack kumiStack = Repo.KumiStackEdit.Get(repoKumi);
            int currentCol = KanchiCol;
            int lastRow = currentRow + rows - 1;

            //町字名の欄をマージ、従前地項目編集
            for (int i = currentRow; i <= lastRow; i++)
            {
                Merge(i, i, JuzenCol, JuzenCol + 1);
                Stack<string> jRec = kumiStack.Juzens.Pop();
                SetJuzenRow(jRec, i);
                
            }
            //換地の欄をマージ
            foreach (int w in kanchiWidths)
            {
                int firstCol = currentCol;
                int lastCol = currentCol + w - 1;
                Merge(currentRow, lastRow, firstCol, lastCol);
                currentCol += w;
            }
            Stack<string> kRec = kumiStack.Kanchis.Pop();
            SetKanchiRow(kRec, currentRow);
            //所有者の編集
            SetShoyuCol(repoKumi, currentRow);
        }



        //////従前地を証明書に書き込み
        private static void SetJuzenRow(Stack<string> jRec, int rowIdx)
        {
            ex.SetCellStr(jRec.Pop().Replace("字","\n字"), rowIdx, JuzenCol); //町字
            ex.SetCellStr(jRec.Pop(), rowIdx, JuzenCol + 2); //地番
            ex.SetCellStr(jRec.Pop(), rowIdx, JuzenCol + 3); //地目
            ex.SetCellStr(jRec.Pop(), rowIdx, JuzenCol + 4); //地積　整数
            ex.SetCellStr(jRec.Pop(), rowIdx, JuzenCol + 5); //地積　小数

        }
        //////換地の書き込み
        private static void SetKanchiRow(Stack<string> kRec, int rowIdx)
        {
            ex.SetCellStr(kRec.Pop(), rowIdx, KanchiCol); //ブロック
            ex.SetCellStr(kRec.Pop(), rowIdx, KanchiCol + 1); //ロット
            ex.SetCellDoubule(double.Parse(kRec.Pop()), rowIdx, KanchiCol + 2); //面積
        }
        //////所有者の書き込み
        private static void SetShoyuCol(RepoKumi repoKumi, int rowIdx)
        {
            string shoCode = repoKumi.KumiRec.ShoCode;
            string name = "";
            if (Data.Util.Shoyu.IsKyoyu(shoCode))
            {
                List<string> names = new RepoKyoyu(shoCode).GetNames();
                if(names.Count > 2)
                {
                    MessageBox.Show($"共有者は {names.Count.ToString()}名です。隠れたデータがないか確認してください。");
                }
                foreach(string n in names)
                {
                    name += $"\n{n.Replace("　", "")}";
                }
                name = name.TrimStart('\n');
            }
            else
            {
                name = Dapper.DapperShoyu.GetShoyu(shoCode).TokiName;
            } 
            ex.SetCellStr(name, rowIdx, NameCol);
        }

        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// １対多の換地編集
        /// </summary>
        private static readonly int shoyuCol = 10;
        private static void Edit1toN(Repo.RepoKumi repoKumi, int currentRow, int rows)
        {
            Repo.KumiStack kumiStack = Repo.KumiStackEdit.Get(repoKumi);
            int currentCol = JuzenCol;
            int lastRow = currentRow + rows - 1;
            //
            //従前地の欄をマージ、従前地項目編集
            foreach (int w in juzenWidths)
            {
                int firstCol = currentCol;
                int lastCol = currentCol + w - 1;
                Merge(currentRow, lastRow, firstCol, lastCol);

                currentCol += w;
            }
            Stack<string> jRec = kumiStack.Juzens.Pop();
            SetJuzenRow(jRec, currentRow);

            //所有者,備考の欄をマージ
            Merge(currentRow, lastRow, shoyuCol, shoyuCol);
            Merge(currentRow, lastRow, shoyuCol + 1, shoyuCol + 1);
            SetShoyuCol(repoKumi, currentRow);

            //換地を編集
            for (int i = currentRow; i <= lastRow; i++)
            {
                Stack<string> kRec = kumiStack.Kanchis.Pop();
                SetKanchiRow(kRec, i);
            }

        }

        private static void Merge(int firstRow, int lastRow, int firstCol, int lastCol)
        {
            var cra = Cra(firstRow, lastRow, firstCol, lastCol);
            ex.Mergecells(cra);
        }

        /// <summary>
        /// １対１の換地の編集
        /// </summary>
        /// <param name="repoKumi"></param>
        /// <param name="currentRow"></param>
        private static void EditOne2one(Repo.RepoKumi repoKumi, int currentRow)
        {
            Merge(currentRow, currentRow, JuzenCol, JuzenCol + 1);

            Repo.KumiStack kumiStack = Repo.KumiStackEdit.Get(repoKumi);
            Stack<string> jRec = kumiStack.Juzens.Pop();
            SetJuzenRow(jRec, currentRow);
            Stack<string> kRec = kumiStack.Kanchis.Pop();
            SetKanchiRow(kRec, currentRow);
            SetShoyuCol(repoKumi, currentRow);

        }

    }

    class SokochiEdit
    {
        private static readonly int SokochiStartCol = 1;

        public static void Set(ExIO ex, List<Repo.RepoKumi> repoKumis, ChohyoRows chohyoRows)
        {
            List<SokochiRec> srList = new List<SokochiRec>();

            foreach(RepoKumi r in repoKumis )
            {
                //換地ごとに底地を編集
                List<Kanchi> kList = r.KanchiRecs;
                foreach (Kanchi k in kList) { 
                    srList.Add(CreateSokochi.Get(k));
                }
                //エクセルにを編集
            }  
            int row = chohyoRows.SokochiStartRow;
            foreach(SokochiRec sr in srList)
            {
                ex.SetCellStr(sr.Block, row, SokochiStartCol);
                ex.SetCellStr(sr.Lot, row, SokochiStartCol + 1);
                ex.SetCellStr("豊橋市" + sr.Str, row, SokochiStartCol + 2);
                row++;
            }
        }

    }



    class ChoshoKeisen
    {

        public static void KeisenJuzen(int[] rowNums)
        {
        }


        public static void One2One(int row) {
            var strs =  new string[] { "AA", "AA", "AA", "AA", "TRa", "TLa","AA","AA", "AA", "AA","AA"};
        }

        public static void KJOne(int row)
        {
            var strs =  new string[] { "AA", "AA", "AA", "AA", "TRa", "TLa"};

        }

        public static void KJTop(int row)
        {

        }

        public static void KJMiddle(int row)
        {

        }

        public static void KJBottom(int row)
        {

        }

        public static void KKTop(int row)
        {

        }

        public static void KKMiddle(int row)
        {

        }

        public static void KKBottom(int row)
        {

        }
    }



}
