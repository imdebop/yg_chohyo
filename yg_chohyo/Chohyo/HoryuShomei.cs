﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Security.RightsManagement;

namespace yg_chohyo.Chohyo
{
    public class HoryuShomei
    {
        public static readonly int HoryuRecRow = 25;
        public static readonly int HizukeRow = 39;
        public static readonly int HizukeCol = 6;
        public static readonly int ColBlk = 6;
        public static readonly int ColLot = 8;
        public static readonly int ColMen = 10;
        //public readonly int RowJusho = 26;
        //public readonly int RowShimei = 27;
        public static readonly int SokochiRow = 32;

        private static ExIO ex;

        public static void Create(string kanchiCD, double date)
        {

            ex = new ExIO("c:/yagyu_settings/保留地証明ベース.xls");

            Repo.RepoKanchi rpKan =  Repo.RepoKanchiEdit.Get(kanchiCD);

            //保留地の編集
            ex.SetCellStr(rpKan.Block, HoryuRecRow, ColBlk);
            ex.SetCellStr(rpKan.Lot, HoryuRecRow, ColLot);
            ex.SetCellDoubule(double.Parse(rpKan.Chiseki), HoryuRecRow, ColMen);

            //底地の編集
            Repo.SokochiRec sr = Repo.CreateSokochi.Get(rpKan.Kanchi);
            ex.SetCellStr(sr.Str, SokochiRow, ColBlk);
            //日付の編集
            ex.SetCellDoubule(date, HizukeRow, HizukeCol);

            string b_l = "保留地証明" + rpKan.BlockLotZeroPad() + ".xls";
            string of = $"c:/yagyu_chohyo/{b_l}";
            ex.Write(of);

            var app = new ProcessStartInfo();
            //app.FileName = "Excel.exe";
            app.FileName = ProgramPath.Excel();
            app.Arguments = of.Replace("/", @"\");
            app.UseShellExecute = true;
            Process.Start(app);

            //ex = new 
        }



    }
}
