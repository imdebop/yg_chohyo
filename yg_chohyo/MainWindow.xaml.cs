﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.IO;
using yg_chohyo.panels;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using NPOI.OpenXmlFormats.Dml;

namespace yg_chohyo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public PopCalendar PopCalendar { get; private set; }

        public MainWindow()
        {
            Chohyo.ProgramPath.SetValues();
            var kana = new KanaTable();

            System.Diagnostics.Debug.WriteLine(Directory.GetCurrentDirectory());

            InitializeComponent();
            //buttonShomei.PreviewMouseDown += new MouseButtonEventHandler(buttonShomei_Clicked);
            //gridShoIndex.SelectedCellsChanged += new SelectedCellsChangedEventHandler(ShoIndex_SelectionChanged);
            gridKanIndex.SelectedCellsChanged += new SelectedCellsChangedEventHandler(KanIndex_SelectionChanged);
            
            //所有者インデックスのかな一覧を作成
            this.gridShoIndex.ItemsSource = kana.data;
            //this.ownerList.Visibility = Visibility.Hidden;

            //仮換地インデックスのブロック番号一覧を作成
            gridKanIndex.ItemsSource = new KanchiIndex().KanIndex;

            //従前地インデックスを作成
            List<string> choazaList = Data.CodeTables.DicChoaza.OrderBy(x => x.Key).Select(x => $"{x.Key}:{x.Value}").ToList();
            juzenIndex.ItemsSource = choazaList;

            //日付ピッカーを初期化
            PopCalendar = new PopCalendar();

        }

        public ObservableCollection<KanRec> kanRecs = new ObservableCollection<KanRec>();


        private static bool eventFlg = true;
        public static bool eventFlipFlop()
        {   //cancelling one of dupulicated events.
            return (!(eventFlg = !eventFlg));
        }

        /*
        private void buttonShomei_Clicked(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("button clicked");
            Debug.WriteLine($"***{((FrameworkElement)e.Source).GetType()}***");

            zengoList.KanchiShomei(sender, e);
        }
        */

        private void ShoIndex_SelectionChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            //if (eventFlipFlop()) { return; }

            DataGridCellInfo cell = gridShoIndex.SelectedCells[0];
            string kana = ((cell.Column.GetCellContent(cell.Item)) as TextBlock).Text;

            Debug.WriteLine($"value=[{kana}]");

            var obj = Dapper.DapperShoyu.GetByKana(kana);

            foreach(Data.Shoyu x in  obj)
            {
                Debug.WriteLine(x.TokiName);
            }
            ownerList.owners.Visibility = Visibility.Visible;
            ownerList.owners.ItemsSource = obj;
        
            ListTabs.SelectedIndex = 0;
        }

        /*
        public static void SetZengoList(string shoCode)
        {
               
            //ZenGoList.EditZengoList(out listZenGo, shoCode);
        }
        */

        private void KanIndex_SelectionChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            DataGridCellInfo cell = gridKanIndex.SelectedCells[0];
            string block = ((cell.Column.GetCellContent(cell.Item)) as TextBlock).Text;
            if (eventFlipFlop()) { return; }
            Debug.WriteLine($"Block={block}");
            ListTabs.SelectedIndex = 2;
            lvKanchiHdr.Header = "換地一覧";
            this.kanchiList.SetList(block.Replace(' ', '0'));
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = ((TextBox)sender).Text;
                indexShocode.Text = "";
                Debug.WriteLine(str);
                if (Data.Util.IsNum(str))
                {
                    zengoList.SetList(str);
                    ListTabs.SelectedIndex = 1;
                }
                else
                {
                    var obj = Dapper.DapperShoyu.SearchByName(str);
                    ownerList.owners.ItemsSource = obj;
                    ListTabs.SelectedIndex = 0;
                }
            }
        }

        private void juzenIndex_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string choazaCD = (sender as ListView).SelectedValue.ToString().Split(":")[0]; 
            //string choazaCD = (idx + 1).ToString("00");
            lvKanchiHdr.Header = "従前地一覧";
            ListTabs.SelectedIndex = 2;
            this.kanchiList.SetJuzenList(choazaCD);
        }

        private void MyWindow_Closed(object sender, EventArgs e)
        {
            PopCalendar.Close();
        }
    }
}
