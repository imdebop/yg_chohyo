﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yg_chohyo.Data
{
    public static class Reigai
    {
        public static void JuzenShozai(ref Data.Util.Shozai shozai)
        {
            switch (shozai.FudeCode)
            {
                case "10043001"://松崎
                    shozai.Eda = "1-1";
                    break;
                case "11037000"://松島
                    shozai.Moto = "37・38合併";
                    break;
            }
        }
    }
}
