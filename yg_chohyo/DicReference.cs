﻿using System;
using System.Collections.Generic;
using System.Text;
using yg_chohyo.Data;

namespace yg_chohyo
{
    class DicReference
    {
        public class ShoyuInfo
        {
            public string shoCode { get; set; }
            public string shimei { get; set; }
        }

        /// <summary>
        ///[換地コード] -> [所有者コード、氏名]
        /// </summary>
        public static ShoyuInfo GetShoInfoByKancode(string kancode)
        {
            ShoyuInfo si = new ShoyuInfo();
            si.shoCode = DicKan2sho[kancode];
            si.shimei = CodeTables.DicShoName[si.shoCode];

            return si;
        }



        public static Dictionary<string, string> DicKan2sho = CodeTables.DicKumiKan2sho;

        public static Dictionary<string, string[]> DicKan2fcodes = new Dictionary<string, string[]>();

        public static Dictionary<string, Kanchi> DicKanchi = new Dictionary<string, Kanchi>();

        public DicReference()
        {
        }
    }


}
