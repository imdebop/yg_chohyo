﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace yg_chohyo
{
    public class TukehoDwg
    {

        private Dictionary<string, string> tuke2kanDic = new Dictionary<string, string > ();

        public TukehoDwg()
        {
            string[] tukeText = File.ReadAllLines(Data.PathInfo.GetPath("dwgTukeho.txt"));

            foreach(string r in tukeText){
                string[] wk = r.Split(':');
                string tukeCode = wk[0];
                string kanCode = wk[2];
                tuke2kanDic[tukeCode] = kanCode;

            }

        }

        public string getTukeKancode(string tukehoCode)
        {
            if (tuke2kanDic.ContainsKey(tukehoCode))
            {
                return tuke2kanDic[tukehoCode];
            }
            else
            {
                return "";
            }
        }
    }
} 
