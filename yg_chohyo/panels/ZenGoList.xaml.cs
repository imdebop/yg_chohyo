﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Linq;

namespace yg_chohyo.panels
{
    /// <summary>
    /// ZenGoList.xaml の相互作用ロジック
    /// </summary>
    public partial class ZenGoList : UserControl
    {
        public ZenGoList()
        {
            InitializeComponent();

            //前後リストの項目が選択されたイベント処理
            lvZenGoList.SelectionChanged += LvZenGoList_SelectionChanged;
            lvZenGoList.PreviewMouseUp += new MouseButtonEventHandler(btnKanchiShomei_MouseUp);
        }


        private void LvZenGoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MainWindow.eventFlipFlop()) { return; }
            if (e.AddedItems.Count == 0) { return; }

            ListView lv = sender as ListView;
            int idx = lv.Items.IndexOf(e.AddedItems[0]);
            Recs[idx].Checked = Recs[idx].Checked == "" ? "〇" : "" ;
            lvZenGoList.Items.Refresh();
            lvZenGoList.SelectedIndex = -1;
            Debug.WriteLine($"index={idx}");
            Debug.WriteLine(Recs[idx].KumiCD);

            
        }

        public ObservableCollection<Repo.ZenGoRec> Recs { get; set; }

        /// <summary>
        /// 前後リストで保留地を表示
        /// </summary>
        public void SetHoryuList(string shoCode, string kanchiCode)
        {
            Repo.RepoKanchi repoKanchi = Repo.RepoKanchiEdit.Get(kanchiCode);
            Repo.ZenGoRec r = new Repo.ZenGoRec();
            r.Go = repoKanchi.HyojiRec();
            r.KumiCD = "H" + kanchiCode;
            r.Zen = "保留地";
            r.Checked = "〇";
            Recs = new ObservableCollection<Repo.ZenGoRec>() { r };
            lvZenGoList.DataContext = Recs;
            //下欄の表示データをセット
            //List<string> shoyuDtl = ShoyuDtl(shoCode);
            List<string> shoyuDtl = (new string[] { "保留地" }).ToList();
            shoyuInfo.ItemsSource = shoyuDtl;

        }


        public void SetList(string shoCode)
        {
            try { //shoCodeの存在チェック
                string test = Data.CodeTables.DicShoName[shoCode];
            }
            catch
            {
                MessageBox.Show($"所有者コード:{shoCode} は存在しません");
                return;
            }
            //
            //前後リストの編集
            Recs = FillZengoList.TestZengoList(shoCode);
            lvZenGoList.DataContext = Recs;
            //lvZenGoList.DataContext = FillZengoList.TestZengoList(shoCode);
            //下欄の表示データをセット
            List<string> shoyuDtl = ShoyuDtl(shoCode);
            shoyuInfo.ItemsSource = shoyuDtl;
        }
        /// <summary>
        ///下欄の所有者・共有者情報を編集
        /// </summary>
        private List<string> ShoyuDtl(string shoCode)
        {
            List<string> sList = new List<string>();
                string name = Data.CodeTables.DicShoName[shoCode];
                sList.Add( $"{shoCode}  {name}");
            if (Data.Util.Shoyu.IsKyoyu(shoCode))
            {
                List<string> hyojiRecs = new Repo.RepoKyoyu(shoCode).GetHyojiRecs();
                foreach(string s in hyojiRecs)
                {
                    sList.Add(s);

                }
            }
            else
            {
                Data.Shoyu sho = Dapper.DapperShoyu.GetShoyu(shoCode);
                string jusho = sho.TokiJusho;
                sList.Add(jusho);
            }
            return sList;
        }
        

        /// <summary>
        /// ボタンクリックで換地・保留地証明を作成
        /// </summary>
        private void btnKanchiShomei_MouseUp(object sender, MouseButtonEventArgs e)
        {
            string who = sender.GetType().ToString().Split(".").Last();
            if(who == "Button")
            {
                Debug.WriteLine((sender as Button).Name);
            }
            else
            {
                return;
            }
            Debug.WriteLine("button kanchishomei clicked.");
            IEnumerable<Repo.ZenGoRec> wkRec = Recs;
            var kumiCDs = wkRec.Where(r => r.Checked == "〇").Select(r => r.KumiCD);
            if(kumiCDs.Count() == 0) {
                MessageBox.Show("項目が選択されていません。");
                return;
            }

            //日付をカレンダーから入力 20201012 
            var calDialog = (Application.Current.MainWindow as MainWindow).PopCalendar;
            calDialog.ShowDialog();
            double date = DateTime.Parse(calDialog.datePicker.Text).ToOADate();


            if (kumiCDs.First().Substring(0,1) == "H" )
            {
                string kanCode = kumiCDs.First().Substring(1);
                Chohyo.HoryuShomei.Create(kanCode, date);
            }
            else
            {
                //エクセルの換地証明書を作成
                Debug.WriteLine(string.Join(':', kumiCDs ));
                Chohyo.KanchiShomei.KanchiShomei.Create(kumiCDs.ToArray(), date);
            }


            //Process.Start(@"C:\Program Files(x86)\Microsoft Office\root\Office16\EXCEL.EXE", @"c:\yagyu_chohyo\aaa.xls");
        }
    }
///////////////////////////////////////////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////////////////
    public class FillZengoList
    {
        public  static ObservableCollection<Repo.ZenGoRec> TestZengoList (string shoCode)
        {
            var recs = new ObservableCollection<Repo.ZenGoRec>();
            //var obj = Dapper.DapperKumi.GetByShocode(shoCode);
            var obj = Repo.RepoKumiSet.Get(shoCode);
            foreach(Repo.RepoKumi x in obj)
            {
                recs.Add(RecEdit(x));
            }
            return recs;
        }

        private static Repo.ZenGoRec RecEdit(Repo.RepoKumi x)
        {
            var rec = new Repo.ZenGoRec();
            string[] fcodes = x.KumiRec.Fcodes.Split("/");

            //前後リストの各レコードを編集
            string[] kcodes = x.KumiRec.Kanchis.Split("/");
            List<string> shozais = new List<string>();
            List<string> kanchis = new List<string>();
            foreach(string fcode in fcodes)
            {
                shozais.Add(Repo.RepoJuzenEdit.Get(fcode).HyojiRec());
            }
            if(kcodes[0].Substring(0,2) == "00")
            {   //換地不交付
                kanchis.Add(x.KumiRec.Memo);
            }
            else
            {
                foreach (string kcode in kcodes)
                {
                    kanchis.Add(Repo.RepoKanchiEdit.Get(kcode).HyojiRec());
                }
            }

            var fstring = string.Join("\n", shozais);
            var kstring = string.Join("\n", kanchis);


            rec.KumiCD = x.KumiRec.Key;
            rec.Zen = fstring;
            rec.Go = kstring;

            return rec;
        }
    }

}
