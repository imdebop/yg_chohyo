﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using yg_chohyo.Chohyo;
using yg_chohyo.Chohyo.KanchiShomei;

namespace yg_chohyo.panels
{
    /// <summary>
    /// PopCalendar.xaml の相互作用ロジック
    /// </summary>
    public partial class PopCalendar : Window
    {
        public PopCalendar()
        {
            InitializeComponent();
            string today = DateTime.Today.ToString();
            datePicker.Text = today;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //DateTime date = DateTime.Parse(datePicker.Text);
            //KanchiShomei.Today = date.ToOADate();
            //HoryuShomei.Today = date.ToOADate();
            
            this.Hide();
        }
    }
}
