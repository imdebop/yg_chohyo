﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Diagnostics;
using System.Linq;

namespace yg_chohyo.panels
{
    /// <summary>
    /// OwnerList.xaml の相互作用ロジック
    /// </summary>
    public partial class OwnerList : UserControl
    {
        public OwnerList()
        {
            InitializeComponent();

            //所有者が選択されたイベント処理
            owners.SelectedCellsChanged += new SelectedCellsChangedEventHandler(Owners_SelectedCellsChanged);
        }

        private void Owners_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (MainWindow.eventFlipFlop()) { return; }

            //所有者コードを取得
            DataGrid row = sender as DataGrid;
            var res = (row.CurrentCell.Item as Data.Shoyu);

            string str;
            if(res is null)
            {
                //「かな」を選択した時にも呼ばれてしまうためキャンセル
                str = "null returned from owners.";
                Debug.WriteLine(str);
                return;
            }
            else
            {
                str = res.ShoCode;
            }

            //owners.Visibility = Visibility.Hidden;

            Debug.WriteLine(str);

            //メインウィンドウのインスタンスを取得
            MainWindow mainWindow = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            //メインウィンドウから前後リストの一覧作成処理を呼ぶ
            mainWindow.zengoList.SetList(str); // str is shoCode

            //一覧表タブで前後データ一覧に表示を切り替え
            mainWindow.ListTabs.SelectedIndex = 1;


        }
    }
}
