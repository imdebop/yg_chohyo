using System.Collections.Generic;


namespace yg_chohyo.panels
{
   public class KanaTable{
        public List<string[]> data { get; } = new List<string[]>();
        public KanaTable(){
            data.Add(new string[] { "あ", "い", "う" ,"え","お"});
            data.Add(new string[] { "か", "き", "く" ,"け","こ"});
            data.Add(new string[] { "さ", "し", "す" ,"せ","そ"});
            data.Add(new string[] { "た", "ち", "つ" ,"て","と"});
            data.Add(new string[] { "な", "に", "ぬ" ,"ね","の"});
            data.Add(new string[] { "は", "ひ", "ふ" ,"へ","ほ"});
            data.Add(new string[] { "ま", "み", "む" ,"め","も"});
            data.Add(new string[] { "や", "ゆ", "よ" ,"",""});
            data.Add(new string[] { "ら", "り", "る" ,"れ","ろ"});
            data.Add(new string[] { "わ", "", "" ,"",""});
        }
   } 


    public class KanchiIndex
    {
        public List<string[]> Data { get; } = new List<string[]>();

        public List<string[]> KanIndex
        {
            get
            {
                Data.Add(new string[]{ " 1", " 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9", "10" });
                Data.Add(new string[] { "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" });
                Data.Add(new string[] { "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" });
                Data.Add(new string[] { "31", "32", "33", "34", "35", "36", "37", "38", "39", "40" });
                Data.Add(new string[] { "41", "42", "43", "44", "45", "46", "47", "48", "49", "50" });
                Data.Add(new string[] { "51", "52", "53", "54", "55", "56", "57", "58", "59", "60" });
                Data.Add(new string[] { "61", "62", "63", "64", "65", "66", "67", "68", "69", "70" });
                Data.Add(new string[] { "71", "72", "73", "74", "75", "76", "77", "78", "79", "80" });
                Data.Add(new string[] { "81", "82", "83", "84", "85", "86", "87", "88", "89", "90" });
                Data.Add(new string[] { "91","", "", "", "", "", "", "", "", "" });
                return Data;
            }
        }
    }
}

