﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using yg_chohyo;
using yg_chohyo.Repo;
using System.Diagnostics;
using System.Linq;

namespace yg_chohyo.panels
{
    /// <summary>
    /// KanchiLIst.xaml の相互作用ロジック
    /// </summary>
    public partial class KanchiLIst : UserControl
    {
        public KanchiLIst()
        {
            InitializeComponent();

            wpKanchi.PreviewMouseUp += new MouseButtonEventHandler(wpKanchi_MouseUp);
            wpKanchi.ItemsSource = krList;
            wpKanchi.Items.Refresh();
        }

        private IEnumerable<KanRec> krList { get; set; }

        public void SetList(string block)
        {
            if(block == "") { return; }
            krList = GetContext( Repo.RepoKanchiEdit.GetByBlock(block));
            Debug.WriteLine(krList.Count());
            wpKanchi.ItemsSource = krList;
            //wpKanchi.Items.Refresh();
        }

        public void SetJuzenList(string choazaCD)
        {
            krList = GetJuzenContext(Repo.RepoJuzenEdit.GetByChoaza(choazaCD));
            wpKanchi.ItemsSource = krList;
        }
        /// <summary>
        ///換地リストを生成 
        /// </summary>
        private IEnumerable<KanRec> GetContext(IEnumerable<RepoKanchi> rkRecs)
        {
            KanRec kr;
            foreach (Repo.RepoKanchi rpRec in rkRecs)
            {
                try
                {
                    kr = new KanRec { L01 = $"{rpRec.HyojiRec()}  {Data.CodeTables.DicShoName[rpRec.ShoCode].Replace("　", "")}    {rpRec.KanchiCD}" };
                }
                catch(KeyNotFoundException e)
                {
                    kr = new KanRec { L01 = $" {rpRec.Block}B {rpRec.Lot.Replace(" ","")}L  エラー：所有者コード={rpRec.ShoCode}" };
                }
                yield return kr;
            }
            Debug.WriteLine("kanrec loop finished.");
            
        }
        /// <summary>
        ///従前地リストを生成
        /// </summary>
        private IEnumerable<KanRec> GetJuzenContext(IEnumerable<RepoJuzen> rpJuzen)
        {
            foreach(RepoJuzen rpRec in rpJuzen)
            {
                KanRec jR = new KanRec { L01 = $"{rpRec.HyojiRec()}          {rpRec.FudeCode}J" };
                yield return jR;
            }
        }

        /// <summary>
        /// 前後リストの作成
        /// </summary>
        private void wpKanchi_MouseUp(object sender, MouseButtonEventArgs e)
        {   
            //換地・従前地を兼用して所有者コードから前後リストを作成する
            if (MainWindow.eventFlipFlop()) { return; }

            //str は所有者コードまたは従前地筆コード
            var str = (sender as Label).Content.ToString();
            string shocode;
            string kcode = "";
            if (str.EndsWith("J"))
            {   //従前地の処理
                int fcodeLen = Data.CodeTables.JuzenCodeLength;
                string fcode = str.Substring(str.Length - (fcodeLen + 1), fcodeLen);
                Data.Juzen j = Dapper.DapperJuzen.GetByFude(fcode);
                shocode = j.ShoCode;
            }else{
                kcode = str.Substring(str.Length - Data.CodeTables.KanCodeLength);
                //保留地の処理
                if (Data.CodeTables.IsHoryuchi(str))
                {
                    shocode = str;
                }
                else
                {   //換地の処理
                    Debug.WriteLine(kcode);
                    shocode = Data.CodeTables.DicKan2sho[kcode];
                }
            }
            Debug.WriteLine(shocode);
            Debug.WriteLine(Data.CodeTables.DicShoName[shocode]);

            //メインウィンドウのインスタンスを取得
            MainWindow mainWindow = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();
            //メインウィンドウから前後リストの一覧作成処理を呼ぶ
            if (Data.CodeTables.IsHoryuchi(shocode))
            {
                mainWindow.zengoList.SetHoryuList(shocode, kcode);

            }
            else
            {
                mainWindow.zengoList.SetList(shocode);
            }
            //一覧表タブで前後データ一覧に表示を切り替え
            mainWindow.ListTabs.SelectedIndex = 1;

        }

    }





    public class KanRec
    {
        public string L01 { get; set; } = "";
        public string L02 { get; set; } = "";//現在不使用
    }
}
