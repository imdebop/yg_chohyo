﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using yg_chohyo.Data;
using NPOI.OpenXmlFormats.Dml;

namespace yg_chohyo.Repo
{
    public class RepoKanchiEdit
    {
        public static RepoKanchi Get(string kanchiCD)
        {
            Data.Kanchi kRec = Dapper.DapperKanchi.GetByKancode(kanchiCD);
            if(Data.Util.IsNum(kRec.KanCode) )
            {
                RepoKanchi rkRec = new RepoKanchi(kRec);
                return rkRec;
            }
            else
            {
                return null; 
            }
        }
        /// <summary>
        ///レポジット換地レコードをブロックごとにまとめて取得 
        /// </summary>
        public static IEnumerable<RepoKanchi> GetByBlock(string block)
        {
            foreach (Data.Kanchi rpRec in Dapper.DapperKanchi.GetByBlock(block))
            {
                yield return new RepoKanchi(rpRec);
            }
        }
    }

    public class RepoKanchi
    {
        public string KanchiCD { get; private set; }
        public string Block { get; private set; }
        public string Lot { get; private set; }
        public string Chiseki { get; private set; }
        public string ShoCode { get; private set; }
        public Data.Kanchi Kanchi { get; private set; }

        public RepoKanchi(Data.Kanchi k)
        {
            KanchiCD = k.KanCode;
            Block = k.GetBlock();
            Lot = k.GetLot().Trim();
            Chiseki = Data.Util.ChisekiFMT( k.Menseki);
            ShoCode = k.ShoCode;
            Kanchi = k;
        }
        public string HyojiRec()
        {
            string str = "";
            str += (Block + "B").PadLeft(4, ' ');
            str += (Lot + "L").PadLeft(6, ' ');
            str += Chiseki.PadLeft(7, ' ');
            return str;
        }
        public string BlockLotZeroPad()
        {
            string str = "";
            str += int.Parse(Block).ToString("00") + "B";
            if (Lot.Contains("-"))
            {
                str += Lot;
            }
            else
            {
                str += int.Parse(Lot).ToString("00") + "L";
            }
            str += "L";
            return str;
        }

    }

    class CreateSokochi
    {
        public static SokochiRec Get(Kanchi kRec)
        {
            Dictionary<string, List<string>> azaDic = new Dictionary<string, List<string>>();
            //換地のブロック・ロットを編集
            SokochiRec sokoRec = new SokochiRec();
            sokoRec.Block = kRec.GetBlock();
            sokoRec.Lot = kRec.GetLot();
            //底地リストを編集
            string[] fudeCodes = kRec.Sokochi.Split("/");
            foreach (string fCode in fudeCodes)
            {
                Juzen j = new Juzen() { FudeCode = fCode };
                string azaCode = j.GetAzaCode();
                if (azaDic.ContainsKey(azaCode))
                {
                    azaDic[azaCode].Add(fCode);
                }
                else
                {
                    azaDic[azaCode] = new List<string>() { fCode };
                }

            }
            //字コード順に底地の地番を編集
            string[] azas = azaDic.Keys.ToArray();
            Array.Sort(azas);
            List<string> azaBetu = new List<string>();
            string str = "";
            foreach (string aza in azas)
            {
                if (aza.Substring(0,2) == "er" || aza.Substring(0,1) == "*")
                {
                    System.Windows.MessageBox.Show("底地データにエラーがあります。\n確認してください。");
                    continue;
                }
                str = Data.CodeTables.DicChoaza[aza];
                string wk = "";
                foreach (string fCodes in azaDic[aza])
                {
                    Juzen j = new Juzen() { FudeCode = fCodes };
                    wk += $",{j.GetJuzenCiban().Trim()}";
                }
                str += wk.Substring(1);
                azaBetu.Add(str);
            }
            sokoRec.Str = string.Join("\n", azaBetu);

            return sokoRec;
        }
    }

    class SokochiRec
    {
        public string Block { get; set; }
        public string Lot { get; set; }
        /// <summary>
        /// 町字名＋地番の並び
        /// </summary>
        public string Str { get; set; }
    }




}
