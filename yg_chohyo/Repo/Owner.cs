﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yg_chohyo.Repo
{
    class Owner
    {
        public string ShoCode { get; private set; }
        public string Name { get; private set; }
        public string Jusho { get; private set; }

    }

    public class RepoKyoyu
    {
        //インスタンスのデータを保管するリスト
        public List<Data.Kyoyu> DataRecs { get; private set; } 

        public RepoKyoyu(string shoCode)
        {
            DataRecs = (List<Data.Kyoyu>)Dapper.DapperKyoyu.GetRecsByShocode(shoCode);
        }

        public List<string> GetHyojiRecs() {
            List<string> strs = new List<string>();
            foreach(Data.Kyoyu kList in DataRecs)
            {
                string bunshi = kList.Bunshi;
                string bunbo = kList.Bunbo;
                string mochibun = $"({bunshi}/{bunbo})".PadRight(6, ' ');
                string tName = kList.TokiName.PadRight(8,'　');
                string tJusho = kList.TokiJusho;
                strs.Add($"{mochibun}{tName}{tJusho}");
            }
            return strs;
        }

        public List<string> GetNames()
        {
            List<string> names = new List<string>();
            foreach(Data.Kyoyu k in DataRecs)
            {
                names.Add(k.TokiName);
            }
            return names;
        }
    }



}
