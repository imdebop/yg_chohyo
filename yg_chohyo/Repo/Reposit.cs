﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using yg_chohyo.Data;

namespace yg_chohyo.Repo
{

    public class RepoKumiSet
    {
        //
        //所有者ごとの組合せデータレポジットのリスト　
        //
        public static IEnumerable<RepoKumi> Get(string shoCode)
        {
            IEnumerable<Kumi> kumis = Dapper.DapperKumi.GetByShocode(shoCode);
            //RepoKumi repokumi = new RepoKumi();
            foreach (Kumi kumi in kumis)
            {
                CheckKancode(kumi);
                Debug.WriteLine(kumi.Key);
                var repokumi = new RepoKumi()
                {
                    KumiRec = kumi,
                    JuzenRecs = GetJuzens(kumi.Fcodes),
                    KanchiRecs = GetKanchis(kumi.Kanchis)
                };
                yield return repokumi;
            }
            
        }
        ///<summary>組合せデータレポジット　１件毎</summary>
        public static RepoKumi GetOneRec(string kumiKey)
        {
            Kumi kumi = Dapper.DapperKumi.GetByKumiKey(kumiKey);
            CheckKancode(kumi);
            var repokumi = new RepoKumi()
            {
                KumiRec = kumi,
                JuzenRecs = GetJuzens(kumi.Fcodes),
                KanchiRecs = GetKanchis(kumi.Kanchis)

            };
            return repokumi;
         }

        private static void CheckKancode(Kumi kumi)
        {
            if (kumi.Kanchis == "")
            {
                switch (kumi.Memo)
                {
                    case "<90条>":
                        kumi.Kanchis = "00090000";
                        break;
                    default:
                        kumi.Kanchis = "00000000";
                        break;
                }
            }
        }


        private static List<Data.Juzen> GetJuzens(string fcodes) {
            List<Data.Juzen> recs = new List<Juzen>();
            string[] keys = fcodes.Split("/");
            foreach(string key in keys)
            {
                recs.Add(JuzenRec.GetRec(key));
                //Debug.WriteLine(recs.Last().GetValues()) ;
            }
            return recs;
        }
        private static List<Data.Kanchi> GetKanchis(string kanchis)
        {
            List<Data.Kanchi> recs = new List<Kanchi>();
            string[] keys = kanchis.Split("/");
            foreach (string key in keys)
            {
                recs.Add(KanchiRec.GetRec(key));
            }
            return recs;
        }
    }
    /// <summary>
    /// 組合せデータレポジット　１件分
    /// </summary>
    public class RepoKumi
    {
        public Kumi KumiRec { get; set; }
        public List<Juzen> JuzenRecs { get; set; }
        public List<Kanchi> KanchiRecs { get; set; }

    }

    //public static class DataExtention
    //{
    //    public static string GetJuzenCiban(this Juzen j)
    //    {
    //        return Data.Util.Fcode2shozai(j.FudeCode).Chiban();
    //    }
    //}


    public class JuzenRec
    {
        public static Data.Juzen GetRec(string fudeCode) {
             return Dapper.DapperJuzen.GetByFude(fudeCode);
        }
    }


    public class KanchiRec
    {
        public static Data.Kanchi GetRec(string kanCode)
        {
            if (kanCode.Substring(0, 2) == "00")
            {
                return new Data.Kanchi() { KanCode = kanCode };
            }
            else
            {
                return Dapper.DapperKanchi.GetByKancode(kanCode);
            }
        }
    }

    public class KumiStackEdit
    {
        public static KumiStack Get(RepoKumi repoKumi)
        {
            KumiStack kumiStack = new KumiStack();
            //従前地レコードを逆順にスタックに積む
            for (int i = repoKumi.JuzenRecs.Count - 1; i >= 0; i--)
            {
                Stack<string> jRec = new Stack<string>();
                Data.Juzen j = repoKumi.JuzenRecs[i];
                jRec.Push(j.ChisekiShosu());
                jRec.Push(j.ChisekiSeisu());
                jRec.Push(j.GetChimoku());
                jRec.Push(j.GetJuzenCiban());
                jRec.Push(j.GetJuzenChoaza());

                kumiStack.Juzens.Push(jRec);
            }
            for(int i = repoKumi.KanchiRecs.Count - 1; i >= 0; i--)
            {
                Stack<string> kRec = new Stack<string>();
                Data.Kanchi k = repoKumi.KanchiRecs[i];
                kRec.Push(k.Menseki);
                kRec.Push(k.GetLot());
                kRec.Push(k.GetBlock());

                kumiStack.Kanchis.Push(kRec);
            }

            return kumiStack;

        }
    }

    public class KumiStack
    {
        public Stack<Stack<string>> Juzens { get; set; } = new Stack<Stack<string>>();
        public Stack<Stack<string>> Kanchis { get; set; } = new Stack<Stack<string>>();
    }

}
