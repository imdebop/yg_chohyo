﻿using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.Security.RightsManagement;
using System.Text;
using yg_chohyo.Data;

namespace yg_chohyo.Repo
{
    public class RepoJuzenEdit
    {
        public static IEnumerable<RepoJuzen> GetByChoaza(string choazaCD)
        {
            foreach(Data.Juzen j in Dapper.DapperJuzen.GetByChoaza(choazaCD))
            {
                yield return new RepoJuzen(j);
            }
        }
        
        public static RepoJuzen Get(string fudeCode)
        {
            Data.Juzen djRec = Dapper.DapperJuzen.GetByFude(fudeCode);
            if(djRec is null)
            {
                return null;
            }
            else
            {
                RepoJuzen rjRec = new RepoJuzen(djRec);
                return rjRec;
            }
        }
    }

    public class RepoJuzen
    {
        public string FudeCode { get; private set; }
        public string ChoAza { get; private set; }
        public string Chiban { get; private set; }
        public string Chimoku { get; private set; }
        public string TokiChiseki { get; private set; }
        public int HyoteiSisu { get; private set; } 
        public RepoJuzen(Data.Juzen j)
        {
            FudeCode = j.FudeCode;
            ChoAza = j.GetJuzenChoaza();
            Chiban = j.GetJuzenCiban();
            Chimoku = j.GetChimoku();
            TokiChiseki = j.GetHyojiChiseki();
            HyoteiSisu = j.HyoteiSisu;
        }
        public string HyojiRec()
        {
            string str = "";
            str += ChoAza.PadRight(8, '　');
            str += Chiban.PadRight(8, ' ');
            str += Chimoku.PadRight(5, '　');
            str += TokiChiseki.PadLeft(8, ' ');
            return str;
        }
    }
}
