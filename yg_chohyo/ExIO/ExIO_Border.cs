using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;

namespace yg_chohyo
{
    partial class ExIO
    {
        public void WriteBorder()
        {
            Border b = new Border(_book);
            var row = _sheet.GetRow(24);
            row.Cells[3].CellStyle = b.AA();
        }

        public void WriteBorder(int row, int start, int end, string type)
        {
            var rowObj = _sheet.GetRow(row);
            switch (type)
            {
                case "AB":
                    for(int i = start; i <= end; i++)
                    {
                        rowObj.Cells[i].CellStyle = this._border.AT();
                    }
                    break;
                default:
                    break;
            }

        }
        
        public void Mergecells(CellRangeAddress cra)
        {
            _sheet.AddMergedRegion(cra);
        }

        public static CellRangeAddress Cra(int firstRow, int lastRow, int firstCol, int lastCol)
        {
            return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
        }

        public void SetCellStr(string str, int rowIdx, int colIdx ) {
            var row = _sheet.GetRow(rowIdx);
            var cell = row.GetCell(colIdx);
            cell.SetCellValue(str);
        }

        public void SetCellDoubule(double num, int rowIdx, int colIdx)
        {
            var row = _sheet.GetRow(rowIdx);
            var cell = row.GetCell(colIdx);
            cell.SetCellValue(num);
        }

    }
    class Border
    {

        public Border(IWorkbook book)
        {
            _book = book;

        }

        private IWorkbook _book;

        //セル別　罫線の種類
        //AA：□　AT:Π　AB：U　AN:｜｜
        //TL：[�U　TR:�U]
        //
        public HSSFCellStyle AA()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.Medium;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.Medium;
            return aa;
        }

        public HSSFCellStyle AT()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.Medium;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

        public HSSFCellStyle AB()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.None;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.Medium;
            return aa;
        }

        public HSSFCellStyle AN()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.None;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

        public HSSFCellStyle TLa()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Thin;
            aa.BorderTop = BorderStyle.Medium;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.Medium;
            return aa;
        }

        public HSSFCellStyle TLt()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Thin;
            aa.BorderTop = BorderStyle.Medium;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

        public HSSFCellStyle TLb()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Thin;
            aa.BorderTop = BorderStyle.None;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.Medium;
            return aa;
        }

        public HSSFCellStyle TLn()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Thin;
            aa.BorderTop = BorderStyle.None;
            aa.BorderRight = BorderStyle.Medium;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

        public HSSFCellStyle TRa()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.Medium;
            aa.BorderRight = BorderStyle.Thin;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

        public HSSFCellStyle TRt()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.Medium;
            aa.BorderRight = BorderStyle.Thin;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

        public HSSFCellStyle TRb()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.None;
            aa.BorderRight = BorderStyle.Thin;
            aa.BorderBottom = BorderStyle.Medium;
            return aa;
        }

        public HSSFCellStyle TRn()
        {
            HSSFCellStyle aa = (HSSFCellStyle)_book.CreateCellStyle();
            aa.BorderLeft = BorderStyle.Medium;
            aa.BorderTop = BorderStyle.None;
            aa.BorderRight = BorderStyle.Thin;
            aa.BorderBottom = BorderStyle.None;
            return aa;
        }

            
    }
}