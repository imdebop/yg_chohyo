namespace LocalPath{

    public static class LocalPath{
        public static string GetLocalRoot(){
            string rootPath = "";
            //System.OperatingSystem os = System.Environment.OSVersion;
            //string res = os.ToString();
            string res = System.Net.Dns.GetHostName();
            if(res.StartsWith( "SakuragiMac" )){
                rootPath = "/Users/hayashi/";
            }else if(res.StartsWith("dh-ub20"))
            {
                rootPath = "/home/dh/";
            }else{
                rootPath = "error in hostname!:" + res;
            }
            return rootPath;
        }

    }
}