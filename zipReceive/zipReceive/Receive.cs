﻿using System;
using Ionic.Zip;
using Ionic.Zlib;
using System.Text;
using System.Diagnostics;

namespace zipReceive
{
    public static class Receive
    {
        public static string Do( string fname, string expandPath ){
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            // or Trace.Listeners.Add(new ConsoleTraceListener());
            Trace.WriteLine("Hello World");
            
            return fname + "aaa";
        }
        public static string Decomp(string outPath){

            string input = @"C:\work\hogefoo.zip";
            string output = @"C:\work\hogefoo";
            using (var zip = new ZipFile(input, Encoding.GetEncoding("shift_jis")))
            {
                // 一括で指定パスに解凍
                zip.ExtractAll(output);

                //// 全エントリーを走査して個別解凍
                //foreach (var entry in zip.Entries)
                //{
                //    entry.Extract();
                //}

                // 特定のファイルのみ解凍
                //zip["hoge.txt"].Extract("extra");
            }
            return "result";
        }

    }

    public static class Send{
        public static string Do( string folderPath, string folderName){
            Compress(folderPath, folderName);
            return "aabb";
        }

        public static string Compress(string folderPath, string folderName){
            System.Text.Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        // ZIPファイルの生成
            using (var zip = new ZipFile(Encoding.GetEncoding("shift_jis")))
                {
                    // 圧縮レベル
                    zip.CompressionLevel = CompressionLevel.BestCompression;

                    // ファイルやディレクトリをZIPアーカイブに追加
                    // 以下のような構成のZIPアーカイブとなる
                    // hogefoo.zip
                    //   --hoge.txt
                    //   -- foo
                    //zip.AddFile(@"C:\work\hoge.txt", "");
                    //zip.AddDirectory(@"C:\work\foo", "foo");
                    string folderFull = folderPath + folderName;
                    zip.AddDirectory(folderFull, "");

                    // 保存
                    //zip.Save(@"C:\work\hogefoo.zip");
                    zip.Save(folderFull + ".zip");
                }
            return "result";
        }


    }
}
