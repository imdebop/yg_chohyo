using Microsoft.VisualStudio.TestTools.UnitTesting;
using zipReceive;
using System.Diagnostics;
using System;
using LocalPath;

namespace zipReceiveTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            // or Trace.Listeners.Add(new ConsoleTraceListener());
            string rootPath = LocalPath.LocalPath.GetLocalRoot();
            Trace.WriteLine(rootPath);
            string result = Receive.Do("xyz", "expandPath");
            Assert.AreEqual(result, "xyzaaa");
            
            //string testFolder = "/home/dh/dev/zipTest/";
            string testFolder = rootPath + "dev/zipTest/";

            result = Send.Do( testFolder, "aFolder");
            Assert.AreEqual( "aabb", result);
        }
    }
}
