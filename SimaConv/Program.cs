﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Linq;
using yg_chohyo.Data;
using yg_chohyo.Dapper;

namespace SimaConv
{
    /// <summary>
    /// Autocadでの底地チェック用の区画結線座標データを作成
    /// </summary>
    class Program
    {

        //sima形式座標レコード保管用
        static Dictionary<string, string> dicZahyo = new Dictionary<string, string>();

        //sima形式ブロック・ロット保管用
        static Dictionary<string, List<string>> dicBlock = new Dictionary<string, List<string>>();

        static void Main(string[] args)
        {
            //底地抽出用「画地・座標リスト(kakuchiOut.txt)」作成用バッファ
            List<string> outList = new List<string>();

            string outPath = "c:/yagyu/sokochi/sima/kakuchiOut.txt";

            /////////////////////////////////////////////////////////////////
            //元simaの処理///////////////////////////////////////////////////
            string simaPath = "c:/yagyu/sokochi/sima/柳生川南部地区座標データ20200417.sim";

            MainProc(simaPath, ref outList);


            ChkKanchiExists cke = new ChkKanchiExists();
            cke.Chk( //古いsima画地データを表示
                dicBlock.OrderBy(x => Util.BL2code(x.Key)).Select(x => Util.BL2code(x.Key))
            );

            Debug.WriteLine("＊＊元のsimaに追加が必要な画地＊＊");
            cke.ChkNoSima(outList);

            //////////////////////////////////////////////////////////////////
            ///追加simaの処理
            simaPath = "c:/yagyu/sokochi/sima/ブロック追加.txt";
            //以下２つの辞書はパブリック
            dicBlock = new Dictionary<string, List<string>>();
            dicZahyo = new Dictionary<string, string>();
            MainProc(simaPath, ref outList);

            Debug.WriteLine($"＊＊追加後のsimaに不足している画地＊＊");
            cke.ChkNoSima(outList);

            File.WriteAllLines(
                outPath,
                //換地コードが管理データに存在するデータを出力
                outList
                    .Where(
                        x => cke.dicKancodes.ContainsKey
                            (Util.BL2code(x.Split(",")[0])
                            )
                    )
                    .OrderBy(x => Util.BL2code(x.Split(",")[0]))
            );
        }

        static void MainProc(string simaPath, ref List<string> outList)
        {

            string currBlk = "";
            string tenban;
            string kancode;
            string[] simData = File.ReadAllLines(simaPath);

            //Regex reg = new Regex(@"\[(?<kakuchi>.+)\]");
            Regex reg = new Regex(@"(?<b_l>\d+B\d+(-\d+)?)");//[5B3-2] 形式にマッチ

            foreach(string r in simData)
            {
                
                string[] recArr = r.Replace(" ","").Split(",");

                if(recArr[0] == "F00") { break; }//以降のデータはパス

                if(recArr[0] == "D00")
                {
                    var m = reg.Match(recArr[2]);//ブロック番号の時が処理対象
                    if (m.Success)
                    {
                        currBlk = m.Groups["b_l"].Value;
                        kancode = Util.BL2code(currBlk);
                        //Debug.WriteLine(kancode);
                    }
                    else
                    {
                        currBlk = "";
                        continue;
                    }

                    if (dicBlock.ContainsKey(currBlk))
                    {
                        Debug.WriteLine($"duplicate Block={currBlk}");
                    }
                    else
                    {
                        dicBlock[currBlk] = new List<string>();
                    }
                }

                if (recArr[0] == "A01")
                {
                    tenban = recArr[1];
                    dicZahyo[tenban] = $"{recArr[3]}:{recArr[4]}";
                    //Debug.WriteLine(tenban);
                    continue;
                }

                if(recArr[0] == "B01")
                {
                    tenban = recArr[1];

                    if(currBlk != "")//ブロック(3B2-4形式)のみ出力
                    {
                        dicBlock[currBlk].Add(dicZahyo[tenban]);
                    }
                }

            }
            //foreach (KeyValuePair<string, List<string>> d in dicBlock.OrderBy(x => Util.BL2code(x.Key)).Select(x => x))
            foreach (var d in dicBlock)
            {
                //Debug.WriteLine(d.Key);
                outList.Add($"{d.Key},{string.Join(',', d.Value)}");
            }



        }
    }

    /// <summary>
    /// 整合性チェック
    /// </summary>
    class ChkKanchiExists
    {
        public Dictionary<string, bool> dicKancodes = new Dictionary<string, bool>();

        public ChkKanchiExists()
        {

        }
        void kanriDicSetup()
        {   //管理データの換地コードをセット・準備
            foreach(string kancode in yg_chohyo.Dapper.DapperKanchi.GetAll().Select(x => x.KanCode))
            {
                dicKancodes[kancode] = false;

            }
        }

        public void Chk(IEnumerable<string> kancodes)
        {
            kanriDicSetup();

            //simaの古い画地データ（管理データに不存在）を表示
            Debug.WriteLine("＊＊古い画地データ＊＊");
            foreach(string kc in kancodes)
            {
                if (dicKancodes.ContainsKey(kc))
                {
                    //dicKancodes[kc] = true;
                }
                else
                {
                    Debug.WriteLine($"sima block not exists in kanri :{kc}");
                }
            }
        }

        public void ChkNoSima(List<string> outList)
        {   //管理データ対応せず、不足しているsimaデータを表示
            //これはファイル出力して新simaからの抽出ブロック情報になる

            kanriDicSetup();

            // bkl = 出力バッファのブロック・ロットをソートし、換地コードに変換
            List<string> kancodes = outList.Select( x => Util.BL2code( x.Split(",")[0]) ).ToList();

            foreach(string kc in kancodes)
            {
                dicKancodes[kc] = true;
            }

            foreach(var d in dicKancodes)
            {

                if (!dicKancodes[d.Key])
                {
                    Debug.WriteLine($"kanri kakuchi not in sima data :{d.Key}");
                }
            }
        }


    }

}
