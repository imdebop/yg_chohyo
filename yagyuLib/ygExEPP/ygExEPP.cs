﻿using System;
using System.IO;
using ygUtil;
using ygUtil.Interfaces;
using OfficeOpenXml;
using ygExBase;
using ygExBase.Interfaces;
using System.Collections.Generic;

namespace ygExEPP
{
    public class YgExEPP
    {
        ExRepo _exRepo;
        //public YgExEPP(string expath, IExDefRec exDefRec){
        public YgExEPP(ExRepo exrepo){
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            FileInfo file = new FileInfo(exrepo.Expath()); 
            using(ExcelPackage exPackage = new ExcelPackage(file)){
                string sheetname = exrepo.SheetName();
                ExcelWorksheet ws = exPackage.Workbook.Worksheets[sheetname]; //base->1
                //var v = ws.Cells[3,2].Value.ToString();
                var v = ws.Cells["H5"].Value;
                if(v == null){
                    Console.WriteLine("v is null");
                }else{
                    Console.WriteLine(v);
                }
                
                //var exCells = ws.Cells;
                var wsdata = new WsData(ws, exrepo);
                exrepo.SetWsdata(wsdata);
                _exRepo = exrepo;
                Console.WriteLine(_exRepo.GetWsRawData(3,4));
            }
        }

        public ExRepo Repo(){
            return this._exRepo;
        }

    }

    public class WsData: IWsData
    {
        List<List<object>> _dataTbl = new List<List<object>>();
        //IExDefRec _defRec;
        public WsData(ExcelWorksheet ws, ExRepo exrepo){
            int sr = exrepo.DefRec().StartRow();
            int lr = exrepo.DefRec().LastRow();
            int sc = exrepo.DefRec().StartCol();
            int lc = exrepo.DefRec().LastCol();

            for( int i = sr; i <= lr; i++){
                List<object> rec = new List<object>();
                for(int ii = sc; ii <= lc; ii++){
                    rec.Add(ws.Cells[i, ii].Value);
               } 
               _dataTbl.Add(rec);
            }
            //Console.WriteLine(ws.Cells[7,8].Value);
        }

        public object GetRawData(int row, int col){
            return this._dataTbl[row][col];
        }

        /*class GetSheetVal{
            ExcelRange excelRange;
            public GetSheetVal(ExcelRange exrange){
                excelRange = exrange;
            }
            public object GetObj(int row, int col){
                return null;
            }
            //return "aabbcc";
        }*/
    }




        /*
        public IExDefRec DefRec(){
            return _defRec;
        }
        public string ValString(int row, int col){
            return "";
        }
        */

    /*
    class xxxExRepo: IExRepo
    {
        List<string> ColDefs;
        List<ColInfo> ColInfos = new List<ColInfo>();
        public ExRepo(ExcelRange cells, IExDefRec exDefRec){
            this.ColDefs =exDefRec.DefVal();
            foreach(string s in ColDefs){
                ColInfos.Add(new ColInfo(s));
            }
        }
        public List<string> GetColDefs(){
            return null;
        }
        public void SetColDefs(IExDefRec ybdef){

        }
        public List<List<string>> DataList(){
            return null;
        }

        class ColInfo{
            public int Seq = 0;
            public string TypeId = "";
            public string ColName = "";

            public ColInfo(string colInfo){
                var wk = colInfo.Split(":");
                Seq = int.Parse(wk[0]);
                TypeId = wk[1];
                ColName = wk[2];
            }
        }


    }
    */
    public class InitData: IygUtil{
        public string GetWkFolder(string prjName){
            return "NA";
        }
    }


}
