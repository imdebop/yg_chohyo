﻿using System;

namespace ygInit
{
    public static class OsInfo  
    {
        public static string OsName(){
            PlatformID pid = System.Environment.OSVersion.Platform;
            string res = "NA";
            switch (pid) 
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    res = "win";
                    break;
                case PlatformID.Unix:
                    res = "linux";
                    break;
                case PlatformID.MacOSX:
                    res = "mac";
                    break;
                default:
                    break;
            }
            return res;
        }


        public static string gdFolderPath(){
            string path = "";
            switch(OsName()){
                case "linux":
                case "mac":
                    path  = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    break;
                default:
                    path = "NA";
                    break;
            }
            path += "/gd_folder/";

            return path;
        }

        public static string ygYobaiPath(){
            string path = gdFolderPath();
            path += @"柳生作業用/用地買収/";
            return path;
        }
    }
}
