﻿using System.IO;
using System.Collections.Generic;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace ygMail
{
    public class YgMailSend
    {
        public static void send(SendMailParams pm){

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(pm.senderName, pm.senderAddr));
            //message.To.Add(new MailboxAddress("柳生組合PC001", "yagyu-i@m01.drive-net.jp"));
            foreach(var a in pm.addressees){
                message.To.Add(new MailboxAddress(a.name, a.addr));
            }
            message.Bcc.Add(new MailboxAddress(pm.bcc.name, pm.bcc.addr));
            message.Subject = pm.subject;
            var body = new TextPart("plain"){
                Text = pm.text
            };

            var multipart = new Multipart("mixed");
            multipart.Add(body);

            foreach(string path in pm.zipPaths){
                //var path = "/home/dh/gd_folder/柳生作業用/yagyu_settings/data/20210921帳票更新大地01.zip";            
                var attachment = new MimePart(){
                    Content = new MimeContent(File.OpenRead(path)),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(path)
                    };
                multipart.Add(attachment);
            }
            message.Body = multipart;

            using(var client = new SmtpClient()){
               client.Connect("smtp.daichi-c.jp", 587, MailKit.Security.SecureSocketOptions.None);
               client.Authenticate("sekkei01@daichi-c.jp","daifuku07!!");
               client.Send(message);
               client.Disconnect(true);
            };

        }
    }
}
