using System.Collections.Generic;
using localizeCs;

namespace ygMail{
    public class SendMailParams : MailParamsBase{
        public List<string> zipPaths {get;} = new List<string>();
        string zipPath;

        public SendMailParams(string title, string mainText, bool test){
            //zipPath = MailPath.zipAttachFolder();
            senderName = "大地Ｃ";
            senderAddr = "sekkei01@daichi-c.jp";
            //addAddressee("nifty", "daiho_hayashi@nifty.com");
            if(test){
                //テスト時の受取とBCC
                addAddressee("nifty", "daiho_hayashi@nifty.com");
                bcc = new BccReceiver(){
                    //name = "google",
                    //addr = "uv7dhys@gmail.com"
                    name = "asahi",
                    addr = "uv7d-hys@asahi-net.or.jp"
                };
            }else{
                addAddressee("柳生PC001", "yagyu-i@m01.drive-net.jp");
                bcc = new BccReceiver(){
                    name = senderName,
                    addr = senderAddr
                };
            }
            subject = title;
            text = mainText;
            setAuth(user: senderAddr, pass: "daifuku07!!");
        }

        public void setAttachment(string attachmentPath){
            zipPaths.Add(attachmentPath);
        }
    }



}