using System.Collections.Generic;
using MailKit;

namespace ygMail{
    public class MailParamsBase{
        public string senderName {get; set;}
        public string senderAddr {get; set;}
        public List<Receiver> addressees {get; set;} = new List<Receiver>();
        public BccReceiver bcc {get; set;} = null;
        public string subject {get; set;}
        public string text {get; set;}
        public List<string> attachPaths {get; set;}

        public Connection conParams {get; set;} 

        public Auth  auth{get; set;}

        public void setAuth(string user, string pass){
            auth = new Auth(){user=user, passwd=pass};
        }
        public void addAddressee(string name, string address){
            Receiver r = new Receiver(){name=name, addr=address};
            addressees.Add(r);
        }

        public class Receiver{
            public string name {get; set;}
            public string addr {get; set;}
        }

        public class BccReceiver{
            public string name {get; set;}
            public string addr {get; set;}
        }
        public class Auth{
            public string user {get; set;}
            public string passwd {get; set;}
        }

        public class Connection{
            public string smtpAddr {get; set;}
            public int port {get; set;}
            public string userId {get; set;}
            public string passwd {get; set;}
            public MailKit.Security.SecureSocketOptions option {get; set;}
        }
    }
}

