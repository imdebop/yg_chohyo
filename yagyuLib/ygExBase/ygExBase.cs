﻿using System;
using System.Collections.Generic;
using ygExBase;
using ygExBase.Interfaces;

namespace ygExBase
{
    public class YgExBase
    {
        public YgExBase(Object rec){
            string objType = rec.GetType().ToString();
            //Console.WriteLine(objType);
        }
    }

    public class ExSheetBase{
        IExDefRec _defRec;

        public ExSheetBase(IExDefRec defrec){
            _defRec = defrec;
        }
        public IExDefRec DefRec(){
            return _defRec;
        }
        public string ValString(int row, int col){
            return "";
        }

    }


    public class ExRepo: IExRepo
    {
        //List<string> ColDefs;
        List<ColInfo> _colInfos = new List<ColInfo>();
        IExDefRec _defrec;
        string _expath;
        string _sheetName;
        IWsData _wsData;

        /////////////////////////////////////////////////////
        public ExRepo(string expath, string sheetName, IExDefRec defrec){
            _expath = expath;
            _sheetName = sheetName;
            _defrec = defrec;
            SetColInfos(defrec);
        }

        public string GetTblData(int r, int c){
            var obj = GetWsRawData(r, c);
            string typeId = this._colInfos[c - 1].TypeId;
            string s = YgExValidate.eval(obj, typeId);
            return s;
        }

        public object GetWsRawData(int r, int c){
            var obj = this._wsData.GetRawData(r,c - 1);
            //Console.WriteLine(_expath);
            //Console.WriteLine(_sheetName);
            return obj;
        }

        public string SheetName(){
            return _sheetName;
        }

        public string Expath(){
            return _expath;
        }
        public IExDefRec DefRec(){
            return _defrec;
        } 

        public void SetWsdata(IWsData wsData){
            _wsData = wsData;
        }
        public int RecsNum(){
            return 0;
        }
        void SetColInfos(IExDefRec defRec)
        {
            foreach(string s in defRec.ColDef()){
                _colInfos.Add(new ColInfo(s));
            }
        }
        public List<string> GetColDefs(){
            return null;
        }
        public void SetColDefs(IExDefRec ybdef){

        }
        public List<List<string>> DataList(){
            return null;
        }

        public class ColInfo{
            public int Seq = 0;
            public string TypeId = "";
            public string ColName = "";

            public ColInfo(string colInfo){
                var wk = colInfo.Split(':');
                Seq = int.Parse(wk[0]);
                TypeId = wk[1];
                ColName = wk[2];
            }
        }
    }
}


/*
namespace ygExBase.Interfaces{
    public interface IExRepo{
        List<string> GetColDefs();
        void SetColDefs(IExDefRec ybdef);
        List<List<string>> DataList();


    }
    public interface IExDefRec{
        List<string> DefVal();
        int StartRow();
        int LastRow();
        int StartCol();
        int LastCol();

    }

    public interface IExSheet<T>{

        //void SetSheet<objType>(Object obj);
        string ValString(int row, int col);

        IExDefRec DefRec();

    }


}
*/

