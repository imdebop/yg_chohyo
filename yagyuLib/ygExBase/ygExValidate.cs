
using System;

namespace ygExBase{
  public static class YgExValidate{
    public static string eval(object data, string typeId){
      if(data is null){
        return "";
      }else if(typeId == "S"){
        return data.ToString();
      }else if(typeId == "N"){
        return "";
      }else{
        string res = obj2numStr(data, typeId);
        return res;
      }
    }

    static string obj2numStr(object obj, string typeId){
      try{
        var s = obj.GetType().ToString();
        //Console.WriteLine(s);
        switch(s){
          case "System.String":
            return obj.ToString();
          case "System.Double":
            if( typeId == "9"){
              return obj.ToString();
            }else{
              int i = Int32.Parse(typeId);
              string fm = $"{{0, {i}}}";
              var ss = string.Format(fm, (double)obj);
              return ss;
            }
          default:
            return obj.ToString();
        }
      }
      catch{
        return "NA";
      }
    }

  }


}