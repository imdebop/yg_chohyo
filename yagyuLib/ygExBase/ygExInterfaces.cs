using ygExBase;
using System.Collections.Generic;

namespace ygExBase.Interfaces{
    public interface IExRepo{

        //カラムはワークシートではなく、テーブルのもの
        string GetTblData(int r, int c);
        int RecsNum();
        List<string> GetColDefs();
        void SetColDefs(IExDefRec ybdef);
        List<List<string>> DataList();


    }
    public interface IExDefRec{
        List<string> ColDef();
        int StartRow();
        int LastRow();
        int StartCol();
        int LastCol();

        //int MaxRow();

    }

    public interface IExSheet{

        //void SetSheet<objType>(Object obj);
        string ValString(int row, int col);

        IExDefRec DefRec();

    }

    public interface IWsData{
      object GetRawData(int row, int col);
    }


}
