﻿using System;

namespace YgLocalRoot
{
    public class LocalRoot
    {
        static string zipSourceFolder{get; set;} = "";
        static string zipDestFolder{get; set;} = "";
        public static string GetLocalRoot(){
            string rootPath = "";
            //System.OperatingSystem os = System.Environment.OSVersion;
            //string res = os.ToString();
            string res = System.Net.Dns.GetHostName();
            if(res.StartsWith( "SakuragiMac" )){
                rootPath = "/Users/hayashi/";
                zipSourceFolder = rootPath + "yagyu_settings/data/";
                zipDestFolder = rootPath + "yagyu/yg_chohyo/yagyuKumiai/pyChohyoUpdate/zipStore/";
            } else if(res.StartsWith("dh-ub20"))
            {
                rootPath = "/home/dh/";
                zipSourceFolder = rootPath + "gd_folder/柳生作業用/yagyu_settings/data/";
                zipDestFolder = rootPath + "yagyu/yg_chohyo/yagyuKumiai/pyChohyoUpdate/zipStore/";
            } else if(res.StartsWith("dh2019"))
            {
                rootPath = "c:/Users/daiho/";
                zipSourceFolder = "c:/yagyu_settings/data/";
            } else{
                rootPath = "error in hostname!:" + res;
            }
            return rootPath;
        }
        
        public static string GetZipSourceFolder(){
            if(zipSourceFolder == ""){
                GetLocalRoot();
            }
            return zipSourceFolder;
        }

        public static string GetZipDestFolder(){
            if(zipDestFolder == ""){
                GetLocalRoot();
            }
            return zipDestFolder;
        }
    }
}
