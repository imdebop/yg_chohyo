using System;
using Xunit;
using ygExEPP;

namespace ygUnit.Test
{
    public class UnitTest1
    {
        [Theory(DisplayName = "display aaa")]
        [InlineData("bbb")]
        public void Test1(string s)
        {
            var res = ygUtil.YgUtil.Welcome(s);
            Assert.Equal("Hello yagyu bbb!", res);
            Assert.NotEqual("aaaaaahahahah", res);
        }

        ///*
        [Fact]
        public void TestYgInit()
        {
            var res = ygInit.OsInfo.OsName();
            Assert.Equal("linux", res);
        }
        [Fact]
        public void TestYgInit2()
        {
            var res = ygInit.OsInfo.gdFolderPath();
            Assert.Equal("/home/dh/gd_folder/", res);
        }

        [Fact]
        public void ExEPP()
        {
            ygExEPP.InitData ygIni = new ygExEPP.InitData();
            //new ygExEPP.YgExEPP(ygIni); 
        
        //Given
        
        //When
        
        //Then
        }


        //*/
    }
}
