﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SimaUtil
{
    public static class Menseki
    {

        public static KeisanRes Calc(GenbaBlkRec gbr)
        {
            KeisanRes kRes = new KeisanRes();//計算結果レコード
            kRes.Id = gbr.Id;
            kRes.Meisho = gbr.Meisho;
            kRes.Ketuens = Ketuen(gbr);

            List<KeisanRec> kRs = new List<KeisanRec>();//計算行レコードリストを作成
            int xyNum = gbr.zahyoDecs.Count;//レコード数
            int lastNum = xyNum - 1;
            for(int i = 0; i < xyNum; i++)
            {
                var kR = new KeisanRec();//行レコード
                var zd = gbr.zahyoDecs[i];//座標(decimal)レコード
                kR.Sokuten = $"{zd.Tenban}:{zd.Meisho}";//測点
                kR.X = decimal.Round(zd.X,3);//ミリへ丸め
                kR.Y = decimal.Round(zd.Y,3);
                //前後レコードへのポインタ
                int a = i == 0 ? lastNum : (i - 1);
                int b = i == lastNum ? 0 : i + 1;
                kR.DX = decimal.Round(gbr.zahyoDecs[a].X, 3);
                kR.DX -= decimal.Round(gbr.zahyoDecs[b].X, 3);
                kR.YDX = kR.Y * kR.DX;
                kRes.KeisanRecs.Add(kR);
            }
            decimal wkMen = kRes.KeisanRecs.Sum(x => x.YDX);
            kRes.Hoko = wkMen > 0 ? Hoko.R : Hoko.L;
            kRes.Menseki = Math.Abs(wkMen) / 2;
            if(kRes.Ketuens.Count > 0)
            {
                kRes.Menseki += kRes.Ketuens.Sum(x => x.Men);
            }
            kRes.MenKirisute = decimal.Round((kRes.Menseki - (decimal)0.005), 2);//切捨
            return kRes;
        }

        public static List<Ketuen> Ketuen(GenbaBlkRec gbr)
        {
            List<Ketuen> keList = new List<Ketuen>();
            foreach(Enko en in gbr.enkos)
            {
                Ketuen kt = new Ketuen();
                kt.R = Math.Abs(en.R);
                ZahyoDec zr1 = gbr.zahyoDecs.Where(x => x.Tenban == en.Tenban1).FirstOrDefault();
                //XY p1 = new XY() { X = (double)zr1.X, Y = (double)zr1.Y };
                ZahyoDec zr2 = gbr.zahyoDecs.Where(x => x.Tenban == en.Tenban2).FirstOrDefault();
                //XY p2 = new XY() { X = (double)zr2.X, Y = (double)zr2.Y };
                decimal len = ZahyoCalc.Len(zr1.GetDecXYmm(), zr2.GetDecXYmm());
                double ang = Math.Asin((double)len / 2 / (double)kt.R) * 2;
                double teihen = (double)kt.R * Math.Cos(ang / 2);
                kt.Sankaku =(decimal)teihen * len / 2;
                kt.Ougi =(decimal)(Math.Pow((double)kt.R, 2) * ang / 2);
                kt.Men = kt.Ougi - kt.Sankaku;
                if(en.R < 0) { kt.Men *= -1; }//半径がマイナスなら面積もマイナス
                kt.CL = kt.R * (decimal)ang;
                kt.Gencho = len;
                kt.Ang = (decimal)(180 * ang / Math.PI);
                kt.R = en.R;//Rを絶対値から元に戻す
                keList.Add(kt);
            }
            return keList;
        }
    }

    public class Ketuen
    {
        public decimal R { get; set; }
        public decimal CL { get; set; }
        public decimal Gencho { get; set; }
        public decimal Ang { get; set; }
        public decimal Ougi { get; set; }
        public decimal Sankaku { get; set; }
        public decimal Men { get; set; }
    }

    public class KeisanRes　//計算結果
    {
        public string Id { get; set; }
        public string Meisho { get; set; }
        public decimal Menseki { get; set; }
        public decimal MenKirisute { get; set; }
        public List<Ketuen> Ketuens { get; set; } = new List<Ketuen>();
        public Hoko Hoko { get; set; }
        public List<KeisanRec> KeisanRecs { get; set; } = new List<KeisanRec>();
    }

    public enum Hoko { L, R}//右回り・左回り

    public class KeisanRec
    {//座標をミリ単位で丸めてセット
        public string Sokuten { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public decimal DX { get; set; }
        public decimal YDX { get; set; }

    }
}
