﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;

namespace SimaUtil
{
    public static class ZahyoCalc
    {
        public static AX Hokokaku(XY vec)
        {
            var ax = new AX();
            ax.A = Math.Atan2(vec.Y, vec.X);
            ax.L = Len(vec);
            return ax;
        }

        public static XY Add(XY xy1, XY xy2)
        {
            return new XY()
            {
                X = xy1.X + xy2.X,
                Y = xy1.Y + xy2.Y,
            };
        }

        public static XY Sub(XY p1, XY p2)
        {
            return new XY()
            {
                X = p2.X - p1.X,
                Y = p2.Y - p1.Y,
            };
        }

        public static XY Inv(XY xy)
        {
            return ZahyoCalc.Sub(new XY { X = 0, Y = 0 }, xy);
        }

        public static double Len(XY p1, XY p2)
        {
            XY v = ZahyoCalc.Sub(p1, p2);
            return Math.Sqrt(v.X * v.X + v.Y * v.Y);
        }

        public static double Len(XY v)
        {
            return Math.Sqrt(v.X * v.X + v.Y * v.Y);
        }

        public static decimal Len(DecXY p1, DecXY p2)
        {
            DecXY w = Sub(p1, p2);
            return SqrtDec(w.X * w.X + w.Y * w.Y);
        }

        public static DecXY Sub(DecXY p1, DecXY p2)
        {
            return new DecXY { X = p1.X - p2.X, Y = p1.Y - p2.Y };
        }

        /// <summary>
        /// Decimalで平方根計算
        /// </summary>
        public static decimal SqrtDec(decimal x, decimal? guess = null)
        {
            var ourGuess = guess.GetValueOrDefault(x / 2m);
            var result = x / ourGuess;
            var average = (ourGuess + result) / 2m;

            if (average == ourGuess) // This checks for the maximum precision possible with a decimal.
                return average;
            else
                return SqrtDec(x, average);
        }

        public static decimal SqrtDec__(decimal x, decimal epsilon = 0.0M)
        {
            if (x < 0) throw new OverflowException("Cannot calculate square root from a negative number");

            decimal current = (decimal)Math.Sqrt((double)x), previous;
            do
            {
                previous = current;
                if (previous == 0.0M) return 0;
                current = (previous + x / previous) / 2;
            }
            while (Math.Abs(previous - current) > epsilon);
            return current;
        }

    }


    public struct XY
    {
        public double X { get; set; }
        public double Y { get; set; }
    }

    public struct DecXY
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }
    }

    public struct AX
    {
        public double L { get; set; }
        public double A { get; set; }
    }

    public static class ImCalc
    {
        public static Complex Set(double r, double i) => new Complex(r, i);
        
        public static double Gaikaku(XY v1, XY v2)
        {
            Complex a1 = new Complex(v1.X, v1.Y);
            Complex a2 = new Complex(v2.X, v2.Y);
            Complex res = a2 / a1;
            double p = res.Phase;
            return p;
        }
    }


}
