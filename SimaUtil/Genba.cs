﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace SimaUtil
{
    public class Genba
    {
        SimaRead rSim;

        public Genba(string simPath)
        {
            rSim = new SimaRead(simPath);
        }

        /// <summary>
        /// ブロック・ロットのキー全部
        /// </summary>
        public IEnumerable<string> GetAllBklKeys()
        {
            return rSim.dicBkLot.Keys;
        }

        public IEnumerable<string> GetAllBlockKeys()
        {
            return rSim.dicBkLot.Keys.Where(x => x.Contains("BLOCK"))
                .Select(x => x.Split(":")[1]);
        }

        public IEnumerable<string> GetAllLotKeys()
        {
            Regex regLot = new Regex(@"\dB\d");
            return rSim.dicBkLot.Keys.Where(x => regLot.IsMatch(x))
                .Select(x => x.Split(":")[1]);
        }

        /// <summary>
        /// ブロックデータの取得
        /// </summary>
        public bool HasGenbaBlkRecByMeisho(string meisho)
        {
            return rSim.HasBkLotByMeisho(meisho);
        }
        public GenbaBlkRec GetGenbaBlkRecByMeisho(string meisho)
        {
            BkLotRec br = rSim.GetBkLotByMeisho(meisho);
            return ZahyoDecs(br);
        }

        public GenbaBlkRec ZahyoDecs(BkLotRec bkr)
        {
            GenbaBlkRec gbr = new GenbaBlkRec()
            {
                Id = bkr.Id,
                Meisho = bkr.Meisho,
            };

            int kessenLen = bkr.Kessen.Count; 
            for(int i = 0; i < kessenLen; i++)
            {
                string k = bkr.Kessen[i];
                List<int> enkoIdx = new List<int>();
                //結線データより点番・名称をセット
                string[] ar = k.Split(",");
                string dataID = ar[0];
                string tenban = ar[1];
                var zd = new ZahyoDec()
                {
                    Tenban = ar[1],
                    Meisho = ar[2],
                };

                if(dataID == "C01")
                {
                    enkoIdx.Add(i);//円弧のレコード番号を保存
                }
                else
                {
                    ZahyoRec r = rSim.GetZahyoRecByMeisho(zd.Meisho);
                    string[] arZ = r.Data.Split(",");
                    zd.X = decimal.Parse(arZ[3]);
                    zd.Y = decimal.Parse(arZ[4]);
                    gbr.zahyoDecs.Add(zd);//座標レコードを追加
                }

                //円弧レコードを処理//////////////////////////////
                if (enkoIdx.Count > 0)
                {
                    foreach(int ii in enkoIdx )
                    {//円弧レコードを前後のレコードの点番と一緒に作成

                        int a = 0, b = 0;//前後レコードのポインタ
                        int endId = kessenLen - 1;
                        if(ii == 0)//先頭レコードが円弧
                        {
                            a = endId;
                            b = ii + 1;
                        }else if(ii == endId)//最後が円弧
                        {
                            a = ii - 1;
                            b = 0;
                        }
                        else//円弧が中間
                        {
                            a = ii - 1;
                            b = ii + 1;
                        }
                        var en = new Enko();//円弧レコード生成
                        en.R = decimal.Parse( bkr.Kessen[ii].Split(",")[2] );
                        en.Tenban1 = bkr.Kessen[a].Split(",")[1];
                        en.Tenban2 = bkr.Kessen[b].Split(",")[1];
                        gbr.enkos.Add(en);
                    }
                }
            }


            return gbr;

        }


    }

    ////////////////////////////////////////////////////////////////////////////
    // 各レコード定義//////////////////////////////////////////////////////////

    public class GenbaBlkRec
    {
        public string Id { get; set; }
        public string Meisho { get; set; }
        public List<ZahyoDec> zahyoDecs = new List<ZahyoDec>();
        public List<Enko> enkos = new List<Enko>();
    }

    public class Enko
    {
        public string Tenban1 { get; set; }
        public string Tenban2 { get; set; }
        public decimal R { get; set; }
        public decimal Menseki { get; set; }
    }


    /// <summary>
    /// 座標値を数値(Decimal)で保持
    /// </summary>
    public class ZahyoDec : ZahyoRec
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public DecXY GetDecXYmm()
        {
            return new DecXY { X = decimal.Round(X, 3) , Y = decimal.Round(Y,3) };
        }
    }

}
