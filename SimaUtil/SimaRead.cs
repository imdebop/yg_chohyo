﻿using System;
using System.Collections.Generic;
using System.Text;
using yg_chohyo.Data;
using System.IO;
using ChkKasanezu;


namespace SimaUtil
{
    public class SimaRead
    {
        //今のところ路線データ（"Z00"以降）は無視する

        public Dictionary<string, ZahyoRec> dicZahyo = new Dictionary<string, ZahyoRec>();
        public Dictionary<string, BkLotRec> dicBkLot = new Dictionary<string, BkLotRec>();
        
        public bool HasBkLotByMeisho(string kukakuMei)
        {
            string key = $"M:{kukakuMei}";
            return dicBkLot.ContainsKey(key);
        }
        public BkLotRec GetBkLotByMeisho(string kukakuMei)
        {
            string key = $"M:{kukakuMei}";
            return dicBkLot[key];
        }

        public bool HasZahyoRecByMeisho(string meisho)
        {
            string key = $"M:{meisho}";
            return dicZahyo.ContainsKey(key);
        }
        public ZahyoRec GetZahyoRecByMeisho(string meisho)
        {
            string key = $"M:{meisho}";
            return dicZahyo[key];
        }

        public SimaRead(string simaPath)
        {
            string[] simData = FileUtil.GetByLines(simaPath); //s-jisファイルを読み込み
            //座標データを処理 -> dicZahyo
            foreach (string r in simData)
            {
                string[] recArr = r.Replace(" ", "").Split(",");
                if (recArr[0] == "A01")
                {
                    ZahyoRec zr = new ZahyoRec
                    {
                        Tenban = recArr[1],
                        Meisho = recArr[2],
                        Data = r,
                    };

                    dicZahyo[$"T:{zr.Tenban}"] = zr;
                    dicZahyo[$"M:{zr.Meisho}"] = zr;
                }
            }
            //街区・画地データを処理
            string currBkId = "";
            string currBkMei = "";
            foreach (string r in simData)
            {
                string[] recArr = r.Replace(" ", "").Split(",");
                if (recArr[0] == "D00")
                {
                    BkLotRec br = new BkLotRec
                    {
                        Id = recArr[1],
                        Meisho = recArr[2],
                        Header = r,
                        Kessen = new List<string>(),
                    };

                    currBkId = $"I:{br.Id}";
                    currBkMei = $"M:{br.Meisho}";
                    dicBkLot[currBkId] = br;
                    dicBkLot[currBkMei] = br; //結線レコードは持たない
                    continue;
                }
                switch (recArr[0])
                {
                    case "B01":
                    case "C01":
                        dicBkLot[currBkId].Kessen.Add(r);
                        break;
                    default:
                        break;
                }

            }
        }
    }

    /// <summary>
    /// 座標の素のsimaレコード
    /// </summary>
    /*
    public class ZahyoRec
    {
        public string Tenban { get; set; }
        public string Meisho { get; set; }
        public string Data { get; set; }
    }


    public class BkLotRec
    {
        public string Id { get; set; }
        public string Meisho { get; set; }
        public string Header { get; set; }
        public List<string> Kessen { get; set; }
    }


    public class KessenDec
    {
        public string Tenban { get; set; }
        public string Meisho { get; set; }
    }
    */

}
