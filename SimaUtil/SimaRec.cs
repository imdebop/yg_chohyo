﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimaUtil
{
    /// <summary>
    /// 座標の素のsimaレコード
    /// </summary>
    public class ZahyoRec
    {
        public string Tenban { get; set; }
        public string Meisho { get; set; }
        public string Data { get; set; }
    }


    public class BkLotRec
    {
        public string Id { get; set; }
        public string Meisho { get; set; }
        public string Header { get; set; }
        public List<string> Kessen { get; set; }
    }


    public class KessenDec
    {
        public string Tenban { get; set; }
        public string Meisho { get; set; }
    }


}
