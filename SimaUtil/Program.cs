﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ChkKasanezu;
using System.Diagnostics;

namespace SimaUtil
{
    /// <summary>
    /// 底地チェックで不足している区画simaデータを他社受け取りsimaから抽出する
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            string simaPath = "c:/yagyu/sokochi/sima/柳生川区画DATA.sim";
            //string simaRefPath = "c:/yagyu/sokochi/sima/柳生川南部地区座標データ20200417.sim";
            string blockExt = "c:/yagyu/sokochi/sima/ブロック抽出.txt";
            string blockAdd = "c:/yagyu/sokochi/sima/ブロック追加.txt";

            var simaIn = new SimaRead(simaPath);
            //var simaRef = new SimaRead(simaRefPath);
            var exSm = new ExtractSima(simaIn);

            List<string> bkExt = new List<string>();
            Debug.WriteLine($"\n抽出するsimaファイル［{simaPath}］");
            foreach(string r in FileUtil.GetByLines(blockExt))
            {   //抽出すべきブロック名を読み込み
                if(r == "" || r.Substring(0,1) == "#"){ continue; }
                string[] arr = r.Split(":");
                string bkl = arr[1];
                if (exSm.SetBk(bkl))
                {
                    
                }else
                {
                    Debug.WriteLine($"{bkl} not found");
                }
                //bkExt.Add(arr[1]);
            }
            List<string> outList = new List<string>();
            foreach(string s in exSm.extZahyo)
            {
                outList.Add(s);
            }
            foreach(BkLotRec r in exSm.extBlock)
            {
                outList.Add(r.Header);
                foreach(string s in r.Kessen)
                {
                    outList.Add(s);
                }
                outList.Add("D99,");
            }

            File.WriteAllLines(blockAdd, outList);
        }
    }

    public class ExtractSima
    {
        private SimaRead Sm;
        //private SimaRead SmRef;
        public List<string> extZahyo = new List<string>();
        public List<BkLotRec> extBlock = new List<BkLotRec>();


        public ExtractSima(SimaRead sm)
        {
            Sm = sm;
            //SmRef = smRef;
        }

        string StoreBk { get; set; }
        public bool SetBk(string bk)
        {
            string bkMeisho = $"M:{bk}";
            if (Sm.dicBkLot.ContainsKey(bkMeisho))
            {
                StoreBk = bk;
                BkLotRec bkl = Sm.dicBkLot[bkMeisho];
                extBlock.Add(bkl);
                setZahyo(bkl);
                return true;
            }
            else
            {
                return false;
            }
        }

        void setZahyo(BkLotRec bkl)
        {
            foreach(string r in bkl.Kessen)
            {
                string[] ar = r.Split(",");
                if (ar[0] == "C01")
                {
                    extZahyo.Add(r);
                }else
                {
                    string tenban = ar[1];
                    string simZahyo = Sm.dicZahyo[$"T:{tenban}"].Data;
                    extZahyo.Add(simZahyo);
                }
            }
        }
    }



}
