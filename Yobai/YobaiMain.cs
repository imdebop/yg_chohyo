﻿using System;
using ygUtil;
using ygInit;
using ygExEPP;
using ygExBase;
using System.Collections.Generic;
using ygExBase.Interfaces;

namespace Yobai
{
    public class YobaiMain
    {
        static void Main(string[] args){
            mainProc();
        }
        static ExRepo mainProc()
        {
            ExRepo repoYobai;
            //var res = YgUtil.Welcome("yobai");
            //Console.WriteLine(res);
            string expath = OsInfo.ygYobaiPath();
            expath += "潰地シート.xlsx";
            //Console.WriteLine(expath);

            //YobaiRec ybr = new YobaiRec();
            //YobaiRepo ybrepo = new YobaiRepo();
            YobaiDefRec ybDR = new YobaiDefRec();
            repoYobai = new ExRepo(expath, "求積一覧表", ybDR);
            YgExEPP ygex = new YgExEPP(repoYobai);
            //var ygEB = new YgExBase(ybr);
            int r = 6;
            for(int i=1; i < 16; i++){
                Console.WriteLine(ygex.Repo().GetTblData(r,i));
            }
            return ygex.Repo();
                
        }

        public static ExRepo RunTest(){
            ExRepo repo = mainProc();
            //Console.WriteLine(repo.GetWsRawData(11,5));
            return repo;
        }
    }

    class RepoYobai{
    }

    class YobaiDefRec: IExDefRec{
        public int StartRow(){ return 3;}
        public int LastRow(){return 10;}
        public int StartCol(){return 2;}
        public int LastCol(){
            int n = this.ColDef().Count;
            return this.StartCol() + n - 1;
        }
        public List<string> ColDef(){

            List<string> data = new List<string>{
                //1番目　カラム番号
                //2     N=不使用,S=文字列,
                //      数字=小数桁数(0は整数, 9はフル桁),
                //3　   フィールド名称
                //4     コメント
                "01:N:Memo:記事",
                "02:S:ID:電算用",
                "03:S:RosenNo:",
                "04:S:IsMachi:単独／まち交",
                "05:S:GenChimoku:現況地目",
                "06:S:Azamei:字名",
                "07:S:Chiban:地番",
                "08:S:Shoyusha:所有者",
                "09:S:Kubun:区分",
                "10:0:Tanka:単価",
                "11:2:Meneki:面積",
                "12:2:BubunMen:部分面積",
                "13:0:Kakaku:価格",
                "14:S:Nendo:年度",
                "15:S:Kurikoshi:繰越",
            };
            return data;
        }
    }
}
