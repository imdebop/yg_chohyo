﻿using System;
using System.Collections.Generic;
using System.Linq;
using yg_chohyo.Dapper;
using yg_chohyo.Data;
using SimaUtil;
using System.Diagnostics;


namespace MensekiChk
{
    class Program
    {
        //チェック用管理データ辞書を生成
        static Dictionary<string, MenChk> DicKanriMen = new Dictionary<string, MenChk>();
        //ブロックと画地面積の合計チェック用
        static Dictionary<string, BlockMenChk> DicBlockMenChk = new Dictionary<string, BlockMenChk>();

        static void Main(string[] args)
        {
            string simaPath = "c:/yagyu/sokochi/sima/柳生川区画DATA.sim";
            var gb = new Genba(simaPath);

            BlockAdjust bAdjst = new BlockAdjust();

            foreach(string bk in gb.GetAllBlockKeys())
            {
                GenbaBlkRec gbr = gb.GetGenbaBlkRecByMeisho(bk);
                decimal men = Menseki.Calc(gbr).MenKirisute;
                Debug.WriteLine(bk);
                string key = bk.Replace("BLOCK", "");
                //9-1B => 9B 等の修正も加える
                string keyAdj = bAdjst.Redirect(key);
                if (DicBlockMenChk.ContainsKey(keyAdj))
                {
                    DicBlockMenChk[keyAdj].SimMenseki += men;
                }
                else
                {
                    DicBlockMenChk[keyAdj] = new BlockMenChk() {
                        Block = keyAdj,
                        SimMenseki = men,
                    };
                }
            }

            foreach (Kanchi kan in DapperKanchi.GetAll())
            {
                string bk = kan.KanCode.Substring(0, 2).TrimStart('0');
                DicBlockMenChk[bk].KanriMenseki += decimal.Parse(kan.Menseki);
            }

            foreach(BlockMenChk bmc in DicBlockMenChk.Values)
            {
                string s = $"{bmc.Block}\t{bmc.SimMenseki.ToString("#.##")}\t{bmc.KanriMenseki.ToString("#.##")}";
                Debug.WriteLine(s);
            }

            ChkBetweenKakuchi();

        }

        /// <summary>
        /// 管理データとsimaの画地面積を照合
        /// </summary>
        static void ChkBetweenKakuchi()
        { 
            string simaPath = "c:/yagyu/sokochi/sima/";

            foreach(Kanchi k in DapperKanchi.GetAll())
            {
                var mcRec = new MenChk();
                mcRec.KanriMen = decimal.Parse(k.Menseki);
                mcRec.SimaMen = 0;
                DicKanriMen[k.KanCode] = mcRec;
            }

            //面積計算結果の出力ファイルパス
            string outPath = simaPath + "lotMenComp.txt";
            List<string> menCompLi = new List<string>();

            string fpath = simaPath + "yaguAll.txt";
            var gb = new Genba(fpath);

            foreach(string kancode in DicKanriMen.Keys)
            {
                string bkl = Util.Kancode2B_L_str(kancode).Replace(" ", "").Replace("L","");
                if (!gb.HasGenbaBlkRecByMeisho(bkl)) { continue; }//換地コード存在チェック
                
                GenbaBlkRec gbr = gb.GetGenbaBlkRecByMeisho(bkl);
                KeisanRes kRes = Menseki.Calc(gbr);
                DicKanriMen[kancode].SimaMen = kRes.Menseki;
            }

            foreach( KeyValuePair<string, MenChk> a in DicKanriMen.OrderBy(x => x.Key).Select(x => x))
            {
                string wk = $"{a.Key}\t{a.Value.KanriMen}\t{a.Value.SimaMen}";
                menCompLi.Add(wk);
                //Debug.WriteLine(a.Key);

            }

            System.IO.File.WriteAllLines(outPath, menCompLi);
        }
    }


    public class MenChk
    {
        public Shubetu Shubetu { get; set; }//ブロック・ロットの別
        public string Meisho { get; set; }// ブロック(B:*)またはロット(L:*B*-*)名称
        public decimal KanriMen { get; set; }
        public decimal SimaMen { get; set; }
    }

    public enum Shubetu { B, L}

    public class BlockMenChk
    {
        public string Block { get; set; }
        public decimal SimMenseki { get; set; }
        public decimal KanriMenseki { get; set; } = 0;
    }


    public class BlockAdjust
    {
        Dictionary<string, string> DicRedirect = new Dictionary<string, string>();

        public BlockAdjust()
        {
            foreach(string bkl in reDirect)
            {
                string[] wk = bkl.Replace("BLOCK", "").Split(",");
                DicRedirect[wk[0]] = wk[1]; 
            }
        }

        public string Redirect(string bkl)
        {
            return DicRedirect.ContainsKey(bkl) ? DicRedirect[bkl] : bkl;
        }

        string[] reDirect = new string[]
        {
            "9-1BLOCK（公）,999",
            "9-2BLOCK,9",
            "30-1BLOCK（公）,999",
            "30-2BLOCK,30",
            "33-1BLOCK,33",
            "33-5BLOCK,33",
            "33-3BLOCK,33",
            "39-3BLOCK,39",
            "41BLOCK（学）,41",
            "48BLOCK（公）,48",
            "63-1BLOCK（公）,999",
            "63-2BLOCK,63",
            "75-1BLOCK（池）,75",
            "75-2BLOCK,75",
            "84-1BLOCK（公）,84",
            "84-2BLOCK（池）,84",
            "91-1BLOCK,91",
            "91-2BLOCK(緑),999",
            "82BLOCK　3部分,999",
            "82BLOCK　2-1部分,999",
        };
    }


}
