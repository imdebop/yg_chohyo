import { PrismaClient } from '@prisma/client'
//import aaa from './src/loadCsv'
import exjs from './src/loadXlsx'

const prisma = new PrismaClient()

// A `main` function so that you can use async/await
async function main() {
  // ... you will write your Prisma Client queries here
  //  const allUsers = await prisma.user.findMany({
  //  include: { posts: true },
  //})
  // use `console.dir` to print nested objects
  //console.dir(allUsers, { depth: null })
  //console.log(aaa())
  exjs().then(val => { console.log(val)})
}

main()
  .catch(e => {
    throw e
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
