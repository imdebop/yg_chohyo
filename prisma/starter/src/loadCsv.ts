import exp from "constants";

const fs = require('fs')
const parse = require('csv-parse/lib/sync')
const file = './data/tubureRaw.csv'
const sqlite3 = require('sqlite3')
const db = new sqlite3.Database('./prisma/dev.db')


function aaa(){
    let data = fs.readFileSync(file)
    let res: string[] = parse(data,{
        delimiter: "\t"
    })
    let recs = res.map(function(rec){
        //removing 9th empty column
        return rec.slice(0, 8).concat(rec.slice(9))
    })

    let insQuery = "INSERT INTO User values (?, ?, ?)"
    let statement = db.prepare(insQuery)
    let a: string[] = ["aaa@bbb", "3", "beth"]
    statement.run(a)
    statement.finalize();

    return recs[2];
}

export default aaa;
