﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
//using yagyuData;

namespace localizeCs
{
    public static class ProgramPath
    {
        static ProgramPath(){   //プログラム開始時に自動的に初期化される
            string path = LocalPath.locationTxtPath();
            SetValues(path);
        }

        static string exPath { get; set; } = "";
        internal static Dictionary<string, string> dicPaths = new Dictionary<string, string>();
        
        public static Dictionary<string, string> getPathDictionary(){
            return dicPaths;
        }

        public static string GyomuDbPath(){
            return dicPaths[ "gyomuDb" ];
        }

        public static string Excel()
        {
            return dicPaths[ "excel"];
        }
        
        public static string MainData()
        {
            return dicPaths[ "mainData"];
        }

        public static string Shoyu()
        {
            return dicPaths[ "shoyu"];
        }

        public static void SetValues(string locationTxtPath)
        {
            foreach(string rec in File.ReadAllLines(locationTxtPath)){
                if(rec.StartsWith("#")){continue;}
                string[] ss = rec.Split('=');
                if( ss[0] != "")
                {
                    dicPaths[ss[0]] = ss[1];
                }
                //if(ss[0] == "excel")
                //{
                //    exPath = ss[1];
                //}
            }
            //System.Windows.MessageBox.Show("initialize program paths.");
        }

    }
}
