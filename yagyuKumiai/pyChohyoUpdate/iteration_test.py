from dataclasses import dataclass

class Test():
    @dataclass
    class aaa:
        pass

    def __init__(self):
        pass


    def __getitem__(self, n):
        if n > 3:
            raise IndexError
        return n * 2
 
#t = Test()
# 
#for n in t:
#    print(n)
    