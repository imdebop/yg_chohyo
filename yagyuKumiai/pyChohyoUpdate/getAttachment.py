import win32com.client #pip install pypiwin32 to work with windows operating sysytm
from locationsPath import *
import locationsPath
from getLastUpdateInfo import *
import os

class GetAttachment:
    outlook = None
    inbox = None
    messages = None
    message = None
    attachment = None

    @classmethod
    def setup(cls, subFolderName: str):
        try:
            cls.outlook = win32com.client.GetActiveObject("Outlook.Application")
        except:
            cls.outlook = win32com.client.Dispatch("Outlook.Application")

        cls.inbox = cls.outlook.GetNamespace("MAPI").GetDefaultFolder(6).Folders[subFolderName]

        return cls.inbox.Items


    @classmethod
    def inboxText(cls, lastUpdate: str):
        subFoldername = "証明書更新"
        cls.messages = GetAttachment.setup(subFoldername)

        print("フォルダ名:" + subFoldername)
        for cls.message in cls.messages:
            sbj = cls.message.Subject
            digits = GetLastUpdateInfo.zipName2digits(sbj)
            if digits != "":
                print(digits)
                if digits > lastUpdate:
                    cls.zipStore(cls.message)

    @classmethod
    def zipStore(cls, message):
        path =  LocationsPath.getAttachedZipStoreFolder()
        print("new:" + message.Subject)
        cls.attachment = message.Attachments.Item(1)
        cls.attachment.SaveAsFile(os.path.join(path, str(cls.attachment)))

    