from typing import NamedTuple
from locationsPath import *
from glob import glob
import re
from dataclasses import dataclass

class GetLastUpdateInfo:
    #フォルダ中のzipファイルから、最新のものの「日付＋連番」を返す
    @classmethod
    def get(cls) -> str:
        zipStoreFolder = LocationsPath.getAttachedZipStoreFolder()
        print(zipStoreFolder)
        wk = glob(zipStoreFolder + "\*")
        zipDigitsList = []
        for s in wk:
            digits = cls.zipName2digits(s)
            if digits != "":
                zipDigitsList.append(digits)
        #print(zipDigitsList)
        if len(zipDigitsList) > 0:
            #print( max(zipDigitsList) )
            return max(zipDigitsList)


    #ジップファイル名から「日付＋連番」の文字列を返す
    @classmethod
    def zipName2digits(cls, name: str) -> str:
        m = re.search('(\d{8})帳票更新大地(\d\d)',name)
        if m is None:
            return ""
        else:
            return "".join(m.groups())