class LocationCustom:
    locationTxtPath = "./locationCustom.txt"

    @classmethod
    def getLocationTxtPath(cls) -> str:
        return cls.locationTxtPath