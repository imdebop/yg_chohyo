from types import ClassMethodDescriptorType
from zipfile import ZipFile
from pathlib import Path
import os
import shutil
import zipfile
from locationsPath import *
from dataclasses import dataclass, field
from typing import List
from getLastUpdateInfo import *

class Unzip2folder:
    @dataclass
    class PathParam:
        zipPath: str = ""
        unzipFolder: str = os.path.join(LocationsPath.getUnzipFolder(), "wkdir")
        zipFiles: List[str]=field(default_factory=list)
        selectedZipFilePath: str = ""

        def selectedZipName(self):
            s = os.path.basename(self.selectedZipFilePath)
            return s.split(".")[0]
        
        def updateYMD(self):
            return self.selectedZipName()[:8]


    def __init__(self) -> None:
        print("init called ******************")
        self.pParam = Unzip2folder.setPath()

    def __getitem__(self, i):
        if i > len(self.pParam.zipFiles) - 1: raise IndexError()
        if os.path.isdir(self.pParam.unzipFolder):
            shutil.rmtree(self.pParam.unzipFolder)
        os.makedirs(self.pParam.unzipFolder)
        self.pParam.selectedZipFilePath = os.path.join(self.pParam.zipPath, self.pParam.zipFiles[i])
        Unzip2folder.unzip(self.pParam)
        return self.pParam

    @classmethod
    def do(cls):
        pParam = cls.setPath()
        for f in pParam.zipFiles:
            #cls.setExtractFolder(pParam, f)
            if os.path.isdir(pParam.unzipFolder):
                shutil.rmtree(pParam.unzipFolder)
            os.makedirs(pParam.unzipFolder)
            pParam.selectedZipFilePath = os.path.join(pParam.zipPath, f)
            cls.unzip(pParam)
        return

    @classmethod
    def unzip(cls, pParam: PathParam):
        with ZipFile(pParam.selectedZipFilePath) as zf:
            for fn in zf.namelist():
                zf.extract(fn, pParam.unzipFolder)
                #extracted_path = Path(zf.extract(fn, tempPath))
                #fnwk = os.path.join(tempPath, fn.encode("cp437").decode("utf-8")) 
                #extracted_path.rename( fn.encode("cp437").decode("utf-8"))
                #extracted_path.rename( fnwk)


    @classmethod
    def setPath(cls) -> PathParam:
        pParam = cls.PathParam() #new
        pParam.zipPath = LocationsPath.getAttachedZipStoreFolder()

        contents = os.listdir(pParam.zipPath) #includes directories and files
        #extract files
        wklist = [f for f in contents if os.path.isfile( os.path.join(pParam.zipPath, f))]
        #「帳票更新大地」形式か
        pParam.zipFiles = [f for f in wklist if GetLastUpdateInfo.zipName2digits(f) != ""]
        return pParam

    ### これは必要なくなった
    @classmethod
    def setExtractFolder(cls, pParam: PathParam, f: str):
        path = os.path.join(pParam.zipPath, f)
        #zipName = os.path.basename(path).split(".")[0]
        zipName = f.split(".")[0]
        #pParam.zipFile = pParam.zipPath + "20210921帳票更新大地01.zip"
        pParam.selectedZipFilePath = os.path.join(LocationsPath.getUnzipFolder(), zipName)
        #unzippedFolders = InspectUpData.getUnzippedFolders(unzipFolder)
        print("zipFile=" + path)
        print("zipName=" + zipName)
        print("unzipFolder=" + pParam.selectedZipFilePath)
        print("\n")

#Unzip2folder.do(zipFile, unzipFolder)

 