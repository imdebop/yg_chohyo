from typing import List

class FolderNames:
    foldernames: List = [
        "仮換地証明データ",
        "個別重ね図Docu_公図入・名前入",
        "個別重ね図Docu_公図入・名前無",
        "個別重ね図Docu_公図無・名前入",
        "個別重ね図Docu_公図無・名前無",
    ]
    @classmethod
    def get(cls):
        return cls.foldernames

class DestPath:
    @classmethod
    def get():
        pass