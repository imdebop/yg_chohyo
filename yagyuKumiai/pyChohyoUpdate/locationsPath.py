from typing import Dict
from locationCustom import *

class LocationsPath:
    #pathは locationCustom.txt
    locationTxtPath = LocationCustom.getLocationTxtPath()
    dictPaths: Dict

    @classmethod
    def getAttachedZipStoreFolder(cls) -> str:
        return cls.getPathFromDic( "attachedZipStoreFolder" )

    @classmethod
    def getUnzipFolder(cls) -> str:
        return cls.getPathFromDic( "unzipFolder" )

    @classmethod
    def getUpdateTargetPath(cls) -> str:
        return cls.getPathFromDic("updateTarget")
        
    ##########################################################
    @classmethod
    def getPathFromDic(cls, id) -> str:
        if id in cls.dictPaths:
            return cls.dictPaths[id]
        else:
           cls.errorProc(id) 

    @classmethod
    def errorProc(cls, msg: str):
        print("{0} Not in locationTest.txt".format(msg))
        raise ValueError(msg)

    def setDictPaths(path: str) -> Dict:
        #pathは locationCustom.txt
        wkDict = {}
        with open(path, encoding="utf8", newline="") as f:
            lines = f.readlines()
            r: str
            for r in lines:
                r = r.strip()
                if r.startswith("#"):
                    continue
                elif "=" in r:
                    k, v = r.split("=")
                    wkDict[k] = v
        
        return wkDict


    
    dictPaths = setDictPaths(locationTxtPath)
    pass
