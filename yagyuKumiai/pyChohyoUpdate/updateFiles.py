from dataclasses import dataclass
from posixpath import basename
from typing import List
from locationsPath import *
from inspectUpData import *
from fileMisc import *
from unzip2folder import Unzip2folder
import os
import glob
import shutil



class UpdateFiles:
    @dataclass
    class CpParam:
        updateYMD: str = ""
        srcRoot: str = ""
        destRoot: str = ""
        dataType: str = ""
        dataExt: str = ".xdw"

        def getSrcFolder(self) -> str:
            return os.path.join(self.srcRoot, self.dataType)
        
        def getDestFolder(self) -> str:
            return os.path.join(self.destRoot, self.dataType)

    def __init__(self) -> None:
        for s in FolderNames.get():
            #更新データフォルダを順にチェック
            print(s)
        pass


    @classmethod
    def do(cls, uf: Unzip2folder):
        cpp = cls.CpParam()
        #更新用zipデータを順に処理する
        cpp.destRoot = LocationsPath.getUpdateTargetPath()
        print("\033[32mdestRoot = \033[37m" + cpp.destRoot)
        pp: Unzip2folder.PathParam
        for pp in uf:
            cpp.srcRoot = pp.unzipFolder
            print( "\033[32munzip folder(srcRoot) = \033[37m" + cpp.srcRoot)
            #zipデータの展開処理を行う
            print( "\033[32mselected zip path = \033[37m" + pp.selectedZipFilePath)

            cpp.updateYMD = pp.updateYMD()
            s: str
            for s in FolderNames.get():
                cpp.dataType = s
                if s.startswith("仮換地"):
                    pass
                else:
                    cls.copyEachData(cpp)
    @classmethod
    def replaceDB(cls, cpp: CpParam):
        dbname = "gyomu.sqlite"
        srcPath = os.path.join(cpp.getDestFolder(), dbname)
        destPath = os.path.joinj(cpp.getDestFolder(), dbname)

        pass

    @classmethod
    def copyEachData(cls, cpp: CpParam):
        print(cpp.updateYMD)
        searchStr = os.path.join(cpp.getSrcFolder(), "*" + cpp.dataExt)
        print( "\033[32msource folder = \033[37m" + cpp.getSrcFolder())
        print( "\033[32mdest folder = \033[37m" + cpp.getDestFolder())
        files = glob.glob(searchStr)
        destFolder = cpp.getDestFolder()
        for s in files:
            #ファイルパスのファイル名に日付を付加して返す
            basename = os.path.basename(s)
            wk = basename[:-len(cpp.dataExt)] + "_" + cpp.updateYMD + cpp.dataExt
            destPath = os.path.join(cpp.getDestFolder(), wk)
            print("\033[35msrc  file = \033[37m" + s)
            print("\033[35mdest file = \033[37m" + destPath)

            #古いファイルのrename
            cls.mvOldFile(wk, cpp)


    @classmethod
    def mvOldFile(cls, fpath, cpp: CpParam) -> str:
        #古いファイルを削除
        globWk = os.path.basename(fpath)[:-len("_yyyymmdd.xwd")]
        if globWk == "":
            raise Exception
        globWk += "*"
        globPath = os.path.join(cpp.getDestFolder() , globWk)
        print("\033[33mglob path = \033[37m" + globPath)
        files = glob.glob(globPath)
        for fp in files:
            base = "xxx" + os.path.basename(fp)
            dirname = os.path.dirname(fp)
            fpMarked = os.path.join(dirname, base)
            print("\033[33mrename before = \033[37m" + fp)
            print("\033[33mrename after  = \033[37m" + fpMarked)
            #print(fpMarked)
            continue
            shutil.move(fp, fpMarked)
        pass


