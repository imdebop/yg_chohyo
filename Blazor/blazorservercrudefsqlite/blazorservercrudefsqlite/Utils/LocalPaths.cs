using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace blazorservercrudefsqlite.Utils
{
    public class LocalPaths
    {
        static Dictionary<string, string> dicLocalPath; // = new Dictionary<string, string> {};
        
        static LocalPaths()
        {
            dicLocalPath = localizeCs.ProgramPath.getPathDictionary();
            //foreach(string rec in File.ReadAllLines("localPaths.txt")){
            //    if (rec.Contains("=")){
            //        string[] ss = rec.Split("=");
            //        dicLocalPath[ss[0]] = ss[1];
            //    }
            //}
        }

        public static List<string> GetRecList(){
            List<string> wk = new List<string>();
            foreach(KeyValuePair<string, string> item in dicLocalPath){
                wk.Add(item.Key + "=" + item.Value);
            }
            return wk;
        }

        public static string GetPath(string folderName){
            string res = dicLocalPath[folderName];
            return res;
        }
    }
}