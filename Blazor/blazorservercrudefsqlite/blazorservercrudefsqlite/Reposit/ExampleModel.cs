using System.ComponentModel.DataAnnotations;

public class ExampleModel
{
    [Required]
    [StringLength(100, ErrorMessage = "Name is too long.")]
    public string Name { get; set; }
}