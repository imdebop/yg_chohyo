using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using blazorservercrudefsqlite.Utils;

namespace blazorservercrudefsqlite.Reposit{
    public class SetUpdateFolders{

       static readonly string jobId = "帳票更新大地";

         ///<summary>更新用フォルダーを作り、親フォルダーパスを返す</summary>
        public static string AddUpdateFolder(DirInfo di){
            string[] wk = DateTime.Now.ToString().Split(" ")[0..2]; 
            string date = wk[0].Replace("/","");
            int n = checkUpdateExists(di, date);
            n++;
            string suffix = n.ToString("D2");
            string parentFolderName = date + jobId + suffix;
            return addUpdateFolder(di,parentFolderName);
        }

        /// <summary>返り値は現在の最大suffix</summary>
        static int checkUpdateExists(DirInfo di, string date){
            List<int> folderSuffixs = new List<int>(){0};
            //Regex rgx = new Regex(@"帳票更新大地(\d\d)$");
            foreach(DirInfo d in di.SubDirs){
                string pattern = date + jobId + @"(\d\d)$";
                Match m = Regex.Match(d.DirPath, pattern);
                if(!m.Success){continue;}
                string v = m.Groups[1].Value;
                folderSuffixs.Add(int.Parse(v));
            }
            return folderSuffixs.Max();
        }

        static string addUpdateFolder(DirInfo di, string parentFolder){
            string parentPath = Path.Combine(di.RootDir, parentFolder);
            Directory.CreateDirectory(parentPath);
            return parentPath;
        }
    }

}