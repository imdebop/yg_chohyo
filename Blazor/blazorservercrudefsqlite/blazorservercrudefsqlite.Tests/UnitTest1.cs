using System.Text.RegularExpressions;
using Xunit;
using blazorservercrudefsqlite.Reposit;
using blazorservercrudefsqlite.Utils;

namespace blazorservercrudefsqlite.Tests
{
    public class UnitTest1
    {
        [Fact] //更新用データのフォルダを作成する
        public void Test1()
        {
            DirInfo di = NestedFileList.Get(EnvCommon.path2mailFolder());
            string res = SetUpdateFolders.AddUpdateFolder(di);
            Assert.Equal("日付・連番の入ったフォルダパスが返ればＯＫ", res);
        }
    }
}
