using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;
using yg_chohyo;
using yg_chohyo.Dapper;
using System.Windows;


namespace tests
{
    public class UnitTest1
    {
        private readonly ITestOutputHelper _output;

        public UnitTest1(ITestOutputHelper output)
        {
            _output = output;
        }

        
        [Theory(DisplayName = "管理データロード")]
        [InlineData("kanchi")]
        [InlineData("shoyu")]
        [InlineData("kyoyu")]
        [InlineData("juzen")]
        [InlineData("souzoku")]
        [InlineData("kumi")]
        /*
        */
        public void LoadTest(string table)
        {
            yg_chohyo.ReadExSheet.Start(table);


        }
        
        /*




        [Fact(Skip = "kyoyu read")]
        public void Test1()
        {
            yg_chohyo.Repo.RepoKyoyu aaa = new yg_chohyo.Repo.RepoKyoyu("2004");
            //_output.WriteLine(aaa.DataRecs[0].TokiName);
            Assert.NotEqual("田中", aaa.DataRecs[0].TokiName.Substring(0,2));
            Assert.NotEqual("足立", aaa.DataRecs[0].TokiName.Substring(0,2));

            return;
            //yg_chohyo.ReadExSheet.Start("shoyu");
            //yg_chohyo.ReadExSheet.Start("kyoyu");
            //return;

            Test_KanchiShomei_KumiRecGet.Test_KumiRecGet();
            return;

        }
        */
        //換地証明作成テスト
        /*/
        [Fact]
        public void Test2()
        {
            Test_KanchiShomei_KumiRecGet.Test_KumiRecGet();
        }
        //*/

        /*/
        //保留地証明作成テスト
        [Fact(DisplayName = "kanchishomei create")]
        public void HoryuShomei()
        {
            Test_HoryuchiShomei.Write("25003002");
        }
        /*/

        /*
        //従前地面積　整数、小数の編集テスト
        [Theory]
        [InlineData("0","01", "0:00")]
        //
        [InlineData("0.1","01", "0:01")]
        [InlineData("1","01", "1:01")]
        [InlineData("1.01","01", "1:01")]
        [InlineData("1.10","01", "1:10")]
        [InlineData("0","03", "0:00")]
        [InlineData("10","02", "10:  ")]
        [InlineData("10","03", "10:00")]
        //
        public void TestChiseki(string men, string chimoku, string expected)
        {
            yg_chohyo.Data.Util.ChisekiWake a = yg_chohyo.Data.Util.ChisekiWakeru(men, chimoku);
            Assert.Equal(expected, $"{a.Seisu}:{a.Shosu}");
        }
        */


    }

    public class Test_KanchiShomei_KumiRecGet : yg_chohyo.Chohyo.KanchiShomei.KanchiShomei
    {
        static void UpdateShoyuKyoyu()
        {

        }

        static string[] KumiCodes { get; } = new string[] { "302902" };
        public static void Test_KumiRecGet()
        {
            double date = DateTime.Parse("2020/11/23").ToOADate();
            Create(KumiCodes, date);    
        }
    }

    public class Test_HoryuchiShomei : yg_chohyo.Chohyo.HoryuShomei
    {
        public static void Write(string kanchiCD)
        {
            double date = DateTime.Parse("2020/11/23").ToOADate();
            Create(kanchiCD, date);
        }

    }
}
