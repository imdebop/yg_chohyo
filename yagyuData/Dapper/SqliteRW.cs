﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using Dapper;
using yg_chohyo.Data;
using System.Linq;
//using yagyuData;

namespace yg_chohyo.Dapper
{
    public class SqliteRW
    {
        static string dbPath;
        static SqliteRW(){
            dbPath = localizeCs.ProgramPath.GyomuDbPath();
        }

        [System.Diagnostics.DebuggerStepThrough]
        public static SQLiteConnection GetConnection()
        {
            var config = new SQLiteConnectionStringBuilder()
            {
                //DataSource = @"c:/yagyu_settings/gyomu.sqlite"
                DataSource = dbPath
            };
            return new SQLiteConnection(config.ToString());
        }

        public static void BulkInsert(string tblName, IList<string> recs) {

            using (var conn = GetConnection())
            {
                conn.Open();

                using (var cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = $"DELETE FROM {tblName};";
                    cmd.ExecuteNonQuery();

                    using (var transaction = conn.BeginTransaction())
                    {
                        foreach (object rec in recs)
                        {
                            //MessageBox.Show(rec.ToString());
                            cmd.CommandText =
                                $"INSERT INTO {tblName} VALUES ({rec});";
                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                }

                conn.Close();
            }
        }
    }

    public class DapperShoyu
    {
        [System.Diagnostics.DebuggerStepThrough]
        public static IEnumerable<Data.Shoyu> GetByKana(string kana)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                conn.Open();
                return conn.Query<Data.Shoyu>($"select * from shoyu where Furigana = '{kana}'");

            }
        }
        public static IEnumerable<Data.Shoyu> SearchByName(string str)
        {
            using(var conn = SqliteRW.GetConnection())
            {
                conn.Open();
                return conn.Query<Data.Shoyu>($"select * from shoyu where TokiName Like '%{str}%'");
            }
        }

        public static Data.Shoyu GetShoyu(string shoCode)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                conn.Open();
                return conn.Query<Data.Shoyu>($"select * from shoyu where ShoCode = '{shoCode}'").First();
            }
        }

        public static IEnumerable<Shoyu> GetAll()
        {
            using (var conn = SqliteRW.GetConnection())
            {
                conn.Open();
                return conn.Query<Shoyu>("select * from shoyu");
            }
        }
    }

    public class DapperKyoyu
    {
        public static IEnumerable<Data.Kyoyu> GetRecsByShocode(string shoCode)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                conn.Open();
                return conn.Query<Data.Kyoyu>($"select * from kyoyu where ShoCode ='{shoCode}' order by Key");
            }
        }
    }



    public class DapperKumi
    {
        public static IEnumerable<Data.Kumi> GetByShocode(string shoCode)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                conn.Open();
                return conn.Query<Data.Kumi>($"select * from kumi where ShoCode = '{shoCode}' order by Key");

            }
        }
        public static Kumi GetByKumiKey(string kumiKey)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                var res = conn.Query<Data.Kumi>($"select * from kumi where Key = '{kumiKey}'");
                return res.First();
            }
        }

        public static IEnumerable<Kumi> GetAll()
        {
            using (var conn = SqliteRW.GetConnection())
            {
                return conn.Query<Kumi>($"select * from kumi");

            }
        }

    }

    public class DapperJuzen
    {
        public static Juzen GetByFude(string fudeCode)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                var res = conn.Query<Data.Juzen>($"select * from Juzen where FudeCode = '{fudeCode}'");
                if(res.Count() == 0)
                {
                    return null;
                }   
                else
                {
                    return res.First();
                }
            }
        }

        public static IEnumerable<Juzen> GetByChoaza(string choazaCD)
        {
            using(var conn = SqliteRW.GetConnection()){
                return conn.Query<Data.Juzen>($"select * from Juzen where substr(FudeCode,1,2) = '{choazaCD}'");
            }
        }

        public static IEnumerable<string> GetAllFcodes()
        {
            using(var conn = SqliteRW.GetConnection()){
                return conn.Query<Data.Juzen>($"select * from Juzen").Select(x => x.FudeCode);
            }
        }

        /// <summary>
        /// Returns all "fcode:shocode" pairs
        /// </summary>
        public static IEnumerable<string> GetAllFcShoPair()
        {
            using (var conn = SqliteRW.GetConnection())
            {
                return conn.Query<Data.Juzen>($"select * from Juzen").Select(x => $"{x.FudeCode}:{x.ShoCode}");
            }
        }



    }

    public class DapperKanchi
    {
        public static Kanchi GetByKancode(string kancode)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                try
                {
                    var res = conn.Query<Data.Kanchi>($"select * from Kanchi where KanCode = '{kancode}'");
                    return res.First();
                }
                catch(InvalidOperationException e)
                {
                    return new Kanchi() { KanCode = kancode, Menseki = "*****"};
                }

            }
        }

        public static IEnumerable<Kanchi> GetByBlock(string block)
        {
            using (var conn = SqliteRW.GetConnection())
            {
                var res = conn.Query<Data.Kanchi>($"select * from Kanchi where KanCode like '{block}%'");
                return res;
            }
        }

        public static IEnumerable<Kanchi> GetAll()
        {
            using (var conn = SqliteRW.GetConnection())
            {
                return conn.Query<Kanchi>("select * from kanchi");
            }
        }

    }


 }

