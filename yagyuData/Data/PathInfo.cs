﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yg_chohyo.Data
{
    public static class PathInfo
    {
        //static string YagyuRoot = @"c:\yagyu\";
        static string YagyuSettings = @"c:\yagyu_settings\";

        public static string GetPath(string fileName)
        {
            switch (fileName)
            {
                case "ChisekiChkRoot":
                    return YagyuSettings + "checkResults";
                case "dwgTukeho.txt":
                    return YagyuSettings + @"checkResults\" + fileName;
                default:
                    return "";
            }
        }
    }
}
