﻿using System;
using System.Collections.Generic;
//using System.Security.RightsManagement;
using System.Text;
using System.Text.RegularExpressions;

namespace yg_chohyo.Data
{
    public static class Util
    {
        static Regex rxNum = new Regex(@"^\d[\d-\.]*$");

        public static bool IsNum(string str)
        {
            bool res = false;
            if (rxNum.IsMatch(str))
            {
                res = true;
            }

            return res;
        }

        /// <summary>
        /// 3B4-1形式をコードに変換
        /// </summary>
        public static string BL2code(string kakuchi)
        {
            string[] bl_lot = new string[0];
            try
            {
                bl_lot = kakuchi.Split('B');
                if (bl_lot.Length != 2)
                {
                    throw new FormatException($"画地番号が不正です:{kakuchi}");
                }
            }
            finally { }

            return BL2code(bl_lot[0], bl_lot[1]);
        }

        public static string BL2code(string block, string lot)
        {
            if (block == "")
            {
                return "";
            }
            else if (block == "指定")
            {
                return "未指定";
            }
            string kancode = "";
            string moto = "";
            string eda = "";
            string bk = Int32.Parse(block).ToString();
            kancode = bk.PadLeft(2, '0');

            string[] moto_eda = lot.Split('-');
            moto = Int32.Parse(moto_eda[0]).ToString().PadLeft(3, '0');
            if (moto_eda.Length == 1)
            {
                moto += "000";
                kancode += moto;
            }
            else
            {
                eda = Int32.Parse(moto_eda[1]).ToString().PadLeft(3, '0');
                kancode += (moto + eda);
            }

            return kancode;
        }
        /// <summary>
        //地番（町字コード＋地番）を筆コードに変換 
        /// </summary>
        public static string Chiban2code(string choaza, string chiban)
        {
            if (!rxNum.IsMatch(choaza))
            {
                return "";
            }
            if (!rxNum.IsMatch(chiban))
            {
                return "error***";
            }
            string fudeCode = "";
            string moto = "";
            string eda = "";

            fudeCode = choaza.Substring(0, 2);

            string[] moto_eda = chiban.Split('-');
            moto = Int32.Parse(moto_eda[0]).ToString().PadLeft(3, '0');
            if (moto_eda.Length == 1)
            {
                moto += "000";
                fudeCode += moto;
            }
            else
            {
                eda = Int32.Parse(moto_eda[1]).ToString().PadLeft(3, '0');
                fudeCode += (moto + eda);
            }

            return fudeCode;
        }

        public static string MotoEda(string moto, string eda)
        {
            string s = moto;
            if (eda != "") { s += $"-{eda}"; }
            else
            {
                s += "  ";
            }
            return s;
        }

        public static Shozai Fcode2shozai(string fcode)
        {
            var shozai = new Shozai();
            shozai.FudeCode = fcode;
            var azaCode = fcode.Substring(0, 2);
            shozai.Choaza = Data.CodeTables.DicChoaza[azaCode];
            var chibanCD = fcode.Substring(2);
            shozai.Moto = chibanCD.Substring(0, 3).TrimStart('0');
            shozai.Eda = chibanCD.Substring(3, 3).TrimStart('0');

            Reigai.JuzenShozai(ref shozai);

            return shozai;

        }

        public static string Choaza2code(string choaza)
        {
            Dictionary<string, string> DicGetAzacode = CodeTables.DicChoaza2code();
            string murocho = "牟呂町字";//省略されている場合追加する
            if (DicGetAzacode.ContainsKey(choaza))
            {
                return DicGetAzacode[choaza];
            }else if(DicGetAzacode.ContainsKey( murocho + choaza))
            {
                return DicGetAzacode[murocho + choaza];
            }else if(choaza == "会所前")
            {
                return DicGetAzacode["神野新田町字会所前"];
            }

            return "errorChoaza";
        }

        /// <summary>
        /// ex:"東里29-1a"　を"01029001:a"に変換
        /// </summary>
        /// <param name="shozai"></param>
        /// <returns></returns>
        public static string Shozai2Fcode(string shozai)
        {
            Regex rg = new Regex(@"^(\D+)([\d-]+)([a-z]*)$");
            var m = rg.Match(shozai);
            string choaza = m.Groups[1].Value;
            string chiban = m.Groups[2].Value;
            string bunkatu = m.Groups[3].Value;
            string azaCode = Choaza2code(choaza);
            string fcode = Chiban2code(azaCode, chiban);
            if (bunkatu != "") { fcode += ":" + bunkatu; }
            return fcode;
        }

        public static BlockLot Kancode2B_L(string kanchiCD)
        {
            Data.Kanchi k = new Kanchi() { KanCode = kanchiCD };
            return new BlockLot()
            {
                Block = k.GetBlock(),
                Lot = k.GetLot()
            };
        }

        public static string Kancode2B_L_str(string kanchiCD)
        {
            BlockLot bl = Kancode2B_L(kanchiCD);
            return $"{bl.Block}B {bl.Lot.Trim()}L";
        }


        public class BlockLot
        {
            public string Block { get; set; }
            public string Lot { get; set; }
        }

        public class Shozai
        {
            public string FudeCode { get; set; }
            public string Choaza { get; set; }
            public string Moto { get; set; }
            public string Eda { get; set; }
            public string Chiban()
            {
                return MotoEda(Moto, Eda);

            }
            public override string ToString()
            {
                var shozai = $"{Choaza}  {Moto}";
                if (Eda == "")
                {
                    shozai += "    ";
                }
                else
                {
                    shozai += $" - {Eda.PadLeft(3, ' ')}";
                }
                return shozai;

            }
        }

        public static string ChisekiFMT(string chiseki)
        {
            if (IsNum(chiseki))
            {
                decimal a = decimal.Parse(chiseki);
                return a.ToString("0.00");
            }
            else
            {
                return $"err:{chiseki}";
            }
        }

        public static ChisekiWake ChisekiWakeru(string chiseki, string chimoku)
        {
            ChisekiWake cw = new ChisekiWake();
            string val = decimal.Parse(chiseki).ToString("0.00");
            string[] chisekiWake = val.Split('.');
            cw.Seisu = chisekiWake[0];
            cw.Shosu = chisekiWake[1];
            cw.HyojiChiseki = chisekiWake[0];
            if (cw.Shosu == "00")
            {   //小数部がゼロの場合
                if(!Data.CodeTables.IsTakuchiOrKousenchi(chimoku))
                {   //宅地・鉱泉地以外
                    if(cw.Seisu.Length > 1)
                    {   //面積が10㎡以上
                        cw.Shosu = "  ";
                        cw.HyojiChiseki += "   ";
                    }
                    else
                    {
                        cw.HyojiChiseki += ".00";
                    }
                }
            }
            else
            {
                cw.HyojiChiseki += $".{chisekiWake[1]}";
            }

            return cw;
        }

        public class ChisekiWake
        {
            public string Seisu { get; set; }
            public string Shosu { get; set; }
            public string HyojiChiseki { get; set; }
        }

        public class Shoyu
        {
            public static bool IsKyoyu(string shoCode)
            {
                if (shoCode.Substring(0, 1) == "2")
                {
                    return true;
                }
                return false;
            }
        }

    }
}
