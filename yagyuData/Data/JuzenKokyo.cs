﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace yg_chohyo.Data
{
    /// <summary>
    /// 従前公共用地
    /// </summary>
    public class JuzenKokyo
    {
        public static IEnumerable<Juzen> getAll()
        {
            foreach (string r in File.ReadAllLines(@"c:/yagyu_settings/data/JuzenKokyo.txt"))
            {
                if (string.IsNullOrEmpty(r)) { continue; }
                string[] arr = r.Split('\t');
                string fcode = Data.Util.Chiban2code(arr[0], arr[1]);

                Juzen ju = new Juzen() {
                    FudeCode = fcode,
                    ChimokuCD = arr[5],
                    TokiMen = arr[6],
                    ShoCode = arr[7],
                };

                yield return ju;
            }
        }
    }
}
