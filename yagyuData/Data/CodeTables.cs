﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using yg_chohyo.Dapper;

namespace yg_chohyo.Data
{
    public class CodeTables
    {
        public static readonly int KanCodeLength = 8;
        public static readonly int JuzenCodeLength = 8;

        public static Dictionary<string, string> DicChimoku { get; private set; } = new Dictionary<string, string>()
        {
            {"01", "田" },
            {"02", "畑" },
            {"03", "宅地" },
            {"04", "塩田" },
            {"05", "鉱泉地" },
            {"06", "池沼" },
            {"07", "山林" },
            {"08", "牧場" },
            {"09", "原野" },
            {"10", "墓地" },
            {"11", "境内地" },
            {"12", "運河用地" },
            {"13", "水道用地" },
            {"14", "用悪水路" },
            {"15", "ため池" },
            {"16", "堤" },
            {"17", "井溝" },
            {"18", "保安林" },
            {"19", "公衆用道路" },
            {"20", "公園" },
            {"21", "雑種地" },
            {"22", "学校用地" },
            {"23", "鉄道用地" },
            {"24", "溝渠" },
            {"25", "渡船敷地" }

        };

        //宅地または鉱泉地であるか
        public static bool IsTakuchiOrKousenchi(string chimokuCD)
        {   
            if (chimokuCD == "03" || chimokuCD == "05")
            {
                return true;
            }
            return false;
        }


        public static Dictionary<string, string> DicChoaza { get; private set; } = new Dictionary<string, string>()
        {
            {"01", "牟呂町字大塚" },
            {"02", "牟呂町字奥山" },
            {"03", "牟呂町字奥山新田" },
            {"04", "牟呂町字北汐田" },
            {"05", "牟呂町字古田" },
            {"06", "牟呂町字古幡焼" },
            {"07", "牟呂町字中西" },
            {"08", "牟呂町字東里" },
            {"09", "牟呂町字百間" },
            {"10", "牟呂町字松崎" },
            {"11", "牟呂町字松島" },
            {"12", "牟呂町字松島東" },
            {"13", "牟呂町字松東" },
            {"14", "牟呂町字南汐田" },
            {"19", "神野新田町字会所前" },
            {"21", "柱四番町" },
            {"22", "柱五番町" },
            {"24", "藤沢町" },
            {"25", "潮崎町" },
            {"26", "牟呂市場町" }
        };

        /// <summary>
        /// 町字名からコードを得る辞書を返す：変数に入れて使用すること
        /// </summary>
        public static Dictionary<string, string> DicChoaza2code()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            return DicChoaza.ToDictionary(x => x.Value, x => x.Key);
        }
        /// <summary>
        /// 換地データでkancode->shocode(要注意)
        /// </summary>
        public static Dictionary<string, string> DicKan2sho { get; private set; } = SetDicKan2sho();
        
            
        private static Dictionary<string, string> SetDicKan2sho()
        {
            Dictionary<string, string> dk2s = new Dictionary<string, string>();
            foreach(Kanchi k in DapperKanchi.GetAll())
            {
                    dk2s[k.KanCode] = k.ShoCode;
            }
            return dk2s;
        }

        public static Dictionary<string, string> DicKumiKan2sho { get; private set; } = SetDicKumiKan2sho();

        private static Dictionary<string, string> SetDicKumiKan2sho()
        {
            Dictionary<string, string> dKan2sho = new Dictionary<string, string>();
            string kanchis = "";
            foreach(Kumi kumi in DapperKumi.GetAll())
            {
                kanchis = kumi.Kanchis;
                foreach(string kancode in kanchis.Split('/'))
                {
                    dKan2sho[kancode] = kumi.ShoCode;
                }
            }
            foreach(Kanchi kan in DapperKanchi.GetAll())
            {
                if( kan.ShoCode == "5000")
                {
                    dKan2sho[kan.KanCode] = "5000";
                }
            }
            return dKan2sho;
        }




        public static Dictionary<string, string> DicShoName { get; private set; } = SetDicShoName();

        private static Dictionary<string, string> SetDicShoName()
        {
            System.Diagnostics.Debug.WriteLine("所有者名辞書の初期化");
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach(Shoyu s in DapperShoyu.GetAll())
            {
                dic[s.ShoCode] = s.TokiName;
            }
            dic["5000"] = "保留地";
            return dic;
        }

        public static Dictionary<string, string> SetDicFcode2Sho()
        {
            System.Diagnostics.Debug.WriteLine("fcode->shocode の初期化");
            Dictionary<string, string> dic = new Dictionary<string, string>();
            string shocode;
            string[] wk;
            foreach(Kumi k in Dapper.DapperKumi.GetAll())
            {
                shocode = k.ShoCode;
                wk = k.Fcodes.Split('/');
                foreach(string fcode in wk)
                {
                    dic[fcode] = shocode;
                }
            }
            return dic;
        }



        public static bool IsHoryuchi(string shoCode)
        {
            if (shoCode == "5000")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }


}
