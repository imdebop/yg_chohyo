using System.Collections.Generic;
using System;
//using System.Security.RightsManagement;
//using Org.BouncyCastle.Crypto.Agreement.Srp;
using System.Dynamic;

namespace yg_chohyo.Data
{

    public class DicShoyu
    {
        public Dictionary<string, Shoyu> dic { get; set; } = new Dictionary<string, Shoyu>();
    }

    public class Shoyu
    {
        public string ShoCode { get; set; }
        public string TokiName { get; set; }
        public string TokiJusho { get; set; }
        public string GenName { get; set; }
        public string GenJusho { get; set; }
        public string SeinenYMD { get; set; }
        public string Seibetu { get; set; }
        public string Furigana { get; set; }
        public string MeiboPirnt { get; set; }
        public string Telephone { get; set; }
        public int id { get; set; }
    }

    public class Kyoyu
    {
        public string Key { get; set; }
        public string ShoCode { get; set; }
        public string TokiName { get; set; }
        public string TokiJusho { get; set; }
        public string GenName { get; set; }
        public string GenJusho { get; set; }
        public string Bunbo { get; set; }
        public string Bunshi { get; set; }
        public string Bojin { get; set; }
        public string SeinenYMD { get; set; }
        public string Seibetu { get; set; }
        public string Furigana { get; set; }
        public string Daihyo { get; set; }
    }

    public class Souzoku
    {
        public string Key { get; set; }
        public string ShoCode { get; set; }
        public string Shimei { get; set; }
        public string Jusho { get; set; }
        public string SeinenYMD { get; set; }
        public string Seibetu { get; set; }
        public string Furigana { get; set; }
        public string Daihyo { get; set; }
        public string Tel { get; set; }
    }

    public class DicKanchi
    {
        public Dictionary<string, Kanchi> dic { get; set; } = new Dictionary<string, Kanchi>();
    }

    public static class DataExtention
    {
        public static string GetValues(this Shoyu s)
        {
            return $"'{s.ShoCode}','{s.TokiName}','{s.TokiJusho}','{s.GenName}','{s.GenJusho}','{s.SeinenYMD}','{s.Furigana}','{s.Seibetu}','{s.MeiboPirnt}','{s.Telephone}', NULL";
        }

        public static string GetValues(this Kyoyu k)
        {
           return $"'{k.Key}','{k.ShoCode}','{k.TokiName}','{k.TokiJusho}','{k.GenName}','{k.GenJusho}','{k.Bunbo}','{k.Bunshi}','{k.Bojin}','{k.SeinenYMD}','{k.Seibetu}','{k.Furigana}','{k.Daihyo}'";
        }

        public static string GetValues(this Souzoku k)
        {
            return $"'{k.Key}','{k.ShoCode}','{k.Shimei}','{k.Jusho}','{k.SeinenYMD}','{k.Seibetu}','{k.Furigana}','{k.Daihyo}','{k.Tel}'";
        }

        //public static string GetValues(this Horyu h)
        //{
        //    return $"'{h.KanCode}','{h.Shubetu}','{h.Nendo}','{h.Kakaku}','{h.ShoCode}','{h.Comment}'";
        //}

        public static string GetValues(this Kumi m)
        {
            return $"'{m.Key}','{m.ShoCode}','{m.Fcodes}','{m.Kanchis}','{m.Shitei}','{m.Memo}', NULL";
        }
        public static string JoinedValues(this KumiShitei t, string sep = "/")
        {
            return $"{t.ShiteiDate}{sep}{t.Hatuban}{sep}{t.KoryokuDate}";
        }

    }

    public static class KanchiExtention
    {
        public static string GetBlock(this Kanchi k)
        {
            return k.KanCode.Substring(0, 2).TrimStart('0');    
        } 
        public static string GetLot(this Kanchi k)
        {
            var moto = k.KanCode.Substring(2, 3).TrimStart('0');
            var eda = k.KanCode.Substring(5, 3).TrimStart('0');
            return Data.Util.MotoEda(moto, eda);
        }


        public static string GetValues(this Kanchi k)
        {
            return $"NULL,'{k.KanCode}','{k.ShoCode}','{k.Menseki}','{k.HeibeiSisu}','{k.Memo}','{k.HyoteiSisu}','{k.KenriSisu}','{k.Sokochi}'";

        }

        public static string GetValues(this Kaishi k)
        {
            return $"'{k.Id}','{k.KanCode}','{k.Renban}','{k.KaishiDate}','{k.Hatuban}','{k.KoryokuDate}','{k.Menseki}','{k.Comment}'";
        }

        public static string GetValues(this Horyu h)
        {
            return $"NULL,'{h.KanCode}','{h.Shubetu}','{h.Nendo}','{h.Kakaku}','{h.ShoCode}','{h.Comment}','{h.TukeKancode}'";
        }
    }

    public static class JuzenExtention
    {
        public static string GetAzaCode(this Juzen j)
        {
            return j.FudeCode.Substring(0, 2);
        }

        public static string GetJuzenCiban(this Juzen j)
        {
            return Data.Util.Fcode2shozai(j.FudeCode).Chiban();
        }

        public static string GetJuzenChoaza(this Juzen j)
        {
            return Data.Util.Fcode2shozai(j.FudeCode).Choaza;
        }

        public static string GetChimoku(this Juzen j)
        {
            return CodeTables.DicChimoku[j.ChimokuCD];
        }

        public static string GetHyojiChiseki(this Juzen j)
        {
            return Data.Util.ChisekiWakeru(j.TokiMen, j.ChimokuCD).HyojiChiseki;
        }

        public static string ChisekiSeisu(this Juzen j)
        {
            return Data.Util.ChisekiWakeru(j.TokiMen, j.ChimokuCD).Seisu;
        }

        public static string ChisekiShosu(this Juzen j)
        {
            return Data.Util.ChisekiWakeru(j.TokiMen, j.ChimokuCD).Shosu;
        }

        public static string GetValues(this Juzen j)
        {
            return $"'{j.FudeCode}','{j.ChimokuCD}','{j.ShoCode}','{j.TokiMen}','{j.KijunMen}','{j.HeibeiSisu}','', NULL";
        }

    }


    public class Kanchi
    {
        public string KanCode { get; set; }
        public string ShoCode { get; set; }
        public string Menseki { get; set; }
        public int HeibeiSisu { get; set; }
        public string Memo { get; set; }
        public int HyoteiSisu { get; set; }
        public int KenriSisu { get; set; }
        public string Sokochi { get; set; }

    }


    public class Kaishi
    {
        public string Id { get; set; }
        public string KanCode { get; set; }
        public string Renban { get; set; }
        public string KaishiDate { get; set; }
        public string Hatuban { get; set; }
        public string KoryokuDate { get; set; }
        public string Menseki { get; set; }
        public string Comment { get; set; }
    }


    public class DicHoryu
    {
        public Dictionary<string, Horyu> dic = new Dictionary<string, Horyu>();
    }

    public class Horyu
    {
        public string KanCode { get; set; }
        public string Shubetu { get; set; }
        public string Nendo { get; set; }
        public string Kakaku { get; set; }
        public string ShoCode { get; set; }
        public string Comment { get; set; }
        public string TukeKancode { get; set; }

    }

    public class DicJuzen
    {
        public Dictionary<string, Juzen> dic = new Dictionary<string, Juzen>();
    }

    public class Juzen
    {
        public string FudeCode { get; set; }
        public string ChimokuCD { get; set; }
        public string ShoCode { get; set; }
        public string TokiMen { get; set; }
        public string KijunMen { get; set; }
        public int HeibeiSisu { get; set; }
        public int HyoteiSisu { get; set; }

    }

    public class DicKumi
    {
        public Dictionary<string, Kumi> dic = new Dictionary<string, Kumi>();
    }

    public class Kumi
    {
        public string Key { get; set; }
        public string ShoCode { get; set; }
        public string Fcodes { get; set; } = "";
        public string Kanchis { get; set; } = "";
        public string Shitei { get; set; } = "";
        public string Memo = "";
    }


    public class KumiShitei
    {
        public string ShiteiDate { get; set; } = "";
        public string Hatuban { get; set; }
        public string KoryokuDate { get; set; }
    }


}