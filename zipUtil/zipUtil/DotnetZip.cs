﻿using System;
using System.IO;
using System.Text;
using Ionic.Zip;
using Ionic.Zlib;

namespace zipUtil
{
    public class DotnetZip
    {
        public static void test( string srcParentFolder, string targetFolder, string outFolder){
            string outZipPath = outFolder + targetFolder + ".zip";
            bool res = File.Exists(outZipPath);
            if(res){
                Console.WriteLine($"\"{outZipPath}\"はすでに存在します。");
            }else{
                //ZipFile.CreateFromDirectory(srcFolder, outZip);
                string res2 = DotnetZip.Compress(srcParentFolder, targetFolder, outFolder);
            }
            
        }
        ///<summary>returns zip saved path</summary>
        public static string CompressFolder(string folderPath, string outFolder){
            string folderBase = Path.GetDirectoryName(folderPath);
            string foldername = Path.GetFileName(folderPath);
            return Compress(folderBase, foldername, outFolder);
        }
        public static string Compress(string folderPath, string targetFolder, string outFolder){
            string zipSavePath;
            //System.Text.Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            // ZIPファイルの生成
            //using (var zip = new ZipFile())
            using (var zip = new ZipFile(Encoding.GetEncoding("utf-8")))
                {
                    // 圧縮レベル
                    zip.CompressionLevel = CompressionLevel.BestCompression;

                    // ファイルやディレクトリをZIPアーカイブに追加
                    // 以下のような構成のZIPアーカイブとなる
                    // hogefoo.zip
                    //   --hoge.txt
                    //   -- foo
                    //zip.AddFile(@"C:\work\hoge.txt", "");
                    //zip.AddDirectory(@"C:\work\foo", "foo");
                    string folderFull = Path.Combine(folderPath, targetFolder);
                    zip.AddDirectory(folderFull, "");

                    // 保存
                    //zip.Save(@"C:\work\hogefoo.zip");
                    zipSavePath = Path.Combine(outFolder,(targetFolder + ".zip"));
                    zip.Save(zipSavePath);
                }
            return zipSavePath;
        }


    }

}
