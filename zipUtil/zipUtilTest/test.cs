﻿using zipUtil;
using System;
using YgLocalRoot;

namespace zipUtilTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //string srcFolder = "bin";
            //string outZip = "aaa.zip";

            Console.WriteLine("zip compress test");

            //string aaa = Class1.GetLocalRoot();

            string srcFolder = LocalRoot.GetZipSourceFolder();
            Console.WriteLine($"zip src folder = {srcFolder}");
            string targetFolder = "20210921帳票更新大地01";
            //string outFolder = "zipOut/";
            string outFolder = LocalRoot.GetZipDestFolder();
            Console.WriteLine($"zip out folder = {outFolder}");
            zipUtil.Class1.test(srcFolder, targetFolder, outFolder);
        }
    }
}
