﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Linq;
using yg_chohyo.Data;

namespace KanriDaicho
{
    class ChkData
    {
        public void ChkKanchi_shoyu()
        {
            //
            //コードを新しいクラスに移しただけで、何もしていない。
            //
            //チェック番外：組合せデータと換地データの所有者の整合
            List<string> kumiData = new List<string>();
            foreach (Kumi kumi in yg_chohyo.Dapper.DapperKumi.GetAll())
            {   //(換地コード):(所有者コード)のリストを作成
                string[] kanRecs = kumi.Kanchis.Split("/");
                foreach (string k in kanRecs)
                {
                    kumiData.Add(k + ":" + kumi.ShoCode);
                }
            }
            List<string> kanData = new List<string>();
            foreach (Kanchi kan in yg_chohyo.Dapper.DapperKanchi.GetAll())
            {
                if (kan.ShoCode == "5000") { continue; }//保留地を除外
                kanData.Add(kan.KanCode + ":" + kan.ShoCode);
            }

            IEnumerable<string> results = kumiData.Except(kanData);

            Debug.WriteLine("kan shoyu list creaed");
        }
    }
}
