using System;
using Xunit;
using Yobai;
using ygExBase;

namespace yobaiTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            ExRepo repo = YobaiMain.RunTest();
            var obj = repo.GetWsRawData(7,7);
            Assert.Equal("aaaa", obj);
        }
    }
}
