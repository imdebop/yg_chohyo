class ZahyoUpdate:
    @classmethod
    def do(self, sR_d, sR_a, dbMon):
        self.dicD = sR_d.dicZahyo
        self.dicA = sR_a.dicZahyo
        self.zahyoErrList = []
        self.zahyoNewList = []
        max = self.getMaxTenban(sR_d.dicZahyo)
        self.getNewRecs()
        print("zahyo diff errors =")
        print(self.zahyoErrList)

        dbMon.insert_list(self.zahyoNewList, "simaZahyo")

    @classmethod
    def getNewRecs(self):
        for recA in self.dicA.values():
            if recA.meisho in self.dicD.keys():
                recD = self.dicD[recA.meisho]
                self.zahyoCompare(recD, recA)
            else:
                self.add2newList(recA)
                print("not in Daichi Sima = " + recA.meisho)

    @classmethod
    def add2newList(self, recA):
        tenban = int(recA.tenban)
        tenban = tenban + 30000
        recA.tenban = str(tenban)
        self.zahyoNewList.append(recA.to_dict())

    @classmethod
    def zahyoCompare(self, rD, rA):
        coordD = rD.x + "0:" + rD.y + "0"
        coordA = rA.x + ":" + rA.y
        if coordD == coordA:
            print("chk ok = " + rD.meisho )
        else:
            self.zahyoErrList.append("** chk NG ** = " + rD.meisho )


    @classmethod
    def getMaxTenban(self, dicZahyo):
        max = 0
        for r in dicZahyo.values():
            n = int( r.tenban )
            if n > max:
                max = n
        print("max = " + str(max))
        return max
