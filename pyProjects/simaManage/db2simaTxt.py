from simaManage.mongo2sima import *
from envCommon import *

class Db2simaTxt:
    @classmethod
    def write(self, dbMon, outFname):
        oPath = ygRoot + "sokochi/sima/" + outFname
        m2s = Mongo2sima(dbMon)
        outList =  []
        li = m2s.getAllZahyo()
        for r in li:
            outList.append(self.editZahyo(r))
        outList.append("A99,")

        li = m2s.getAll_Lots()
        for r in li:
            outList = outList + self.editBlkLot(r)

        with open(oPath, 'wt') as f:
            f.write(("\n").join(outList) + "\n")

    @classmethod
    def editZahyo(self, li):
        rec = [li.label, li.tenban, li.meisho, li.x, li.y, li.z, li.note ]
        return (",").join(rec)

    @classmethod
    def editBlkLot(self, li):
        outList = [li.header]
        for r in li.kessen:
            wk = [r["kID"], r["t"], r["m"], r["o"]]
            outList.append((",").join(wk))
        outList.append("D99,")
        return outList





