from simaManage.mongo2sima import *


class BlkLotUpdate:
        #sR_A is for simaRec_Aduma
    def __init__(self, dbMon, sR_A):
        # mongoからsimaを読み出すクラス
        m2s = Mongo2sima(dbMon)

        zList = m2s.getAllZahyo()
        dicMei2ten = self.getMei2ten(zList)
        blkListA = sR_A.blkList
        self.blkMeisho_set = m2s.getBlkMeisho_set() 
        self.maxBkLt_id = m2s.getBkLt_maxID()
        blkListMod = self.getBlkListMod(dicMei2ten, blkListA)
        dbMon.insert_list(blkListMod, "simaLot")

    def getMei2ten(self, zList):
        dic = {}
        for zr in zList:
            dic[zr.meisho] = zr.tenban
        return dic

    def getBlkListMod(self, dicMei2ten, blkListA):
    #ブロックのbid, 結線の点番を変更
        #bidは50001から
        if self.maxBkLt_id < 50001:
            wkid = 50001
        else:
            wkid = self.maxBkLt_id + 1
        lst = []
        for br in blkListA:
            if self.checkIfDup(br):
                print("duplicate block:meisho = " + br["meisho"])
                continue
            br['bID'] = str(wkid)
            br["kessen"] = self.kessenTenbanMod(br["kessen"], dicMei2ten)
            br["header"] = "D00," + br["bID"] + "," + br["meisho"] + ",1,"
            lst.append(br)
            wkid = wkid + 1
        return lst
    
    #ブロック・画地レコードの点番を変更        
    def kessenTenbanMod(self, kessenList, dicMei2ten):
        lst = []
        for ks in kessenList:
            if ks["kID"] == "B01":
                ks["t"] = dicMei2ten[ks["m"]]
                lst.append(ks)
            else:
                lst.append(ks)
        return lst

    #ブロック・画地の重複チェック
    def checkIfDup(self, br):
        if br["meisho"] in self.blkMeisho_set:
            return True
        return False
