from dataclasses import dataclass,field
from dataclasses_json import dataclass_json

@dataclass_json
@dataclass
class ZahyoRec():
    label: str = ""
    tenban: str = ""
    meisho: str = ""
    x: str = ""
    y: str = ""
    z: str = ""
    note: str = ""

@dataclass_json
@dataclass
class KessenRec():
    kID: str = ""
    t: str = "" # tenban
    m: str = "" # meisho
    o: str = "" # other

@dataclass_json
@dataclass
class BkLotRec():
    bID: str = ""
    meisho: str = ""
    header: str = ""
    kessen: list = field(default_factory=list)


