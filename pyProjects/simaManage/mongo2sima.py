import copy
from dataclasses import dataclass,field
from dataclasses_json import dataclass_json
from simaManage.simaRec import *
from data.dataUtil import *

class Mongo2sima():
    def __init__(self, dbMon):
        self.dbMon = dbMon

    def getAllZahyo(self):
        zList = []
        recs = self.dbMon.getAll("simaZahyo")
        for r in recs:
            zr = ZahyoRec(**r)
            zList.append(zr)
        return zList
    
    def getAll_Lots(self):
        lotList = []
        recs = self.dbMon.getAll("simaLot")
        for r in recs:
            lr = BkLotRec(**r)
            lotList.append(lr)
        return lotList

    def getBkLt_maxID(self):
        recs = self.dbMon.getAllByFields("simaLot", ["bID"])
        maxID = 0
        for t in recs:
            wk = int(t["bID"])
            if  wk > maxID:
                maxID =wk 
        return maxID

    def getBlkMeisho_set(self):
        recs = self.dbMon.getAllByFields("simaLot", ["meisho"])
        mSet = set()
        for r in recs:
            mSet.add(r["meisho"])
        return mSet

    def getBlk_asKancode_list(self):
        kanList = []
        meishoSet = self.getBlkMeisho_set()
        for m in meishoSet:
            bl, lot = m.split("B") 
            kancode = DataUtil.bl_lot2kancode(bl, lot)
            kanList.append(kancode)
        return kanList

    def getBlk_asKancode_set(self):
        li = self.getBlk_asKancode_list()
        return {*li}