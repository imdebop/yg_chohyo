import envCommon
import os
from simaManage.simaRec import *
from data.dataUtil import *
import re

class SimaRead():
    def __init__(self, simPath, kanDic):
        self.dicZahyo = {}
        self.dicM2T = {}
        self.dicKanCode = kanDic
        self.zahyoList = []
        self.blkList = []

        fPath = envCommon.ygRoot + "sokochi/sima/" + simPath
        with open(fPath, 'r', encoding='utf-8_sig') as f:
            wklist = f.readlines()
        recs = []
        for r in wklist:
            wk = r.replace(" ", "")
            wk = wk.replace("\n", "")
            if wk == "":
                continue
            li = wk.split(",")
            if li[0] == "Z00":
                continue
            recs.append(li)
        self.readZahyo(recs)
        self.readBlk(recs)
        print("for break point")

    def readZahyo(self, recs):
        for li in recs:
            if li[0] == "A01":
                zr = ZahyoRec(*li)
                self.zahyoList.append(zr.to_dict())
                self.dicZahyo[zr.meisho] = zr
                self.dicM2T[zr.meisho] = zr.tenban

            if li[0] == "A99":
                break

    def readBlk(self, recs):
        startFlg = False # to skip irregular C01 data
        writeFlg = False
        for li in recs:
            if li[0] == "D00":
                startFlg = True
                br = BkLotRec()
                br.bID = li[1]
                br.meisho = li[2]
                br.header = ",".join(li) 
                # dataはとりあえず不使用
                res = self.kubunCheck(br.meisho)
                kubun, data = res
                if kubun == "other":
                    writeFlg = False
                elif kubun == "lot":
                    writeFlg = True
                else:
                    writeFlg = False

            if not startFlg:
                continue
            if li[0] == "B01" or li[0] == "C01":
                ks = KessenRec(*li)
                br.kessen.append(ks.to_dict())

            if li[0] == "D99":
                if writeFlg:
                    self.blkList.append(br.to_dict())
                
    def kubunCheck(self, meisho):
        m = re.search(r'\dB\d+$', meisho)
        if not m:
            m = re.search(r'\dB\d+-\d+$',meisho)

        if m:
            bl, lot = meisho.split("B")
            kancode = DataUtil.bl_lot2kancode(bl, lot)
            if kancode in self.dicKanCode.keys():
                self.dicKanCode[kancode] = True
                return ('lot', kancode)
            else:
                print("kancode not in kanri = " + kancode)
                return ('lot_err', '')
        else:
            return ("other", "")




