from dataclasses import dataclass, field
from typing import List

@dataclass
class Kanchi:
    block: str
    lot: str
    
@dataclass
class Juzen:
    fcode: str
    choaza: str
    chiban: str

@dataclass()
class Keisan:
    block: str = ''
    lot: str = ''
    horyuMen: str = ''
    horyuKubun: str = ''
    kenrisha: str = ''
    m2sisu: int = 0
    hyoteiSisu: int = 0
    tokiMen: str = ''
    kanchiMen: str = ''
    type1: str = ''
    type2: str = ''
    yusenMen: str = ''
    yusenTanka: int = 0
    teigenRate: str = ''
    zanMen: str = ''
    zanTanka: str = ''
    kingaku: int = 0
    kobetuTanka: int = 0
    kanchiList: List[Kanchi] =field(default_factory= list)
    juzenList: List[Juzen] = field(default_factory= list)


kan = Kanchi("block", "23")
kei = Keisan(block = '83821')
kei.kanchiList.append( kan )
print(kei)