from pymongo import MongoClient
from horyuMatome.matomeRec import *

class ReadMongo:
    def __init__(self, kanri):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client.horyu
        self.kanri = kanri
        self.readSeq()

    def all_cursors_for_horyu(self):
        cursors = self.db.recs.find()
        return cursors

    def readSeq(self):
        #dbHo = ReadMongo()
        cursors = self.all_cursors_for_horyu()

        for c in cursors:
            s = c.get('kakaku')
            if s != None:
                print(s['blk'] + ':' + s['lot'])



        print("end!")


