from pymongo import MongoClient
from reposit.repoKanchi import *

client = MongoClient('localhost',27017)

# 保留地の換地コードのみのレコードで初期化
# 最初の回のみの実行となる。
class MongoHoryuInit:
    horyues = []
    @staticmethod
    def do():
        kanList = RepoKanchi()
        horyues = kanList.getHoryu()
        monDB = HoryuFromGyomuDB()
        monDB.create_from_list(horyues)

    @staticmethod
    def do_N_getHoryues():
        MongoHoryuInit.do()
        return MongoHoryuInit.horyues



class HoryuFromGyomuDB:
    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client.horyu

    def add_one(self, kancode):
        post = {
            'kanchiCode': kancode,
        }
        return self.db.recs.insert_one(post)

    def create_from_list(self, kanList):
        self.db.recs.drop()
        self.db.recs.create_index("kanchiCode", unique = True)
        posts = []
        for r in kanList:
            posts.append({'kanchiCode': r.KanCode, 'menseki': r.Menseki})
        self.db.recs.insert_many(posts)

    def update_one(self, kancode, field, data):
        self.db.recs.update_one({'kanchiCode': kancode},{"$set":{field : data}})

#obj = HoryuFromGyomuDB()
#obj.update_one("01008000", "Name", {"姓": "岡田", "名": "太郎"})
#obj.create_from_list([])
#res = obj.add_one("aabbcc")




