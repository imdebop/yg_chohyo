from horyu.recHoryu import *
from data.dataUtil import *
from dataclasses_json import dataclass_json
from pymongo import MongoClient

class Daicho2Mongo:
    @classmethod
    def setHoryuRec(self, daichoRec):
        keiyakuRec = KeiyakuRec()
        sto = HoryuInputStore(*daichoRec)
        shoCode = self.getShocode(sto)
        hoRec = self.setHoryuKihon(sto, shoCode)
        memList = self.members(sto, shoCode)
        keiyakuRec.members = memList

        hoRec.keiyakuJoho = [keiyakuRec]
        return hoRec

    @classmethod
    def setHoryuKihon(self, sto, shocode):
        hoRec = HoryuRec()
        hoRec.kanchiCode = DataUtil.bl_lot2kancode(sto.blk, sto.lot)
        hoRec.shoCode = shocode
        hoRec.nendoSeireki = sto.seireki
        hoRec.keiyakubi = sto.keiyakubi
        hoRec.menseki = sto.men
        hoRec.tanka = sto.tanka
        hoRec.kingaku = sto.kingaku
        return hoRec
        

    @classmethod
    def getShocode(self, sto):
        sCode = sto.seireki
        wkSeq = '{:02d}'.format(int(sto.seq))
        sCode += wkSeq
        return sCode

    @classmethod
    def members(self, sto, shocode):
        memList = []
        if sto.shimei == None:
            wkShimei = None
            max = 1
        else:
            wkShimei = sto.shimei.split("\n")
            max = len(wkShimei)
        if sto.jusho == None:
            wkJusho = None
        else:
            wkJusho = sto.jusho.split("\n")

        i = 0
        while(i < max):
            member = Member()
            if max > 1:
                suffix = '{:02d}'.format(i)
                member.shoCode = shocode + suffix
            else:
                member.shoCode = shocode + "00"
            member.shimei = self.dataExtra(wkShimei, i)
            member.jusho = self.dataExtra(wkJusho, i)
            memList.append(member)
            i += 1
        return memList

    # 所有者名がコメント等で複数行の時の処理
    def dataExtra(li, i):
        if li == None:
            return "no data"
        if len(li) > i:
            return li[i]
        else:
            return "**no data**"
    
class UpdateMongo:
    def __init__(self):
        self.client = MongoClient('localhost', 27017)
        self.db = self.client.horyu        
    # 台帳データの追加
    def checkNUpdate(self, horec):
        kancode = horec.kanchiCode
        data = self.db.recs.find_one({'kanchiCode': kancode})
        if data == None:
            nen = horec.nendoSeireki
            date = horec.keiyakubi
            print("not found = {},{},{}".format(nen, date, kancode))
            return False
        else:
            self.db.recs.update_one({'kanchiCode': kancode},{'$set' : {'daicho': horec.to_dict()}})
            print(data)
            return True
    # 処分価格データの追加
    def addKakakuData(self, kakakuRec):
        kancode = DataUtil.bl_lot2kancode(kakakuRec.blk, kakakuRec.lot)
        data = self.db.recs.find_one({'kanchiCode': kancode})
        if data == None:
            print("not found = {}".format(kancode))
            return False
        else:
            self.db.recs.update_one({'kanchiCode': kancode},{'$set' : {'kakaku': kakakuRec.to_dict()}})
            return True       


print(int(3))
print('{:03d}'.format(4))
print("ss-dd".split("-"))