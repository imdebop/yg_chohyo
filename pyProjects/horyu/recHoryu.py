import dataclasses
from typing import List
from dataclasses_json import dataclass_json

@dataclasses.dataclass(init=False)
class Member:
    shoCode: str = ""
    shimei: str = ""
    jusho: str = ""
    daihyo: str = ""
    mochibun: str = ""

@dataclasses.dataclass(init=False)
class KeiyakuRec:
    shoCode: str = ""
    idoDate: str = ""
    note: str = ""
    members: list

@dataclass_json
@dataclasses.dataclass(init=False)
class HoryuRec:
    kanchiCode: str = ""
    shoCode: str
    nendoSeireki: str = ""
    keiyakubi: str = ""
    menseki: str = ""
    tanka: int = 0
    kingaku: int = 0
    keiyakuJoho: list

@dataclasses.dataclass
class HoryuInputStore:
    nendo: str
    seireki: str 
    seq: str
    blk: str
    lot: str
    men: str
    tanka: str
    kingaku: str
    keiyakubi: str
    jusho: str
    shimei: str
    kijitu: str
    maekin: str
    zenbu: str
    note: str

    def __post_init__(self):
        self.seireki = str(self.seireki) 
        self.blk = str(self.blk)
        self.lot = str(self.lot)
