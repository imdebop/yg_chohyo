from dataclasses import dataclass
from dataclasses_json import dataclass_json
import os

# 保留地の処分価格の計算レコード
@dataclass_json
@dataclass(init=True)
class KakakuRec():
    blk: str = ""
    lot: str = ""
    menseki: float = 0
    kubun: str = ""
    shoName: str = ""
    m2sisu: int = 0
    hyoSisu: int = 0
    tokiMen: float = 0
    kanchiMen: float = 0
    kigo: str = ""
    type1: str = ""
    type2: str = ""
    yusenMen: float = 0
    yusenTanka: int = 0
    teigenR: float = 0
    zanMen: float = 0
    zanTanka: int = 0
    kingaku: int = 0
    kobetuTanka: int = 0
    biko: str = ""

class KakakuLoad():
    @classmethod
    def do(self):
        kakakuList = []
        fpath = os.getcwd() + '/horyu/kakaku.txt'
        with open(fpath, 'r', encoding='utf-8_sig') as f:
            lines = f.readlines()
            for r in lines:
                if r == '' or r == "\n":
                    continue
                wk = r.replace(",","")
                wk = wk.replace(" ", "")
                hr = KakakuRec(*(wk.split('\t')))
                if hr.blk[0] == "#":
                    continue
                kakakuList.append(hr)
            return kakakuList



#a = KakakuRec()
#a.lot = 'sdf'
#print(a)
