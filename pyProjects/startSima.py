from simaManage import simaRead
from mongoDB.accessMongo import *
from reposit.repoKanchi import *
from reposit.kancodes import GetKancodes_dic
from simaManage.simaRec import *
from simaManage.zahyoUpdate import *
from simaManage.bkLotUpdate import *
from simaManage.chkWithKanri import *
from simaManage.db2simaTxt import *

kanDic = GetKancodes_dic.do()

# sima for daichi
sR_d = simaRead.SimaRead("柳生川南部地区_大地.sim", kanDic)

dbMon = AccessMongo("yagyu")
dbMon.recreate(sR_d.zahyoList, "simaZahyo")

#mongoにロットをセット
dbMon.recreate(sR_d.blkList, "simaLot")

# sima for aduma
sR_a = simaRead.SimaRead("ブロック追加.txt", kanDic)

ZahyoUpdate.do(sR_d, sR_a, dbMon)

bkUP = BlkLotUpdate(dbMon,sR_a)

chk_withKanri = ChkWithKanri(dbMon)
chk_withKanri.diffKancodes()

# simaテキストを書き出し
Db2simaTxt.write(dbMon, "yaguAll.txt")

print("max bkLot id =" + str(bkUP.maxBkLt_id))