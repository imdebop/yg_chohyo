import exUtil.exRead
from horyu.gyomu2mongo import *
# import reposit.repoKanchi
from envCommon import EnvCommon
from horyu.daicho2Mongo import *
from horyu.kakakuRec import *
from dataclasses_json import dataclass_json
import json

# 保留地コレクションを初期化する場合、アンコメント
MongoHoryuInit.do_N_getHoryues()

pathHoryu = EnvCommon.ygHoryu
fpath = pathHoryu + "台帳一覧.xlsx"
data = exUtil.exRead.OpenPXL(fpath,"一覧").getData(startRow = 4, lastRow = 200, startCol = 1, lastCol = 15)

# 保留地台帳と処分価格データを同じクラスで
# mongoDBへ書き込む
    # 台帳データの書き込み
db = UpdateMongo()
for rec in data:
    if rec[0] == None:
        continue
    hoRec = Daicho2Mongo.setHoryuRec(rec)
    db.checkNUpdate(hoRec)
print(json.loads(hoRec.to_json()))

    # 処分価格データの書き込み
kakakuList = KakakuLoad.do()
for r in kakakuList:
    db.addKakakuData(r)

