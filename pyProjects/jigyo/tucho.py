import exUtil.exRead
from collections import namedtuple


class TuchoUtil:
    @classmethod
    def date2key(wareki, dateByDot):
        pass

class Ex2list:
    Item = namedtuple('Item', ('date', 'kozaID', 'ginko', 'himokuID', 'himoku', 'tekiyo', 'kingaku'))

    @classmethod
    def extract(self, exPath, sheet, tplRange):
        startRow, lastRow, startCol, lastCol = tplRange
        data = exUtil.exRead.OpenPXL(exPath, sheet).getData( startRow ,lastRow, startCol, lastCol)
        dic = {}
        for r in data:
            r[0] = self.date2seq( sheet, r )
            rr = self.Item(*r)
            if rr.himokuID != None:
                key = self.editKey(rr)
                if  key in  dic.keys():
                    dic[key].append(list(rr))
                else:
                    dic[key] = [list(rr)]
        li = list(dic.keys())
        li.sort()
        for key in li:
            recs = dic[key]
            print(key + " *******************")
            for rec in recs:
                rec = self.recCheck(rec)
                print("\t".join(rec))
    
    def recCheck(rec):
        if isinstance(rec[3], int):
            rec[3] = "{:1d}".format(rec[3])
        if isinstance(rec[3], float):
            rec[3] = "{:1.1f}".format(rec[3])
        if rec[5] == None:
            rec[5] = ""
        if isinstance(rec[6], int):
            rec[6] = "{:1d}".format(rec[6])
        return rec 

    def date2seq(wareki, r):
        wa_int = int(wareki) 
        if r[0] == None:
            mon = "**"
            wa_int = 9999
        else:
            mon = r[0][0:2]
            if int(mon) < 5:
                wa_int = wa_int + 1
        if wa_int == 31:
            return "2019_" + r[0]
        elif wa_int == 32:
            return "2020_" + r[0]
        else:
            return "****_**"

    def editKey(tpr):
        id = '{:1.1f}'.format(tpr.himokuID)
        name = tpr.himoku
        if tpr.tekiyo == None:
            tekiyo = ""
        else:
            tekiyo = tpr.tekiyo
        return id + ":" + name


