from dataclasses_json.api import C
from simaManage.simaRec import ZahyoRec
import sqlite3
from simaManage.simaRead import SimaRead
from reposit.kancodes import GetKancodes_dic
from reposit.repoZahyo import RepoZahyo

rz = RepoZahyo()

kanDic = GetKancodes_dic.do()

def clearZahyoDb():
  rz.clearZahyoDb()

def simaBaseInsert():
  sRead = SimaRead('柳生川南部地区_大地.sim', kanDic)
  data = []
  for k, v in kanDic.items():
    if v == False:
      pass
      #print('sima zahyo missing = ' + k)
  for r in sRead.zahyoList:
    tpl = (r["tenban"], r["meisho"], r["x"], r["y"], r["z"], r["note"])
    data.append(tpl)

  #print(sRead.zahyoList)
  wklist = rz.getZahyoList()
  print(data[0])
  rz.bulkInsert(data)

#simaBaseInsert()
clearZahyoDb()
