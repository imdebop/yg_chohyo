import peeWeeDB.recKanchi
from peeWeeDB.recKanchi import Kanchi
from peeWeeDB.recKanchi import Horyu
from data.dataUtil import *
#from exUtil.exORM import *
import exUtil.exWin32
from data.recRepoKanchi import *


class RepoKanchi:
    def __init__(self):
        self.datautil = DataUtil()
        self.list_db()
        self.write_ex()

    def list_db(self):
        peeWeeDB.recKanchi.db.connect()
        self.kan_list = list(Kanchi.select())

        self.dic_horyu = {}
        for h in Horyu.select():
            self.dic_horyu[h.KanCode] = h

        kancode = self.kan_list[0].KanCode
        (bl, lot, eda) = self.datautil.kancode2bl_lot(kancode)
        print(bl, lot, eda)

    def write_ex(self):
        #print(self.kan_list.count())
        #try:
            ex_kan = IfExKanchi(exUtil.exWin32.exwin)
            ex_kan.open_book()
            ex_kan.clear_range(11, 2, 2011, 234)
            write_row = ExWriteKan(ex_kan, self.dic_horyu)
            row_cnt = 11
            for k in self.kan_list[0:150]:
                rrkan = self.set_kanchi(k)
                write_row.write_row(row_cnt, rrkan)
                row_cnt += 1
        #except * as e:
            pass
        #finally:
            pass
            ex_kan.close(True)

    def set_kanchi(self, kanchi):
        soko_list = self.edit_sokochi(kanchi)
        horyu = self.get_horyu(kanchi)
        rrkan = RecRepoKanchi(kanchi, horyu, soko_list)
        return rrkan

    def get_horyu(self, kanchi):
        kancode = kanchi.KanCode
        if kancode in self.dic_horyu:
            return self.dic_horyu[kancode]
        else:
            return None
        
    def edit_sokochi(self, kanchi):
        soko_str = kanchi.Sokochi
        if soko_str[:1] == "*":
            print(soko_str)
        rec_soko_list = self.datautil.juzen_edit_sokochi(soko_str)
        return rec_soko_list

class ExWriteKan:
    def __init__(self, if_ex_kan, dic_horyu):
        self.ex_kan = if_ex_kan
        self.dic_horyu = dic_horyu

    def write_row(self, row, rrkanchi):
        print(rrkanchi.kan.KanCode)
        self.ex_kan.set_rc_str(row, 2, rrkanchi.b_l.blk)
        self.ex_kan.set_rc_str(row, 3, rrkanchi.b_l.lot_eda)
        self.ex_kan.set_rc_str(row, 4, rrkanchi.kan.Menseki)
        self.ex_kan.set_rc_str(row, 5, rrkanchi.kan.ShoCode)
        self.ex_kan.set_rc_str(row, 6, rrkanchi.kan.HeibeiSisu)
        self.ex_kan.set_rc_str(row, 7, rrkanchi.kan.Memo)
        self.write_horyu(row, rrkanchi)

    def write_horyu(self, row, rrkanchi):
        if rrkanchi.kan.ShoCode == '5000':
            ho = self.dic_horyu[rrkanchi.kan.KanCode]
            self.ex_kan.set_rc_str(row, 8, ho.Shubetu)
            self.ex_kan.set_rc_str(row, 9, ho.Nendo)




class IfExKanchi:
    def __init__(self, exwin):
        self.exwin = exwin
        self.rk = RepoKanchi

    def open_book(self):
        self.exwin.open_ex()

    def close(self, dum):
        self.exwin.close_ex()
        self.exwin.close_app()

    def set_book(self, book_path):
        pass

    def set_sheet(self, sheet_name):
        pass

    def set_rc_str(self, r, c, val):
        self.exwin.set_rc_str(r, c, val)

    def set_rc_int(self, val):
        pass

    def set_rc_fload(self, val):
        pass

    def clear_range(self, r1, c1, r2, c2):
        self.exwin.range_clear(r1, c1, r2, c2)

rk = RepoKanchi()
#rk.list_db(rk)

#exw = exUtil.exWin32.exwin
#exw.open_ex()
#exw.close_ex()
#exw.close_app()
