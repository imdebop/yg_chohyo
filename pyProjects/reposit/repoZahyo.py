from peeWeeDB.recZahyo import *

class RepoZahyo:
  def getZahyoList(self):
    return (Zahyo.select())

  def bulkInsert(self, data):
    Zahyo.insert_many(data, fields=[
      Zahyo.Tenban,
      Zahyo.Tenmei,
      Zahyo.X,
      Zahyo.Y,
      Zahyo.Z,
      Zahyo.Memo
    ]).execute()
    
  def clearZahyoDb(self):
    Zahyo.delete().execute()

  