#from peeWeeDB.recKanchi import Kumi

class KumiConv:
    def __init__(self, repoKanchi):
        self.convDic = {}
        self.kumiDic = {}
        kumiRecs = repoKanchi.getKumi()
        for k in kumiRecs:
            shocode, seq = k.Key[:4], k.Key[4:6]
            fudeArr = k.Fcodes.split('/')
            for fude in fudeArr:
                key = "ju2sho:" + fude
                self.convDic[key] = shocode
            kanArr = k.Kanchis.split('/')
            for kan in kanArr:
                key = "kan2sho:" + kan
                self.convDic[key] = shocode
            
            if shocode in self.kumiDic:
                self.kumiDic[shocode].append(k)
            else:
                self.kumiDic[shocode] = [k]
    
    def getShoCodeByKancode(self, kancode):
        key = "kan2sho:" + kancode
        if key in self.convDic:
            return self.convDic[key]
        else:
            return '*no_data*'

    def getFuderecsByShocode(self, shocode):
        pass

    def getFuderecsByKancode(self, kancode):
        shocode = '' 
