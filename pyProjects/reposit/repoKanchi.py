from data.dataUtil import *
from peeWeeDB.recKanchi import *
from horyu.gyomu2mongo import *

# 換地データを保持するクラス
class RepoKanchi:
    db.connect()
    _instance = None
    @classmethod
    def getSingleton(cls):
        if not cls._instance:
            cls._instance = cls()
        return cls._instance

    def __init__(self):
        self.datautil = DataUtil()
        self.list_db()

    def list_db(self):
        # db.connect()
        self.kan_list = list(Kanchi.select())

    def list_kanchi(self):
        self.kan_list = list(Kanchi.select())
        return self.kan_list #20210306

    def getKanchiByKey(self, kancode): #20210309
        return Kanchi.select().where(Kanchi.KanCode == kancode)[0]

    def getShoyuByKey(self, shocode):
        return Shoyu.select().where(
            Shoyu.ShoCode == shocode
        )[0]
    
    def getJuzenByKey(fcode):
        return Juzen.select().where(
            Juzen.FudeCode == fcode
        )[0]

    def getKumi(self):
        return list(Kumi.select())

    def getHoryu(self):
        wkList = []
        for k in self.kan_list:
            if k.ShoCode == "5000":
                wkList.append(k)
        return wkList
    
    def getHoryuOpt(self):
        return list(Horyu.select())

    def getHoryuOptDic(self):
        wkDic = {}
        for h in self.getHoryuOpt():
            wkDic[h.KanCode] = h
        return wkDic

    def getAllKancodes(self):
        wkList = []
        for k in self.kan_list:
            wkList.append(k.KanCode)
        return wkList

    def getKaishiByKancode(self, kancode):
        return Kaishi.select().where(Kaishi.KanCode == kancode)

    def convSokochi(self, sokoStr):
        pass
        


class RepoKaishi:
    def __init__(self):
        self.repoKanchi = RepoKanchi()
        self.kaishiList = list(Kaishi.select())
        print("aaa")


# 保留地の換地コードのみのレコードで初期化
# 最初の回のみの実行となる。
class MongoHoryuInit:
    horyues = []
    @staticmethod
    def do():
        kanList = RepoKanchi()
        horyues = kanList.getHoryu()
        monDB = HoryuFromGyomuDB()
        monDB.create_from_list(horyues)

    @staticmethod
    def do_N_getHoryues():
        MongoHoryuInit.do()
        return MongoHoryuInit.horyues


#MongoHoryuInit.do("wwwwwaaa")

