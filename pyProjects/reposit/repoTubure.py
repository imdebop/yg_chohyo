from peeWeeDB.recTubure import *

class RepoTubure:
  def getTubure(self):
    return (Tubure.select())

  def bulkInsert(self, data):
    Tubure.insert_many(data, fields=[
      Tubure.Id,
      Tubure.Rosen,
      Tubure.Shubetu,
      Tubure.Chimoku,
      Tubure.Choaza,
      Tubure.Chiban,
      Tubure.Shoyusha,
      Tubure.Kuiki,
      Tubure.Kubun,
      Tubure.Tanka,
      Tubure.Menseki,
      Tubure.Bubun,
      Tubure.Kakaku,
      Tubure.Nendo,
      Tubure.Kurikoshi
    ]).execute()
    
  def clearDb(self):
    Tubure.delete().execute()

  