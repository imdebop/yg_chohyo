from collections import namedtuple
from re import split
from reposit.repoIten import compNendoKonkyo
import typing
import openpyxl
from openpyxl.styles import colors
from peewee import Column
from exUtil.exRead import OpenPXL
from openpyxl.worksheet.worksheet import Worksheet
from reposit.repoIten.itenMain import RepoIten
from data.recIten import ItenExData
from exUtil.setStyle import EditExel
from typing import NamedTuple
from reposit.repoIten.out2setFormat import SetFormat
import math
from reposit.repoIten.compNendoKonkyo import *
from . import itenCodes
from .out2konkyoMinaoshi import KonkyoMinaosiNum

class ToggleRow:
    state: bool = False
    currTatemonoNo: str = ""
    @classmethod
    def toggle(cls):
        cls.state = not cls.state
    @classmethod
    def check(cls, val: str):
        if val == "":
            cls.toggle()
        elif cls.currTatemonoNo != val:
            cls.currTatemonoNo = val
            cls.toggle()

class Out2konkyoEx:
    Opxl: OpenPXL
    def __init__(self, path2konkyoExOut: str, sheetname: str, out2konkyoExPath: str, rpIten: RepoIten) -> None:

        self.Opxl = OpenPXL(path2konkyoExOut, sheetname)
        sd = SetData2ex(self.Opxl.ws)
        r: dict
        for rowNo, r in enumerate(rpIten.tdb, start= 1):
            #rr: ItenExData = ItenExData(**r)
            #hojoData: list = rr.hojoRecs()
            sd.setRow(rowNo, r)
            #break
        self.Opxl.wb.save(out2konkyoExPath)


class SetData2ex:

    def __init__(self, ws: Worksheet) -> None:
        self.ws = ws
        self.colHs = 1  #header start
        self.colHe = 10 #header end
        self.hdrRange = range(1, 12) # => 1 - 11        
        self.sRow = 3
        self.curRow = self.sRow


    #根拠別out -> A3セルからスタート
    def setRow(self, rowNo: int, r: dict):
        rr = ItenExData(**r)
        ToggleRow.check( rr.minaoshi )
        flgUnpaid: bool = False
        #minaoshi = rr.minaoshi #新しく付番した戸数の連番
        #if minaoshi == '':
        #    minaoshi = '0'

        colHojo = 11
        val:str
        code:str = ""
        nendo: int # 17 => H17 を表す連番　令和元年=>31
        hojoData: list = rr.hojoRecs()
        for i, val in enumerate(hojoData, start=1):
            cell = self.ws.cell(row=self.curRow, column= colHojo + i)
            if val == "":
                if ToggleRow.state:
                    EditExel.setPattern(cell, 'gray125')
            else:
                num, code, state = itenCodes.hojoShubetu2tuple(val)
                cellColor = SetFormat.fillColor(code)
                a = math.floor(int(num) / 1000) #金額を千円単位に
                cell.value = a

                EditExel.setCellColorByType(cell, cellColor)
                if state == "未":
                    flgUnpaid = True
                    EditExel.setAsMikanryo(cell)

                nendo = i + 16 #年度は H17から
                CompNendoKonkyo.setKonkyo(rr.seiriNo, nendo, num, rowNo) #num  は金額
                
        #未完了のヘッダー着色のために処理を後ろで行う
        hdrData: list = rr.headerData()
        for i, val in enumerate(hdrData, start=1):
            self.ws.cell(row=self.curRow, column= i).value = val
            if flgUnpaid:
                EditExel.setColor(self.ws, self.curRow, i, 'blue')

        self.curRow += 1

        #row = 8
        #col = 10
        #color = 'pink'
        #EditExel.setColor(ws, row, col, color)
        pass





