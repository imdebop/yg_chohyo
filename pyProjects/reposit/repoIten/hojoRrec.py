from dataclasses import dataclass

@dataclass
class HojoRec:
    nendo: str
    gaku: int
    cd: str
    state: str