from os import pardir
from reposit.repoIten import itenRead, repoItenShuhyo, itenChk
from peewee import quote
from envCommon import EnvCommon
from collections import namedtuple
from data.recIten import *
from reposit.repoIten.itenUtil import *
from reposit.repoIten.compNendoKonkyo import *
from reposit.repoIten.itenPath import *
import csv


#エクセルデータ読み込み処理を定義　#################################
def loadItenNew():
    #移転計画　主表からデータ取得
    # startRow, lastRow, startCol, lastCol
    TplRange = namedtuple('TplRange', ('sR', 'lR', 'sC', 'lC'))
    tpr = TplRange( 4, 742, 4, 26)

    outPath = "out/itenShuhyo.csv"
    if ItenExelUpdated.chkIfUpdated(ItenPath.shuhyoPath, outPath):
        print('主表は最新です')
    else:
        print('loading エクセル　主表')
    
        dataList = itenRead.Ex2list.extract(ItenPath.shuhyoPath, "主表", tpr)
        #print(dataList[0])
        #rpItenShuhyo = RepoItenShuhyo(dataList)
        with open(outPath, "w", encoding='UTF-8', newline='\n') as f:
            write = csv.writer(f, quotechar = '"')
            write.writerows(dataList)

#年度別データの読み込み #############################################
    tpr = TplRange( 4, 830, 2, 22)
    outPath = 'out/itenShuNendo.csv'
    if ItenExelUpdated.chkIfUpdated(ItenPath.nendPath, outPath):
        print('年度別データは最新です')
    else:
        print('loading エクセル　年度別')
        dataList = itenRead.Ex2list.extract(ItenPath.nendPath, "Sheet1", tpr)
        with open("out/itenShuNendo.csv", "w", encoding='UTF-8', newline='\n') as f:
            wklist = []
            for r in dataList:
                wk = []
                for rr in r:
                    if rr == None:
                        wk.append("")
                    else:
                        wk.append(str(rr).replace(" ",""))
                wklist.append(wk)
            write = csv.writer(f, quotechar = '"' )
            write.writerows(wklist)
#exit()

def loadItenKonyobetu():
    global isKonkyoNew
#根拠別移転費 読み込み処理 #############################

## startRow, lastRow, startCol, lastCol
    TplRange = namedtuple('TplRange', ('sR', 'lR', 'sC', 'lC'))
    tpr = TplRange( 3, 890, 1, 36)

    fpath = ItenPath.konkyobetuPath
    
    outPath = "out/itenKonkyobetu.csv"
    if ItenExelUpdated.chkIfUpdated(fpath, outPath):
        print('根拠別 は最新です')
        return
    else:
        print('loading エクセル　根拠別')

    dataList = itenRead.Ex2list.extract(fpath, "Sheet2", tpr)
    #rpIten = RepoIten(dataList)

    with open(outPath, "w", encoding='UTF-8', newline='\n') as f:
        wklist = []
        for r in dataList:
            wk = []
            for rr in r:
                if rr == None:
                    wk.append("")
                else:
                    wk.append(str(rr).replace(" ",""))
            wklist.append(wk)
        write = csv.writer(f, quotechar = '"')
        write.writerows(wklist)
    #isKonkyoNew = ItenExelUpdated.chkIfUpdated(fpath, outPath)

loadItenNew()
loadItenKonyobetu()
