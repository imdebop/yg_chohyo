from openpyxl.styles import colors
from exUtil.exRead import OpenPXL
from openpyxl.worksheet.worksheet import Worksheet
from reposit.repoIten import itenKanryoShukei
from reposit.repoIten.itenMain import RepoIten
from data.recIten import ItenExData
from exUtil.setStyle import EditExel
from reposit.repoIten.out2setFormat import SetFormat
import math
from reposit.repoIten.compNendoKonkyo import *
from . import itenCodes
from .itenKanryoShukei import *
#from .out2konkyoMinaoshi import KonkyoMinaosiNum

class Out2shukeiEx:
    Opxl: OpenPXL
    def __init__(self, path2shukeiExOut: str, sheetname: str, out2shukeiExPath: str) -> None:

        self.Opxl = OpenPXL(path2shukeiExOut, sheetname)
        sd = SetData2ex(self.Opxl.ws)
        r: ItenShukeiData
        #hcd: HojoCd
        hcds = [hcd for hcd in itenCodes.hojoCodeList() if hcd.cds != "NA"] #種別の順に集計レコードを出力
        for hcd in hcds:
            r = ItenKanryoShukei.dictShubetuSum[hcd.cds]
            sd.setRow(r)
            #break
        self.Opxl.wb.save(out2shukeiExPath)

        rtKeys = list(ItenKanryoShukei.dicTatemonoSum.keys())
        rtKeys.sort()
        rtMikan = []
        kanryoNum = 0
        for key in rtKeys:
            kanryoNen = ItenKanryoShukei.kanryoNen
            rt = ItenKanryoShukei.dicTatemonoSum[key]
            hojoList: list = rt.hojoRecs()
            kanryo, mikan = rt.hojoKanMikanSum(kanryoNen)
            if kanryo > 0:
               kanryoNum += 1 

        print("支払済建物=" + str(kanryoNum))


class SetData2ex:

    def __init__(self, ws: Worksheet) -> None:
        self.ws = ws
        self.colHs = 1  #header start
        self.colHe = 10 #header end
        self.hdrRange = range(1, 12) # => 1 - 11        
        self.sRow = 3
        self.curRow = self.sRow


    #根拠別out -> A3セルからスタート
    def setRow(self, r: ItenShukeiData):
        #ヘッダー編集, 種別名称をセット
        self.ws.cell(row=self.curRow, column= 2).value = r.shukeiId
            #if flgUnpaid:
            #    EditExel.setColor(self.ws, self.curRow, i, 'blue')

        colHojo = 11
        val:str
        code:str = ""
        nendo: int # 17 => H17 を表す連番　令和元年=>31
        hojoData: list = r.hojoRecs()
        for i, val in enumerate(hojoData, start=1):
            cell = self.ws.cell(row=self.curRow, column= colHojo + i)
            if val == 0:
                continue
            else:
                cell.value = val

        self.curRow += 1