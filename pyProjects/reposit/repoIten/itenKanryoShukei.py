#完了、未完了等の集計を行う
from data.recItenShuhyo import ItenShuNendoData
from typing import Dict,List, NamedTuple
from data.recIten import ItenExData
from data.recItenShukei import *
from .itenCodes import HojoCd, hojoCodeList
#from collections import namedtuple

class HojoData(NamedTuple): #補助金１件の最小データ
    nendoKey: str
    kingaku: int
    id: str

class ItenKanryoShukei:
    kanryoNen = 31 #平成年で指定
    totalSum = ItenShukeiData()
    dicTatemonoSum: Dict[str, ItenShukeiData] = {}
    dictShubetuSum: Dict[str, ItenShukeiData] = {} #末尾で初期化される
    dictHojoCd = {hd.cds: hd.name for hd in hojoCodeList() }

    @classmethod
    def shukei(cls, irec: ItenExData):
        key = irec.minaoshi
        if key != "":
            if key in cls.dicTatemonoSum:
                sumRecTate = cls.dicTatemonoSum[key]
            else:
                sumRecTate = ItenShukeiData()
                sumRecTate.minaoshi = key
                cls.dicTatemonoSum[key] = sumRecTate
            
        else:
            sumRecTate = None

        cls.add2total(sumRecTate, irec)


    @classmethod
    def add2total(cls, sumRecTate: ItenShukeiData, iRec: ItenExData) -> bool:
        for i, v in enumerate(iRec.hojoRecs(), start=17): # 17は平成１７年の意味
            if v == "":
                continue
            hd: HojoData = cls.toHojoData(i, v)
        
            #建物番号（見直し番号)別の集計
            if sumRecTate is not None:
                kin = getattr(sumRecTate, hd.nendoKey)
                setattr(sumRecTate, hd.nendoKey, kin + hd.kingaku)
            #全体集計
            kin = getattr(cls.totalSum, hd.nendoKey)
            setattr(cls.totalSum, hd.nendoKey, kin + hd.kingaku)
            #種別集計
            sR = cls.dictShubetuSum[hd.id]
            sR.shukeiId = cls.dictHojoCd[hd.id]
            kin = getattr(sR, hd.nendoKey)
            setattr(sR, hd.nendoKey, kin + hd.kingaku)


    @classmethod
    def toHojoData(cls, hN: int, v: str) -> HojoData:
        # hN:平成年度
        # v:年度別の１件の値(金額/種別)
            if "/" in v:
                kin, id = v.split("/")[:2]
            else:
                kin = v; id = "tan"

            if hN <= 31:
                key = "H" + str(hN).zfill(2)
            else:
                key = "R" + str(hN - 30).zfill(2)
            
            return HojoData(key, int(kin), id)

    
    #dicShubetuSumを初期化
    @classmethod
    def initDicShubetuSum(cls):
        r: HojoCd
        for r in hojoCodeList():
            if r.cds == "NA":
                continue
            cls.dictShubetuSum[r.cds] = ItenShukeiData()

ItenKanryoShukei.initDicShubetuSum()

    
