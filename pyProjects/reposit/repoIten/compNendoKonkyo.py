from data.recIten import ItenExData
from data.recItenShuhyo import ItenShuNendoData
from typing import Dict

class CompNendoKonkyo:
    dicNendo: Dict[str, str] = {} #dict[ "年度:整理番号:金額（カンマなし）", "行番号" ]
    dicKonkyo: Dict[str, str] = {}

    @classmethod
    def setKonkyo(cls,seiriNo: str, nen: int, kingaku: str, rowNo: int): #rowNo は検索用で何行目かを表す
        hojoData = seiriNo + ":" + str(nen) + ":" + kingaku
        if hojoData in cls.dicKonkyo:
            row1 = cls.dicKonkyo[hojoData]
            print("konkyo duplicate=" + hojoData + "  " + str(row1) + ":" + str(rowNo))
        else:
            cls.dicKonkyo[hojoData] = rowNo
        pass

    @classmethod
    def setNendo(cls, keySeiri: str, isRec: ItenShuNendoData, rowNo: int):
        eda = keySeiri[-2:]
        if eda == "00":
            seiriNo = keySeiri[:-2].lstrip("0")
        else:
            seiriNo = keySeiri[:-2].lstrip("0") + "." + keySeiri[-2:]
        nen = isRec.nendo
        kingaku = isRec.kingaku
        hojoData = seiriNo + ":" + str(nen) + ":" + kingaku
        if hojoData in cls.dicNendo:
            row1 = cls.dicNendo[hojoData]
            print("nendo duplicate=" + hojoData + "  " + str(row1) + ":" + str(rowNo))
        else:
            cls.dicNendo[hojoData] = rowNo
        pass
