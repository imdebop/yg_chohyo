#print("repoShuhyo was read")
from types import ClassMethodDescriptorType
from typing import Dict, List
from data.recItenShuhyo import *
from data.numUtil import *
from reposit.repoIten.compNendoKonkyo import *

class RepoShuhyo:
    dictTateNew: Dict[int, ItenShuhyoData] ={}
    dictTateNendo: Dict[str, List[ItenShuNendoData] ] ={}

    #現在の建物番号を使う
    @classmethod
    def setTateNew(cls, recList: List):
        for r in recList:
            isRec = ItenShuhyoData(*r)
            key = cls.getKey(isRec)
            cls.dictTateNew[key] = isRec

    #年度別リストを読み込み
    @classmethod
    def setTateNendo(cls, recList: List) -> Dict[str, List[ItenShuNendoData]]:
        for rowNo, r in enumerate(recList, start=1):
            isRec = ItenShuNendoData(*r)
            if isRec.kingaku == "0":
                continue
            wk = isRec.tatemonoNo
            if NumUtil.is_num(wk):
                wklist = wk.split(".")
                if len(wklist) == 1:
                    #枝番がない場合・工作物
                    if len(wk) == 4:
                        key = wk + "00"
                        #print(wk + "*", end="")
                    else:
                        #枝番のない建物番号
                        key = wk + "01"
                    cls.add2dicTateNendo(key, isRec)
                else:
                    moto, eda = wklist
                    if len(eda) == 1: #198.5 -> 198.50
                        eda = eda + "0"
                    key = str(int(moto)).zfill(4) + str(int(eda)).zfill(2)
                    cls.add2dicTateNendo(key, isRec)
            else:
                continue
            CompNendoKonkyo.setNendo(key, isRec, rowNo) # keyはseiriNoにゼロがついている
        return cls.dictTateNendo

    @classmethod
    def add2dicTateNendo(cls, key, r: ItenShuNendoData):
        if key in cls.dictTateNendo:
            #年度が降順のため、逆順に追加
            cls.dictTateNendo[key].insert(0, r)
        else:
            cls.dictTateNendo[key] = [r]


    @classmethod
    def getKey(cls, isRec: ItenShuhyoData):
        moto = isRec.tatemonNo
        eda = isRec.shinEda
        key = moto.zfill(4) + eda.zfill(2)
        return key



