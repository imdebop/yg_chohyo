from tinydb import TinyDB, Query
from .repoItenShuhyo import *
from .hojoRrec import *
from data.recIten import *

class ItenChk:
    tdb: TinyDB #根拠別データ
    listKensuUnmatch: List[List] = []
    listKingakuUnmatch: List[List] = []

    def __init__(self, tdb: TinyDB) -> None:
        print("キー不存在=>主表")
        for r in tdb.all():
            key = r['key']
            if key != "":
                if float(r['seiriNo']) < 1000:
                    if key not in RepoShuhyo.dictTateNew:
                        print(key + ":" + r['seq'] + "/ ", end="")

        print("\n建物キー不存在=>年度別")
        for r in tdb.all():
            key = r['key']
            if key != "":
                if float(r['seiriNo']) < 1000:
                    if key not in RepoShuhyo.dictTateNendo:
                        print(key + ":" + r['seq'] + "/ ", end="")

    @classmethod
    def compKonkyo2Nendo(cls, key: str, tosho: List[HojoRec], listNendoBetu: List[ItenShuNendoData]) -> None:
        n1 = len(tosho)
        n2 = len(listNendoBetu)
        if n1 != n2:
          #cls.listKensuUnmatch.append([key, n1, n2])
          cls.listKensuUnmatch.append([key, tosho, [r.nenGaku() for r in listNendoBetu]])
        else:
          for i in range(n1):
            #当初の金額
            konKingaku = tosho[i].gaku
            #年度別の金額
            nenKingaku = int(listNendoBetu[i].kingaku)
            nendo = listNendoBetu[i].nendo
            if konKingaku != nenKingaku:
              #print(key + "<>", end="")
              cls.listKingakuUnmatch.append([i, n1, nendo, key, konKingaku, nenKingaku])

        pass
