class SetFormat:
    @classmethod
    def fillColor(cls, hojoType: str) -> str:
        if(hojoType == "cc"): #地活　直物
            return "yellow"
        elif(hojoType == "cy"): #
            return "yellow-paleBlue"
        elif(hojoType == "sc"): #市　直物
            return "pink"
        elif(hojoType == "sy"): #市　用地
            return "pink-paleBlue"
        elif(hojoType == "mc"): #まち交　直物
            return "lightGreen"
        elif(hojoType == "my"): #まち交　用地
            return "lightGreen-paleBlue"
        elif(hojoType == "mkc"): #まち交　経対直物
            return "blue"
        elif(hojoType == "mky"): #まち交　経対用地
            return "blue-white"
        elif(hojoType == "tan"): #単費
            return "vermilion"
        elif(hojoType == "ni"): #無利子貸付
            return "paleBlue"
        elif(hojoType == "tu"): #通常
            return "orange"
        elif(hojoType == "ky"): #公管金　用地
            return "green"
        elif(hojoType == "NA"): #要確認
            return ""
        else:
            return "NA"