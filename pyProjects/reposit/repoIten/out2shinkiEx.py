from collections import namedtuple
from re import I, split
from reposit.repoIten import extract2shinki
from reposit.repoIten.itenCodes import ItenCodes
from reposit.repoIten.extract2shinki import Extract2shinki
from exUtil.exRead import OpenPXL
from openpyxl.worksheet.worksheet import Worksheet
from reposit.repoIten.itenMain import RepoIten
from data.recIten import ItenExData
from exUtil.setStyle import EditExel
from reposit.repoIten.out2setFormat import SetFormat
import math
from reposit.repoIten.compNendoKonkyo import *
from .itenPath import *
from .itenCodes import *
from .extract2shinki import *
import data.nenUtil

class Out2shinkiEx:
    Opxl: OpenPXL
    def __init__(self) -> None:

        self.Opxl = OpenPXL(ItenPath.out2shinkiExForm, "Sheet1")
        sd = SetData2ex(self.Opxl.ws)
        #r: ItenExData
        for r in Extract2shinki.listShinkiExtract:
            sd.setRow(r)
            #break
        sd.curRow += 5
        for r in Extract2shinki.listTuikaExtract:
            sd.setRow(r)
            
        sd.curRow += 5
        for r in Extract2shinki.listMikanExtract:
            sd.setRow(r)
        
        sd.curRow += 5
        sd.ws.cell(row = sd.curRow, column = 13).value = "建物計"
        sd.ws.cell(row = sd.curRow, column = 14).value = Extract2shinki.subtotalTatemono
        sd.curRow += 1
        sd.ws.cell(row = sd.curRow, column = 13).value = "工作物計"
        sd.ws.cell(row = sd.curRow, column = 14).value = Extract2shinki.subtotalKosaku
        
        #費目別・年度別合計
        sd.curRow += 3
        colNendo = 15
        for k, v in Extract2shinki.dictTotal.items():
            sd.ws.cell(row = sd.curRow, column = 13).value = ItenCodes.dicAbbr2name[k].name
            sd.ws.cell(row = sd.curRow, column = 14).value = v
            for kk, vv in Extract2shinki.dictNendoSum[k].items():
                sd.ws.cell(row = sd.curRow, column = colNendo + kk).value = vv
            sd.curRow += 1

        sd.curRow += 1
        sd.ws.cell(row = sd.curRow, column = 13).value = "工作物年度別"
        for k, v in Extract2shinki.dicKosakuNendoSum.items():
            #nen = data.nenUtil.NenUtil.h2R(k + 17)
            #sd.ws.cell(row = sd.curRow, column = 13).value = nen
            sd.ws.cell(row = sd.curRow, column = 15 + k).value = v
            #sd.curRow += 1

        self.Opxl.wb.save(ItenPath.out2shinkiExPath)


class SetData2ex:

    def __init__(self, ws: Worksheet) -> None:
        self.ws = ws
        self.colHs = 1  #header start
        self.colHe = 10 #header end
        self.hdrRange = range(1, 12) # => 1 - 11        
        self.sRow = 3
        self.curRow = self.sRow


    #根拠別out -> A3セルからスタート
    def setRow(self, irec: ItenExData):
        colHojo = 12
        val:str
        code:str = ""
        nendo: int # 17 => H17 を表す連番　令和元年=>31

        #flgDup: bool = False
        for nen, val in enumerate(irec.hojoRecs(), start=17):
            if val == "":
                pass
            else:
                #補助金のヘッダーデータ編集
                self.editHeader(irec)
                #flgDup = True
                hojoTpl = self.hojoRecTpl(nen, val)
                for i, r in enumerate(hojoTpl, start= 0):
                    self.ws.cell(row= self.curRow, column= colHojo + i).value = hojoTpl[i]

                self.curRow += 1


    def editHeader(self, irec: ItenExData):
        hdrData: list = irec.headerData()
        minaoshi = irec.minaoshi #新しく付番した戸数の連番
        if minaoshi == '':
            minaoshi = '0'
        hojoData: list = irec.hojoRecs()
        for i, val in enumerate(hdrData, start=1):
            if i == 10 and val != "":
                self.ws.cell(row=self.curRow, column= i).value = int(val)
            else:
                self.ws.cell(row=self.curRow, column= i).value = val
            if int(float(minaoshi)) > 500:
                EditExel.setColor(self.ws, self.curRow, i, 'blue')

    def hojoRecTpl(self, nen: int, hojo: str) -> tuple:
        if hojo != "":
            kin, code, state = hojoShubetu2tuple(hojo)
        nendo = self.heiseiReiwa2str(nen)
        shubetu: str = ItenCodes.getNameByAbbr(code)
        kinInt = int(kin)
        return (nendo, shubetu, kinInt)


    def heiseiReiwa2str(cls, nen: int) -> str:
        if nen < 17 or nen > 50:
            return 999
        else:
            if nen > 30:
                n = nen - 30
                return "R" + str(n)
            else:
                return str(nen)
