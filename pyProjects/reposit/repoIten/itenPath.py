from envCommon import EnvCommon

class ItenPath: 
    #根拠別エクセルシート出力パス
    out2konkyoExForm: str
    out2konkyoExPath: str

    pathItenRoot = EnvCommon.ygRoot + "R02事業計画/"
    shuhyoPath = pathItenRoot + "01.柳生側南部　移転計画（更新用）R3繰越額入力済.xlsx"
    nendPath = pathItenRoot + "移転費年度別.xlsx"
    konkyobetuPath = pathItenRoot + "路線別建物補償費_editing.xlsx"

    out2konkyoExForm = pathItenRoot + "路線別枠.xlsx"
    out2shinkiExForm = pathItenRoot + "新規抽出枠.xlsx"
    out2shukeiExForm = pathItenRoot + "集計枠.xlsx"
    out2konkyoExPath = pathItenRoot + "out路線別.xlsx"
    out2shinkiExPath = pathItenRoot + "out新規.xlsx"
    out2shukeiExPath = pathItenRoot + "out集計.xlsx"
    