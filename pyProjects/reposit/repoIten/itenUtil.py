import os
import re

class ItenUtil:
  pass

class ItenExelUpdated:
  reloadAll: bool = False

  #データが最新なら True
  @classmethod
  def chkIfUpdated(cls, indata: str, outdata: str) -> bool:
    try:
      inSec = os.stat(indata).st_mtime
      outSec = os.stat(outdata).st_mtime
    except FileNotFoundError:
      return False
      
    if cls.reloadAll:
      return False
    else:
      return  outSec > inSec

