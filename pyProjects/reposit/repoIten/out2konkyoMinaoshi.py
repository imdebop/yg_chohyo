#見直し番号の連番をやり直す
#
from data.recIten import *

class KonkyoMinaosiNum:
    dicMinaosi: dict[str, int]

    @classmethod
    def initDic(cls, listIten: list[ ItenExData ]):
        nums = []
        for r in listIten:
            mi = r.minaoshi
            try:
                x = float(mi)
                if x > 500:
                    continue
                nums.append(mi)
            except:
                continue
        wklist = list(dict.fromkeys(nums).keys())
        wklist.sort(key= lambda x: float(x))

        cls.dicMinaosi = {k: i for i, k in enumerate(wklist, start=1)}
        pass