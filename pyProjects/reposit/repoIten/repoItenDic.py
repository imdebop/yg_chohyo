from re import I
from typing import Dict, List
from dataclasses import dataclass

from peewee import DictCursorWrapper
from data.recIten import *
from .itenHojo import *
from data.numUtil import *
from typing import Tuple
import enum
from .hojoRrec import HojoRec

#
#移転レコードのキー
#　　9.01->00901、　複数の場合は cf. 00901:01200等
#
class ItenType(enum.Enum):
    OK = enum.auto()
    New = enum.auto()
    Del = enum.auto()


class RepoItenDic:
    dicTousho: Dict[int, str] = {}
    print("RepoItenDic.dicToushoを1-305まで'None'で初期化")
    for i in range(1, 305):
        dicTousho[i] = None
    dicNew: Dict[int, ItenExData] = {}
    dicBunkatu: Dict[int, ItenExData] = {}
    dicHojoTosho: Dict[str, List[HojoRec]] = {}
    dicKeyS2T: Dict[str, str] = {}

    #根拠別・当初データの処理
    @classmethod
    def setNcheck(cls, iRec: ItenExData) -> ItenType:
        #建物番号が数字のみ
        if NumUtil.is_num(iRec.tatemonNo.split(".")[0]):
            i = int(iRec.tatemonNo.split(".")[0])
            if NumUtil.is_num(iRec.seiriNo):
                if int(iRec.seiriNo.split(".")[0]) < 1000:
                    cls.dicTousho[i] = "ok"
                    cls.save2dicHojoTosho(iRec)
                    return ItenType.OK
                else:
                    #または整理番号>1000ならば工作物
                    if cls.dicTousho[i] != "ok":
                        #建物->工作物
                        cls.dicTousho[i]= "kou"
                    cls.save2dicHojoTosho(iRec)
                    return ItenType.OK
            elif iRec.seiriNo in ["削除", "対象外", "別レコード"]:
                cls.dicTousho[i] = "del"
            elif iRec.seiriNo == "工作物":
                #整理番号=工作物
                cls.dicTousho[i]= "kou"
            else:
                #目下不使用
                wk = iRec.seiriNo[:2]
                if wk in ["分割", "完了"]:
                    cls.dicTousho[i] = "ok"
                    cls.setBunkatu(iRec)
        else:
            if iRec.tatemonNo == "新規":
                n = iRec.seiriNo.split(".")[0]
                if NumUtil.is_num(n):
                    cls.dicNew[int(n)] = iRec            
                    cls.save2dicHojoTosho(iRec)
            elif iRec.rosenName == "工作物等" and NumUtil.is_num(iRec.seiriNo):
                cls.save2dicHojoTosho(iRec)

    @classmethod
    def save2dicHojoTosho(cls, r: ItenExData):
        seiriKey = cls.code2key(r.seiriNo)
        wkrec = ItenHojo.hojoList(r)
        if seiriKey in cls.dicHojoTosho:
            cls.dicHojoTosho[seiriKey] += wkrec
        else:
            cls.dicHojoTosho[seiriKey] = wkrec


    ############################################################
    #sub routines ##############################################
    @classmethod
    def setBunkatu(cls, iRec: ItenExData):
        i = int(iRec.tatemonNo.split(".")[0])
        if i not in cls.dicBunkatu:
            cls.dicBunkatu[i] = iRec

    @classmethod
    def code2keys(cls, code: str):
      wklist = code.split(",")
      #wkstr = [cls.code2key(s) for s in wklist]
      #print(wkstr)
      return '|'.join( [cls.code2key(s) for s in wklist])


    @classmethod
    def code2key(cls, code: str):
        if NumUtil.is_num(code) == False:
            return code
        
        wklist = code.split(".")
        if len(wklist) == 2:
            num = float(code)
            wkstr = "{:0.2f}".format(num).zfill(7).replace(".","")
            #wkstr = wklist[0].zfill(4) + wklist[1].zfill(2)
        else:
            wkstr = wklist[0].zfill(4) + "00"
        return wkstr

"""
    @classmethod
    def setItenDic(cls, rec: list):
        #当初キー
        itenRec = ItenExData(*rec)
        keyT = "T/" + cls.code2keys(itenRec.tatemonNo)
        cls.dicHojoTosho[keyT] = itenRec
        #変更キー
        #整理キー
        keyS = "S/" + cls.code2keys(itenRec.seiriNo)
        cls.dicKeyS2T[keyS] = keyT + "-" + itenRec.seq
"""


#a = RepoItenDic.code2keys("3.5,17")
#print(a)
#a = RepoItenDic.code2keys("45")
#print(a)

