from os import minor, replace
from data.recIten import *
from data.numUtil import *
from .itenCodes import *
import copy
from .itenKanryoShukei import *

class Extract2shinki:
    listShinkiExtract: list[ItenExData] = []
    listMikanExtract: list[ItenExData] = []
    listTuikaExtract: list[ItenExData] = []
    subtotalTatemono: int = 0
    subtotalKosaku: int = 0
    dicKosakuNendoSum: dict[int, int] = { i: 0 for i in range(0,30)}
    dictTotal: dict[str, int] = {}
    dictNendoSum: dict[str, list[int]] = {}

    for tpl in hojoCodeList():
        dictTotal[tpl.cds] = 0
        dictNendoSum[tpl.cds] = {i: 0 for i in range(0, 30)}

    @classmethod
    def checkNadd(cls, irec: ItenExData):
        minaoshi: int
        cls.totalKingaku(irec)

        try:
            minaoshi = int(irec.minaoshi)
        except:
            minaoshi = -1
        if irec.tatemonNo.startswith("新規"):
            if minaoshi > 0:
                cls.listShinkiExtract.append(irec) 
        if irec.tatemonNo.startswith("追加"):
            cls.listTuikaExtract.append(irec)
        wkIrec = copy.deepcopy(irec)
        if cls.hasMikan(wkIrec):
            cls.listMikanExtract.append(wkIrec)
    
        ItenKanryoShukei.shukei(irec)

    @classmethod
    def checkKosaku(cls, irec: ItenExData):
        flgKosaku: bool = False
        try:
            n = float(irec.seiriNo)
            if n >= 1000:
                flgKosaku = True
        except:
            flgKosaku = True
        return flgKosaku

    
    @classmethod
    def totalKingaku(cls, irec: ItenExData):
        r:str
        for i, r in enumerate(irec.hojoRecs()):
            if r == "":
                continue
            wk = r.split("/")
            kin = int(wk[0])
            if len(wk) == 1:
                wk.append("tan")
            cls.dictTotal[wk[1]] += kin
            cls.dictNendoSum[wk[1]][i] += kin

            if cls.checkKosaku(irec):
                cls.subtotalKosaku += kin
                cls.dicKosakuNendoSum[i] += kin
            else:
                cls.subtotalTatemono += kin


    @classmethod
    def hasMikan(cls, irec: ItenExData) -> bool:
        flg: bool = False
        data = asdict(irec)
        for k, v in data.items():
            if k[:1] == "H":
                setattr(irec, k, "")
                continue
            elif k[:1] == "R":
                flg = True
        return flg

            