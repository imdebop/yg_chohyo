from data.recIten import ItenExData
from typing import Dict, List
from .hojoRrec import HojoRec
from .itenCodes import *

class ItenHojo:
    dicHojoCode: Dict[str, str]

    #根拠別データから補助金の「金額/種別コード」のリストを返す
    @classmethod
    def hojoList(cls, r: ItenExData) -> List[HojoRec]:
        wkList: List[HojoRec] = []
        exList = r.nendoBetuList()
        recSize = len(exList)
        for i in range(recSize):
            item: str = exList[i]
            if item != '':
                #wklst2 = item.split("/")
                #if len(wklst2) == 2:
                #    wkGaku, wkCd = wklst2
                #else:
                #    wkGaku = item
                #    wkCd = "tan"
                wkGaku, wkCd, state = hojoShubetu2tuple(item)
                hj = HojoRec(
                    gaku = int(wkGaku),
                    cd = wkCd,
                    state = state,
                    nendo = str( i + 17 )
                )
                wkList.append( hj )
        return wkList

    @classmethod
    def detail(cls, data: str):
        nendo, kingaku, hojo = data.split("/")
        return [nendo, kingaku, hojo, cls.dicHojo[hojo]]

ItenHojo.dicHojoCode = setDicHojoCode()
#print("code dic is set")
pass
"""
ItenHojo.dicHojoCode = {
        "cc": "地活直",
        "cy": "地活用地",
        "sc": "市直",
        "sy": "市用地",
        "mc": "まち直",
        "my": "まち用地",
        "mck": "まち直経対",
        "tan": "単費",
        "ni": "無利子",
        "tu": "通常",
        "ky": "公管用地",
        "NA": "要確認",
}

"""
    

      