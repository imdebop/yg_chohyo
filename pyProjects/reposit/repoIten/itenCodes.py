from typing import Dict, Tuple, List
from collections import namedtuple

HojoCd = namedtuple('HojoCd', ['cds', 'cdn', 'name'])

class ItenCodes:
  dicAbbr2name: dict[str, HojoCd] = {}
  @classmethod
  def getNameByAbbr(cls, abbr: str) -> str:
    if abbr in cls.dicAbbr2name:
      r = cls.dicAbbr2name[abbr]
      return r.name
    else:
      return "NA"


def setDicHojoCode() -> Dict[str, str]:
  return {}

def hojoShubetu2tuple(val: str) -> Tuple:
  wk = val.split("/")
  num = len(wk)
  if num == 1:
    return (val, "tan", "完")
  elif num == 2:
    wk.append("完")
    return tuple(wk)
  elif num == 3:
    if wk[2] == "*":
      wk[2] = "未"
    return tuple(wk)

def hojoCodeList() -> List[HojoCd]:
    data = [
        ["cc", "2", "地活直"],
        ["cy", "3", "地活用地"],
        ["sc", "8", "市直"],
        ["sy", "9", "市用地"],
        ["mc", "4", "まち直"],
        ["my", "5", "まち用地"],
        ["mkc", "12","まち直経直"],
        ["mky", "13","まち直経用"],
        ["tan", "10", "単費"],
        ["ni", "11", "無利子"],
        ["tu", "1", "通常"],
        ["ky", "7", "公管用地"],
        ["NA", "0", "要確認"],
    ]
    return [HojoCd(*r) for r in data ]

## 辞書データセット
ItenCodes.dicAbbr2name = {r.cds: r for r in hojoCodeList() }

