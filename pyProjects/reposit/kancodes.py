from reposit.repoKanchi import *

class GetKancodes_dic:
    @classmethod
    def do(self):
        dic = {}
        db = RepoKanchi.getSingleton()
        kancodes = db.getAllKancodes()
        for kcode in kancodes:
            dic[kcode] = False
        return dic

class GetKancodes_set:
    @classmethod
    def do(self):
        db = RepoKanchi.getSingleton()
        kancodes = db.getAllKancodes()
        return {*kancodes}

class ChkKancodes:
    def __init__():
        pass

