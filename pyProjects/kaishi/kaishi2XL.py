from openpyxl import load_workbook

#ws.cell(4,4, 123)
#wf = '/docFormat/使用未開始.xlsx'
#wpath = cwd + wf
#wb.save(wpath)

class Write2xl:
    def __init__(self, inF):
        self.wb = load_workbook(filename= inF)

    def setWksheet(self, wsname):
        self.ws = self.wb[wsname]

    def saveWs(self, outF):
        self.wb.save(outF)
    
    def setCell(self, row, col, val):
        self.ws.cell(row, col, val)

    def listFill(self, startRow, startCol, valList):
        itemLen = len(valList[0]) 
        curRow = startRow
        curCol = startCol
        for r in valList:
            for i in range(itemLen):
                c = i + curCol     
                self.ws.cell(curRow, c, r[i])
            curRow += 1


    