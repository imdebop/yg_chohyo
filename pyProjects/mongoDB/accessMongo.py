from pymongo import MongoClient

class AccessMongo:
    def __init__(self, dbName):
        self.client = MongoClient('localhost',27017)
        self.db = self.client[dbName]

    def recreate(self, data, collName):
        self.db[collName].drop()
        self.db[collName].insert_many(data)

    def insert_list(self, data, collName):
        self.db[collName].insert_many(data)


    def getAll(self, collName):
        recs = self.db[collName].find({},{'_id': 0})
        return recs
    
    def getAllByFields(self, collName, fieldList):
        fields = {'_id' : 0}
        for f in fieldList:
            fields[f] = 1
        recs = self.db[collName].find({}, projection= fields)
        return recs


#db = AccessMongo("simaZahyo")
#db.createNew("simaZahyo", [{"a": 1},{"b": 2}])