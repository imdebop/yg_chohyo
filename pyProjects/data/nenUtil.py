class NenUtil:
    #平成での年が３１以上なら令和で返す
    def h2R(n: int) -> str:
        if n > 30:
            s = "R" + str(n - 30)
        else:
            s = "H" + str(n)
        return s