from data.recRepoKanchi import RecSokochi
from data.codeTables import *

cdTbl = CodeTables()

class DataUtil:
    def kancode2bl_lot(self, kancode):
        bl = kancode[:2]
        lot = kancode[2:5]
        eda = kancode[5:]
        return (bl, lot, eda)

    # chibanRec is obj of ChibanRec with fudecode in
    def editChibanRec(self, chibanRec):
        (aza, moto, eda) = self.kancode2bl_lot(chibanRec.fudeCode)
        chibanRec.azaCode = aza
        chibanRec.azamei = cdTbl.getChoazaShort(aza)
        chiban = moto.lstrip("0")
        chibanRec.chiban_moto = chiban
        if eda == '000':
            chibanRec.chiban = chiban
        else:
            wk = eda.lstrip("0")
            chibanRec.chiban_eda = wk
            chibanRec.chiban = chiban + '-' + wk

    def juzen_edit_sokochi(self, sokochi_str):
        soko_list = sokochi_str.split('/')
        res_list = []
        for soko in soko_list:
            (aza, chiban, eda) = self.kancode2bl_lot(sokochi_str)
            aza = aza + "0"
            chiban = chiban.lstrip("0")
            eda = eda.lstrip("0")
            res_list.append(RecSokochi(aza, chiban, eda))
        return res_list

    def juzen_hyoji_list(self, fcodes):
        return self.juzen_edit_sokochi(self, fcodes)

    @classmethod
    def bl_lot2kancode(self, blk, lot):
        kCode = '{:02d}'.format(int(blk))
        wk = lot.split("-")
        moto = '{:03d}'.format( int(wk[0]) )
        if (len(wk) == 1):
            eda = "000"
        else:
            eda = '{:03d}'.format( int(wk[1]) )
        kCode += (moto + eda)
        return kCode