import data


class RecRepoKanchi:
    def __init__ (self, kan, horyu, soko_list):
        self.kan = kan
        self.b_l = RecBnL(kan.KanCode)
        self.horyu = horyu
        self.soko_list = soko_list


class RecSokochi:
    def __init__(self, aza, chiban, eda):
        self.aza = aza
        self.chiban = chiban
        self.eda = eda


class RecBnL:
    def __init__(self, kancode):
        (blk, lot, eda) = data.dataUtil.DataUtil.kancode2bl_lot(self, kancode)
        self.blk = blk.lstrip("0")
        self.lot = lot # use lot_eda for usual purpose
        self.eda = eda
        if eda == "000":
            wkeda = ""
        else:
            wkeda =  "-" + eda.lstrip("0")
        self.lot_eda =  lot.lstrip("0") + wkeda
