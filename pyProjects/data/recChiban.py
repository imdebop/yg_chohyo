from dataclasses import dataclass
from data.dataUtil import DataUtil

dtUtil = DataUtil()

@dataclass
class ChibanRec:
    fudeCode: str
    azaCode: str =''
    azamei: str = ''
    chiban_moto: str = ''
    chiban_eda: str = ''
    chiban: str = ''

    def __post_init__(self):
        dtUtil.editChibanRec(self)

class ChibansHyoji:
    @classmethod
    def do(self, fcodes):
        wkdic = {}
        fcodeL = fcodes.split('/')
        for fc in fcodeL:
            chiban = ChibanRec(fudeCode=fc)
            if chiban.azaCode in wkdic:
                wkdic[chiban.azaCode].append(chiban)
            else:
                wkdic[chiban.azaCode] = [chiban]
        chiban_sorted = sorted(wkdic.items(), key= lambda x:x[0])
        hyojiL = []
        hyojiStr = ''
        for c in chiban_sorted:
            #c => (azacode,[chiban, .....])
            azamei = c[1][0].azamei
            hyojiStr += azamei
            wkL = []
            for cc in c[1]:
                wkL.append(cc.chiban)
            hyojiStr += str.join(",", wkL)
            hyojiL.append(hyojiStr)
            hyojiStr = ''
                
        return str.join(",", hyojiL)


#cb = ChibanRec(fudeCode="aabbbccc")
#print(cb)