from dataclasses import dataclass, asdict
from os import W_OK
import re
from dataclasses_json import dataclass_json
from data.numUtil import *

@dataclass_json
@dataclass
class ItenShukeiData:
    shukeiId: str = ""  #0
    frikaeNo: str = ""  #1
    seiriNo: str = ""   #2
    minaoshi: str = ""  #3
    dum01: str = ""     #4
    dum02: str = ""     #5
    dum03: str = ""     #6
    dum04: str = ""     #7
    dum05: str = ""     #8
    dum06: str = ""     #9
    dum07: str = ""     #10
    H17: int = 0        #11        
    H18: int = 0        #12
    H19: int = 0        #13
    H20: int = 0        #14
    H21: int = 0        #15
    H22: int = 0        #16
    H23: int = 0        #17
    H24: int = 0        #18
    H25: int = 0        #19
    H26: int = 0        #20
    H27: int = 0        #21
    H28: int = 0        #22
    H29: int = 0        #23
    H30: int = 0        #24
    H31: int = 0        #25
    R02: int = 0        #26
    R03: int = 0        #27
    R04: int = 0        #28
    R05: int = 0        #29
    R06: int = 0        #30
    R07: int = 0        #31
    R08: int = 0        #32
    kanendo: str = ""   #33--->この数を kanendoCol にセット
    chosaGaku: str = "" #34
    note: str = ""      #35
    seq: str = ""       #36
    key: str = ""       #37

    def __post_init__(self):
        self.kanendoCol = 33
        #self.tatemonNo = str(self.tatemonNo)
        #self.seiriNo = str(self.seiriNo).replace("/", ",")
        #self.henkoNo = str(self.henkoNo)
        #self.shimei = self.shimei.replace(" ","")
        #if not NumUtil.is_num(self.seq):
        #    self.seq = str(Seq.getSeq()).zfill(4)
        #if NumUtil.is_num(self.seiriNo):
        #    self.key = self.getKey()
    
    
    def headerData(cls):
        return list(asdict(cls).values())[:11]
    

    def hojoRecs(self):
        wkl = list(asdict(self).values())[11:self.kanendoCol]
        return wkl

    def hojoKanMikanSum(self, nen: int) -> tuple[int, int]:
        #nen=17年完了なら x=1 -> x=nen-16
        wklist = self.hojoRecs()
        x = nen - 16
        kanryo = sum(wklist[:x])
        mikan = sum(wklist[x:])
        return (kanryo, mikan)

class Seq:
    seq: int = 0

    @classmethod
    def getSeq(cls):
        cls.seq += 1
        return cls.seq