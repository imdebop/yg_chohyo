class CodeTables:
    def __init__(self) -> None:
        self.dicChoazaShort = self.setChoazaShort()
        self.dicChimoku = {}

    def getChoazaShort(self, azacode):
        return self.dicChoazaShort[azacode]

    def setChoazaShort(self): #町字の略称
        wkdic = {}
        for k, v in self.dicChoaza().items():
            choazaL = v.split('/')
            wkdic[k] = choazaL[1]
        return wkdic

    def dicChoaza(self):
        return {
            "01":"牟呂町字/大塚",
            "02":"牟呂町字/奥山",
            "03":"牟呂町字/奥山新田",
            "04":"牟呂町字/北汐田",
            "05":"牟呂町字/古田",
            "06":"牟呂町字/古幡焼",
            "07":"牟呂町字/中西",
            "08":"牟呂町字/東里",
            "09":"牟呂町字/百間",
            "10":"牟呂町字/松崎",
            "11":"牟呂町字/松島",
            "12":"牟呂町字/松島東",
            "13":"牟呂町字/松東",
            "14":"牟呂町字/南汐田",
            "19":"神野新田町字/会所前",
            "21":"/柱四番町",
            "22":"/柱五番町",
            "24":"/藤沢町",
            "25":"/潮崎町",
            "26":"/牟呂市場町",
        }

    def dicChimoku(self):
        return {
            "01":"田",
            "02":"畑",
            "03":"宅地",
            "04":"塩田",
            "05":"鉱泉地",
            "06":"池沼",
            "07":"山林",
            "08":"牧場",
            "09":"原野",
            "10":"墓地",
            "11":"境内地",
            "12":"運河用地",
            "13":"水道用地",
            "14":"用悪水路",
            "15":"ため池",
            "16":"堤",
            "17":"井溝",
            "18":"保安林",
            "19":"公衆用道路",
            "20":"公園",
            "21":"雑種地",
            "22":"学校用地",
            "23":"鉄道用地",
            "24":"溝渠",
            "25":"渡船敷地",
        }




#ct = CodeTables()
#print(ct.dicChoazaShort)
