from dataclasses import dataclass, field
from reposit.repoKanchi import *
from typing import List

rpk = RepoKanchi()
kaishi = Kaishi

@dataclass
class Kaishi:
    seq: int

@dataclass
class KanDtl:
    KanCode: str
    ShoCode: str
    KumiSho: str
    Menseki: str
    KaishiL: List[Kaishi] = field(default_factory=list)

    def __post_init__(self):
        if self.ShoCode != '5000':
            self.KaishiL = rpk.getKaishiByKancode(self.KanCode)
        
        #print('aaaaa' + self.KanCode)