from dataclasses import dataclass, asdict
from decimal import Clamped
from dataclasses_json import dataclass_json
from collections import namedtuple

@dataclass_json
@dataclass
class ItenShuhyoData:
    groupNo: str = ""
    tatemonNo: str = ""
    shinEda: str = ""
    itenNo: str = ""
    shikichiNo: str = ""
    tateCode: str = ""
    tateCodeEda: str = ""
    chomei: str = ""
    azamei: str = ""
    chiban: str = ""
    chibanEda: str = ""
    yukaMenseki: str = ""
    shikichiMen: str = ""
    shimei: str = ""
    nendo: str = ""
    keikakuGaku: int = 0
    keiyakuYMD: str = ""
    kingaku: int = 0
    kinshuCD: int = 0
    kinshu: str = ""
    yy_cd: str = ""
    konkyo: str = ""
    seq: str = ""

    def xxx__post_init__(self):
        self.tatemonNo = str(self.tatemonNo)
        self.seiriNo = str(self.seiriNo).replace("/", ",")
        self.henkoNo = str(self.henkoNo)
        if self.seq == "":
            self.seq = str(Seq.getSeq()).zfill(4)

@dataclass  
class ItenShuNendoData:
    nendo: str = ""
    num: str = ""
    tatemonoNo: str =""
    shimei: str = ""
    chomei: str = ""
    azamei: str = ""
    chiban: str = ""
    chibanEda: str = ""
    kingaku: str = ""
    cd: str = ""
    kinshu: str = ""
    bunbetu: str = ""
    bunbetuyo: str = ""
    konkyo: str = ""
    block: str = ""
    lot: str = ""
    toshoKeiyaku: str = ""
    saisinKoki: str = ""
    kanryo: str = ""
    biko: str = ""
    seq: str = ""

    def nenGaku(self):
        return NenGaku(self.nendo, self.kingaku, self.kinshu)

NenGaku = namedtuple('NenGaku', ('nendo', 'kingaku', 'kinshu'))

class Seq:
    seq: int = 0

    @classmethod
    def getSeq(cls):
        cls.seq += 1
        return cls.seq