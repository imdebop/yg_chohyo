from dataclasses import dataclass, asdict
from os import W_OK
from dataclasses_json import dataclass_json
from data.numUtil import *

@dataclass_json
@dataclass
class ItenExData:
    comment: str = ""   #0
    rosenName: str = "" #1
    seiriNo: str = ""   #2
    minaoshi: str = ""   #3
    tatemonNo: str = "" #4
    henkoNo: str = ""   #5
    yoto: str = ""      #6
    menseki: str = ""   #7
    koho: str = ""      #8
    keikakuGaku: str = ""   #9
    shimei: str = ""    #10
    H17: str = ""       #11        
    H18: str = ""       #12
    H19: str = ""       #13
    H20: str = ""       #14
    H21: str = ""       #15
    H22: str = ""       #16
    H23: str = ""       #17
    H24: str = ""       #18
    H25: str = ""       #19
    H26: str = ""       #20
    H27: str = ""       #21
    H28: str = ""       #22
    H29: str = ""       #23
    H30: str = ""       #24
    H31: str = ""       #25
    R02: str = ""       #26
    R03: str = ""       #27
    R04: str = ""       #28
    R05: str = ""       #29
    R06: str = ""       #30
    R07: str = ""       #31
    R08: str = ""       #32
    kanendo: str = ""   #33--->この数を kanendoCol にセット
    chosaGaku: str = "" #34
    note: str = ""      #35
    seq: str = ""       #36
    key: str = ""       #37

    def __post_init__(self):
        self.kanendoCol = 33
        self.tatemonNo = str(self.tatemonNo)
        self.seiriNo = str(self.seiriNo).replace("/", ",")
        self.henkoNo = str(self.henkoNo)
        self.shimei = self.shimei.replace(" ","")
        #if not NumUtil.is_num(self.seq):
        #    self.seq = str(Seq.getSeq()).zfill(4)
        if NumUtil.is_num(self.seiriNo):
            self.key = self.getKey()
    
    #整理番号をキーに編集
    def getKey(self):
        wk = self.seiriNo.split(".")
        if len(wk) == 2:
            moto, eda = self.seiriNo.split(".")
        else:
            moto = wk[0]
            eda = '00'
        moto = str(int(moto)).zfill(4)
        key = moto + eda
        return key

    def nendoBetuList(self):
        return list(asdict(self).values())[11:self.kanendoCol]
    
    def headerData(cls):
        return list(asdict(cls).values())[:11]
    
    @classmethod
    def headerColEnd():
        return 10

    def hojoRecs(self):
        wkl = list(asdict(self).values())[11:self.kanendoCol]
        return wkl

class Seq:
    seq: int = 0

    @classmethod
    def getSeq(cls):
        cls.seq += 1
        return cls.seq