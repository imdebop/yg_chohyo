class NumUtil:
    @classmethod
    def is_num(cls, s: str):
        try:
            float(s)
            return True
        except ValueError:
            return False
        