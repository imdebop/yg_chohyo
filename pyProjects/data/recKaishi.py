from data.recChiban import ChibansHyoji
from dataclasses import dataclass
from reposit.repoKanchi import *
from data.recRepoKanchi import *
from data.recKanDtl import *

rpKan = RepoKanchi()

@dataclass
class RecRepoKaishi:
    kanCode: str = ""
    block: str = ""
    lot: str = ""
    menseki: float = 0
    kaishiMen: float = 0
    shoCode: str = ""
    shimei: str = ""
    fudeCodes: str = ""
    hyojiChiban: str = ""
    def __post_init__(self):
        EditRepoKaishi.do(self)
        self.hyojiChiban = ChibansHyoji.do(self.fudeCodes)
    def getList(self):
        return GetKaishiList.do(self)

class EditRepoKaishi:
    @classmethod
    def do(self, rrKai):
        kan = rpKan.getKanchiByKey(rrKai.kanCode)
        recBnL = RecBnL(rrKai.kanCode)
        rrKai.block = recBnL.blk
        rrKai.lot = recBnL.lot_eda
        rrKai.menseki = float( kan.Menseki )
        rrKai.shoCode = kan.ShoCode
        shoyu = rpKan.getShoyuByKey(kan.ShoCode)
        rrKai.shimei = shoyu.TokiName

class GetKaishiList:
    @classmethod
    def do(self, rrKai):
        wkKaishi = rrKai.kaishiMen
        if rrKai.kaishiMen == 0:
            wkKaishi = ''
        return [
            rrKai.block,
            rrKai.lot,
            rrKai.menseki,
            wkKaishi,
            rrKai.shimei,
            rrKai.shoCode,
            rrKai.hyojiChiban
        ]