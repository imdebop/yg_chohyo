from os import pardir
from reposit.repoIten import itenPath
from reposit.repoIten.out2ShukeiEx import Out2shukeiEx
import reposit
from reposit import repoIten
from reposit.repoIten import itenMain, out2konkyoEx, out2shinkiEx, out2ShukeiEx
from reposit.repoIten import itenRead, repoItenShuhyo, itenChk
#from peewee import quote
import exUtil.exRead
from envCommon import EnvCommon
#from collections import namedtuple
from data.recIten import *
from reposit.repoIten import itenUtil
from reposit.repoIten.itenUtil import *
from reposit.repoIten.compNendoKonkyo import *
from reposit.repoIten.itenPath import *
import csv
import reposit.repoIten.readEx2csv

# 実行処理  #############################################################
#現在の移転リスト
with open("out/itenShuhyo.csv", encoding='utf-8') as f:
    reader = csv.reader(f)
    wklist = list(reader)
    #print(wklist[0])
    repoItenShuhyo.RepoShuhyo.setTateNew(wklist)
    #repoItenShuhyo.RepoShuhyo.setTateNew(wklist)
    #RepoShuhyo.setTateNew(wklist)
#print(RepoShuhyo.dictTateNew.items())

#年度別_移転リスト
with open("out/itenShuNendo.csv", encoding='utf-8') as f:
    reader = csv.reader(f)
    wklist = list(reader)
    #print("*** 年度別リスト ***")
    #print(wklist[0])
    dicListNendo = repoItenShuhyo.RepoShuhyo.setTateNendo(wklist)

#根拠別当初の移転リスト -> Repoiten.tdb, RepoIten.dicHojoTosho に登録
with open("out/itenKonkyobetu.csv", encoding='utf-8') as f:
    reader = csv.reader(f)
    wklist = list(reader)
    #print('根拠データcsv')
    #print(wklist[10])
    #dicTosho = itenMain.RepoIten(wklist)
    rpIten: itenMain.RepoIten = itenMain.RepoIten(wklist)

    if True: # 編集抑制 ##########################
        #根拠別エクセル帳票編集　年度別データとの突き合わせデータも削正　
        exKonkyoForm: exUtil.exRead.OpenPXL = out2konkyoEx.Out2konkyoEx(ItenPath.out2konkyoExForm, "Sheet1", ItenPath.out2konkyoExPath, rpIten)
        #新規データのエクセル編集
        out2shinkiEx.Out2shinkiEx()
    out2ShukeiEx.Out2shukeiEx(ItenPath.out2shukeiExForm, "Sheet1", ItenPath.out2shukeiExPath)
    


############################
#根拠別　年度別　突き合わせ
nendoSet = set(CompNendoKonkyo.dicNendo.keys())
konkyoSet = set(CompNendoKonkyo.dicKonkyo.keys())
dif: set = nendoSet.difference(konkyoSet)
difList: list = list(dif)
print("年度別不突合")
print(sorted(difList, key= lambda x: float(x.split(":")[0])))

dif: set = konkyoSet.difference(nendoSet)
difList: list = list(dif)
print("根拠別不突合")
print(sorted(difList, key= lambda x: float(x.split(":")[0])))

exit()







#
#
#
#
#
#
#################################################################
#根拠別リストと現在の主表の突き合せチェック
res = itenChk.ItenChk(itenMain.RepoIten.tdb)

#根拠別リストと年度別リストの突き合せ
print('\n*** matching 当初 and 年度別 ***')
print('  当初にしかキーがないもの')
for k, listHojorec in itenMain.RepoIten.dicHojoTosho.items():
    if k not in dicListNendo:
        print(k + ' ',end='')
    else:
        itenChk.ItenChk.compKonkyo2Nendo(k, listHojorec, dicListNendo[k])

print('\n  現在にしかキーがないもの')
for k, rNendo in dicListNendo.items():
    if k not in itenMain.RepoIten.dicHojoTosho:
        wklist = ",".join([x.nendo for x in rNendo])
        print(k + "(" + wklist + ")" + ' ',end='')
    
print('\n  補助金の件数が合わないもの')
print("　現在非表示")
if False:
    for r in itenChk.ItenChk.listKensuUnmatch:
        print(r[0])
        for rr in r[1]:
            print(rr)
        for rr in r[2]:
            print(rr)

print('\n  補助金の金額が合わないもの')
print(itenChk.ItenChk.listKingakuUnmatch)

print('\n*** 複数補助金あり ***')
for k, v in itenMain.RepoIten.dicHojoTosho.items():
    if len(v) > 1:
        print(k, end=',')

#print(RepoIten.dicHojoTosho)
#for k, v in dicListNendo.items():
#    print([k, v])