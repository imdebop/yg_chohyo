from reposit.repoKanchi import *
from data.recKaishi import RecRepoKaishi
import os
from kaishi.kaishi2XL import *

cwd = os.getcwd()
exKaishiPath = '/bunsho/_使用未開始_.xlsx'
fpath = cwd + exKaishiPath
of = '/bunsho/使用未開始.xlsx'
oPath = cwd + of
w2x = Write2xl(fpath)
w2x.setWksheet('1')
#w2x.setCell(10,3, 'aaa')
#w2x.saveWs(oPath)
#exit()


rpKan = RepoKanchi()

cntKaishi = 0
outL = []
for kumi in rpKan.getKumi():
    fcodesL = kumi.Fcodes.split('/')
    kancodesL = kumi.Kanchis.split('/')
    if kancodesL[0] == '':
        continue
    elif kancodesL[0] == '未指定':
        continue
    #print(kumi.Key)
    #print(kancodesL)
    for kancode in kancodesL:
        kaishiL = rpKan.getKaishiByKancode(kancode)
        kaiMen = 0
        if len(kaishiL) > 0:
            for kai in kaishiL:
                kaiMen += float(kai.Menseki)
            cntKaishi += 1

        rrKan = RecRepoKaishi(
            kanCode=kancode,
            fudeCodes=kumi.Fcodes
            )
        if rrKan.menseki - kaiMen < 0.001:
            continue
        else:
            rrKan.kaishiMen = kaiMen

        outL.append(rrKan)

print(cntKaishi)
print(len(outL))

outL.sort(key=lambda x:x.kanCode)
outL2 = []
cnt = 1
for r in outL:
    outL2.append([cnt] + r.getList())
    cnt += 1
    
w2x.listFill(4, 2, outL2)
w2x.saveWs(oPath)
