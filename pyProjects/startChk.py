from re import I
from reposit.repoKanchi import *
from reposit.kumiConv import KumiConv
from reposit.recKanDtl import *

import reposit.chkShocode as chkSho

rpk = RepoKanchi()
kumiRecs = rpk.getKumi()
kumiC = KumiConv(rpk)

for kan in rpk.list_kanchi():
    kancode = kan.KanCode
    kumiSho = kumiC.getShoCodeByKancode(kancode)
    r = KanDtl(
        kan.KanCode,
        kan.ShoCode,
        kumiSho,
        kan.Menseki
    )
    res = chkSho.sho_validate(r)
    if res:
        for kai in r.KaishiL:
            print(
                r.KanCode,
                ':',
                kai.id
            )
