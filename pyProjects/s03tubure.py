from envCommon import EnvCommon
import peeWeeDB.peeConfig as peeConfig
import sys


tuburePath = EnvCommon.ygRoot + "用地買収/"
peeConfig.tubureDbPath = tuburePath + "用買潰地.sqlite3"

from reposit.repoTubure import *

if sys.argv[1] == "loadDB":
    print("loading db")
    pass
else:
    import tubure.table_sample
    #import tubure.listbox
    #import tubure.tbMain
    print("潰地編集")
    exit()


###############################################################
### エクセルからSQLITEへデータをロード #############################
###############################################################
from openpyxl import Workbook, load_workbook
from openpyxl.worksheet.table import Table, TableStyleInfo
from decimal import *

wbPath = tuburePath + "潰地シート.xlsx"

wb = load_workbook(wbPath, data_only=True)
ws = wb["求積一覧表"]
tubureTbl = ws.tables["tubure_tbl"]

data_range = tubureTbl.ref
data = ws[data_range]
content = [
    [cell.value for cell in ent]
    #tuple([cell.value for cell in ent])
    for ent in data
]

tuple_list = []
for row in content[1:]:
    num = row[12]
    if row[1] == None:
        continue
    if num == None:
        row[12] = 0
    else:
        row[12] = int(Decimal(row[12]).quantize(Decimal('0'), rounding=ROUND_HALF_UP))
    tuple_list.append(tuple(row))

print(tuple_list[2])

rt = RepoTubure()
rt.clearDb()
rt.bulkInsert(tuple_list)

#print(tuburePath)
#print(ws.tables)