import win32com.client
from pathlib import Path


app = win32com.client.Dispatch('Excel.application')
ex_re_path = "c:/yagyu_settings/Main_test.xlsm"
#ex_abs_path = str(Path(ex_re_path).resolve())


class ExWin32:
    def __init__(self, app_obj, expath):
        self.app = app_obj
        self.ex_abs_path = str(Path(expath))

    def open_ex(self):
        self.wb = app.Workbooks.Open(self.ex_abs_path, UpdateLinks=0, ReadOnly=False)
        self.wb.Activate()
        #app.Visible = True
        self.ws = self.wb.worksheets(1)
        #input("hit return")

    def close_ex(self):
        self.wb.Close(True)

    def close_app(self):
        self.app.Quit()

    def range_clear(self, r1, c1, r2, c2):
        cell_1 = self.rc_addr(r1, c1)
        cell_2 = self.rc_addr(r2, c2)
        self.ws.Range(cell_1, cell_2).ClearContents()

    def rc_addr(self, r, c):
        return self.ws.cells(r, c)

    def set_rc_str(self, r, c, val):
        cell = self.rc_addr(r, c)
        self.ws.Cells(r, c).Value = val



exwin = ExWin32(app, ex_re_path)
