import collections
from re import I
import re
from openpyxl.styles.colors import BLACK, WHITE, Color 
from openpyxl.styles import Fill, PatternFill, Font
from openpyxl.styles.fills import GradientFill

class EditExel:
    @classmethod
    def setColorByType(cls, ws, iRow, iCol, type):
        fill = cls.getByColorCombination(type)
        cell = ws.cell(row = iRow, column = iCol)
        cell.fill = fill
        
    @classmethod
    def setCellColorByType(cls, cell, type):
        fill = cls.getByColorCombination(type)
        cell.fill = fill

    @classmethod
    def setColor(cls, ws, iRow, iCol, color):
        fill = PatternFill(
            fgColor= cls.colorByName(color),
            fill_type='solid')
        cell = ws.cell(row = iRow, column = iCol)
        cell.fill = fill

    @classmethod
    def setFontColor(cls, ws, iRow, iCol, color):
        cell = ws.cell(row = iRow, column = iCol)
        cell.font = Font(color= cls.colorByName(color))

    @classmethod
    def setFontColorByCell(cls, cell, color):
        cell.font = Font(color= cls.colorByName(color))
    
    @classmethod
    def setPattern(cls, cell, pattern):
        fill = PatternFill(
            patternType= pattern
        )
        cell.fill = fill

    @classmethod
    def getByColorCombination(cls, s: str):
        if("-" in s):
            c1, c2 = s.split('-')
            cc1 = cls.colorByName(c1)
            cc2 = cls.colorByName(c2)
            fill = GradientFill(
                stop=[cc1, cc2],
                #start_color='00FFFFFF',
                #end_color='0099CC00',
            )
        else:
            colorCode = cls.colorByName(s)
            fill = PatternFill(
                fgColor= colorCode,
                fill_type='solid')
        return fill

    @classmethod
    def setAsMikanryo(cls, cell):
        cell.font = Font(name = "MS UI Gothic", italic=True, underline="single", bold=True)


    @classmethod
    def colorByName(cls, name) -> str:
        if name == 'pink':
            return '00FF99CC'
        elif name == 'yellow':
            return '00ffff00'
        elif name == 'lightGray':
            return '00ccffff'
        elif name == 'lightGreen':
            return '00bbffbb'
        elif name == 'paleBlue':
            return '00dadaff'
        elif name == 'skyBlue':
            return '0000bfff'
        elif name == 'orange':
            return '00ffc900'
        elif name == 'green':
            return '0000ff00'
        elif name == 'vermilion':
            return '00fe8050'
        elif name == 'blue':
            return '0075bbfd'
        elif name in ['gray', 'grey']:
            return '00909090'
        elif name == 'white':
            return '00ffffff'
        else:
            return '00000000'


    @classmethod
    def getByColorIdx(cls, idx):
        return Color(index= idx)

    
