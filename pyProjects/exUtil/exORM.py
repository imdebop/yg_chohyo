from abc import ABCMeta
from abc import abstractmethod


class ExORM(metaclass=ABCMeta):
    @abstractmethod
    def set_book(self, book_path):
        pass

    @abstractmethod
    def set_sheet(self, sheet_name):
        pass

    @abstractmethod
    def set_rc_str(self, r, c, s):
        pass

    @abstractmethod
    def set_rc_int(self, r, c, i):
        pass

    @abstractmethod
    def set_rc_float(self, r, c, f):
        pass

