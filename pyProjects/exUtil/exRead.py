import openpyxl

class OpenPXL:
    def __init__(self, path, sheetname):
        self.wb = openpyxl.load_workbook(path, data_only=True)
        self.ws = self.setSheet(sheetname)

    def setSheet(self, sheetName):
        return self.wb[sheetName]

    def getData(self, startRow, lastRow, startCol, lastCol):
        rowRange = list(range(startCol, lastCol + 1))
        li = []
        for i in list(range(startRow, lastRow + 1)):
            li2 = []
            for ii in rowRange:
                li2.append(self.ws.cell(row = i, column = ii).value)
            li.append(li2)
        return li

#wb.create_sheet(title = "first")
#ws = wb['Sheet1']
#ws.cell(row=3, column=2).value = "sksksksk"

#wb.save("sample.xlsx")

#print ("test test")