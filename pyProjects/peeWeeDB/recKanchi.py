from peewee import *
from envCommon import EnvCommon

dbpath = EnvCommon.ygSettings + "gyomu.sqlite"
#db = SqliteDatabase('c:/yagyu_settings/gyomu.sqlite')
db = SqliteDatabase(dbpath)

class Shoyu(Model):
    ShoCode = CharField()
    TokiName = CharField()
    TokiJusho = CharField()
    GenName = CharField()
    GenJusho = CharField()
    SeinenYMD = CharField()
    Furigana = CharField()
    Seibetu = CharField()
    MeiboPrint = CharField()
    Telephone = CharField()

    class Meta:
        database = db

class Kumi(Model):
    Key = CharField()
    ShoCode = CharField()
    Fcodes = CharField()
    Kanchis = CharField()
    Memo = CharField()

    class Meta:
        database = db

class Juzen(Model):
    FudeCode = CharField()
    ChimokuCD = CharField()
    ShoCode = CharField()
    TokiMen = CharField()
    KijunMen = CharField()
    HeibeiSisu = IntegerField()
    HyoteiSisu = IntegerField()

    class Meta:
        database = db

class Kanchi(Model):
    KanCode = CharField()
    ShoCode = CharField()
    Menseki = CharField()
    HeibeiSisu = IntegerField()
    Memo = TextField()
    HyoteiSisu = IntegerField()
    KenriSisu = IntegerField()
    Sokochi = TextField()

    class Meta:
        database = db

class Horyu(Model):
    KanCode = CharField()
    Shubetu = CharField()
    Nendo = CharField()
    Kakaku = IntegerField()
    ShoCode = CharField()
    Comment = CharField()
    TukeKancode = CharField()

    class Meta:
        database = db

class Kaishi(Model):
    id = IntegerField()
    KanCode = TextField()
    Renban = TextField()
    KaishiDate = TextField()
    Hatuban = TextField()
    KoryokuDate = TextField()
    Menseki = TextField()
    Comment = TextField()

    class Meta:
        database = db
