from peewee import *
from envCommon import EnvCommon

dbpath_zahyo = EnvCommon.ygSettings + "zahyo.sqlite"
#db = SqliteDatabase('c:/yagyu_settings/gyomu.sqlite')
dbZahyo = SqliteDatabase(dbpath_zahyo)

class Zahyo(Model):
    Tenban = IntegerField()
    Tenmei = CharField()
    X = CharField()
    Y = CharField()
    Z = CharField()
    Memo = CharField()

    class Meta:
        database = dbZahyo
