from peewee import *
import peeWeeDB.peeConfig as cfg



#db = SqliteDatabase('c:/yagyu_settings/gyomu.sqlite')
dbTubure = SqliteDatabase(cfg.tubureDbPath)

class Tubure(Model):
    Id = IntegerField()
    Rosen = CharField()
    Shubetu = CharField()
    Chimoku = CharField()
    Choaza = CharField()
    Chiban = CharField()
    Shoyusha = CharField()
    Kuiki = CharField()
    Kubun = CharField()
    Tanka = IntegerField()
    Menseki = CharField()
    Bubun = CharField()
    Kakaku = IntegerField()
    Nendo = CharField()
    Kurikoshi = CharField()

    class Meta:
        database = dbTubure
