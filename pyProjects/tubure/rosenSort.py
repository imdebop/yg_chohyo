from decimal import *

# 14-1  => 1-14-0-00
# 0.7-1 => 1-00-7-00
# T6-2  => 0-06-0-00
#

class RosenSort:
    key01 = ""
    key02 = ""
    eda = ""

    @classmethod
    def rosenSortCode(cls, rosen):
        wkList = rosen.split('-')
        eda = wkList[1].zfill(2)
        if wkList[0][:1] == "T":
            key01 = "1"
            fukuin = wkList[0][1:]
        else:
            key01 = "0"
            fukuin = wkList[0]

        key02 = str(1000 - int((Decimal(fukuin) * 10))).zfill(3)
        
        return key01 + "-" + key02 + "-" + eda + "-" + wkList[0]



wk = ["14-1", "0.7-1", "6-75","T6-2"]
for s in wk:
    print(RosenSort.rosenSortCode(s))




