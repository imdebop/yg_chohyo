﻿using System;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace FileUitls
{
    public class ReadSjisText
    {
        public static string[] GetByLines(string path)
        {
            string[] lines;
            try
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

                lines = File.ReadAllLines(path, Encoding.GetEncoding("shift_jis"));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                lines = new string[0];
            }
            return lines;
        }






    }
}
