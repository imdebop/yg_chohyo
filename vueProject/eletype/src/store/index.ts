import { createStore } from 'vuex'

export default createStore({
  state: {
    aaa: { data: "a" }
  },
  mutations: {
    changeAaa (state, str) {
      state.aaa.data += str
    }
  },
  actions: {
    changeAaa(context, payload){
      context.commit('changeAaa', payload.str)
    }
  },
  modules: {
  }
})
