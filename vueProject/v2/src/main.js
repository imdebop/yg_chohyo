import Vue from 'vue'
import App from './App.vue'
import vueCheetarGrid from 'vue-cheetah-grid'

Vue.config.productionTip = false
Vue.use(vueCheetarGrid)

new Vue({
  render: h => h(App),
}).$mount('#app')
