export function myFoo (width) {
    let str = '<svg width="200" height="30"> <rect x="3" y="3" width="'
     + `${width}`
     + '" height="18" stroke="black" stroke-width="1" fill="yellow" />'
    str += '<rect x="2" y="2" width="190" height="20" stroke="black" stroke-width="1" fill="none" />'
     str += '</svg>'
    return str
}

export function myGrid () {
let str = '\
<div id="container">\
    <div id="itemA" class="aa">A</div>\
    <div id="itemB" class="aa">B</div>\
    <div id="itemC" class="aa">C</div>\
</div>'
return str
}