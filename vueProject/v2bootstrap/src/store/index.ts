import Vue from 'vue'
import Vuex from 'vuex'
import cats from '@/data/cats'

Vue.use(Vuex)

console.log(cats)

export default new Vuex.Store({
  state: {
    cats
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
