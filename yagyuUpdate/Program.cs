﻿using System;
using yg_chohyo;

namespace yagyuUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("データベース更新開始");

            string[] tblNames = new string[]{"shoyu"};

            foreach(string tbl in tblNames){
                //ReadExSheet.Start(tbl);
                ReadExSheet.UpdateAll();
            }
            
            Console.WriteLine("更新終了");
        }
    }
}
