﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Text.RegularExpressions;
using yg_chohyo.Data;

namespace yg_chohyo
{
    /// <summary>
    /// エクセルシートからデータベースへデータを移行する。
    /// </summary>
    public class ReadExSheet
    {
        static bool unInit = true;
        static ExIO exShoyu = null;
        static ExIO exMain = null;
        public static void Start(string table)
        {
            //bool unInit = true;
            if (unInit){
                unInit = false;
                Console.WriteLine("Shoyu, MainData initialized.");
                //Chohyo.ProgramPath.SetValues();
                string pathShoyu = localizeCs.ProgramPath.Shoyu();
                string pathMain = localizeCs.ProgramPath.MainData();
                exShoyu = new ExIO(pathShoyu);
                exMain = new ExIO(pathMain);
            }


            switch (table)
            {
                case "shoyu":
                    Console.WriteLine("所有者データ更新");
                    Shoyu(exShoyu);
                    break;
                case "kyoyu":
                    Console.WriteLine("共有データ更新");
                    Kyoyu(exShoyu);
                    break;
                case "souzoku":
                    Console.WriteLine("相続データ更新");
                    Souzoku(exShoyu);
                    break;
                case "kanchi":
                    Console.WriteLine("換地データ更新");
                    Kanchi(exMain);
                    break;
                case "juzen":
                    Juzen(exMain);
                    Console.WriteLine("従前データ更新");
                    break;
                case "kumi":
                    Kumi(exMain);
                    Console.WriteLine("組合データ更新");
                    break;
                default:
                    break;
            }
        }

        public static void UpdateAll(){
            unInit = true; // make sure to load excel files at the start
            string[] tbls = new string[]{"shoyu", "kyoyu", "souzoku", "kanchi", "juzen", "kumi"};
            foreach(string tbl in tbls){
                Start(tbl);
            }
            exShoyu = null;
            exMain = null;

        }

        private static void Shoyu(ExIO ex)
        {
            ex.ChangeSheetByName("所有者");

            int startRow = 10;
            int currRow = startRow;
            string shocode;
            //var rec = new Data.Shoyu();
            List<string> list = new List<string>();
            while (Data.Util.IsNum(shocode = ex.GetString(currRow, 1)))
            {
                var rec = new Data.Shoyu();
                rec.ShoCode = shocode;
                rec.TokiName = ex.GetString(currRow, 2);
                rec.TokiJusho = ex.GetString(currRow, 3);
                rec.GenName = ex.GetString(currRow, 6);
                rec.GenJusho = ex.GetString(currRow, 7);
                rec.SeinenYMD = ex.GetString(currRow, 9);
                rec.Seibetu = ex.GetString(currRow, 10);
                rec.Furigana = ex.GetString(currRow, 24);
                rec.MeiboPirnt = ex.GetString(currRow, 26);
                rec.Telephone = ex.GetString(currRow, 30).Replace("\n","&&");
                list.Add(rec.GetValues());

                //Debug.WriteLine($"{rec.ShoCode}:{rec.TokiName}:{rec.TokiJusho}:{rec.GenName}:{rec.GenJusho}:{rec.Furigana}");
                currRow++;
            }
            Dapper.SqliteRW.BulkInsert("Shoyu", list);
        }

        private static void Kyoyu(ExIO ex)
        {
            ex.ChangeSheetByName("共有者");

            int startRow = 10;
            int currRow = startRow;
            string shocode;
            var rec = new Data.Kyoyu();
            List<string> list = new List<string>();
            while (Data.Util.IsNum(shocode = ex.GetString(currRow, 1)))
            {
                rec = new Data.Kyoyu();
                rec.Key = KyoyuKey(ex, currRow);
                rec.ShoCode = shocode;
                rec.TokiName = ex.GetString(currRow, 6);
                rec.TokiJusho = ex.GetString(currRow, 7);
                rec.Bunbo = ex.GetString(currRow, 8);
                rec.Bunshi = ex.GetString(currRow, 9);
                rec.Bojin = ex.GetString(currRow, 10);
                rec.SeinenYMD = ex.GetString(currRow, 17);
                rec.Seibetu = ex.GetString(currRow, 18);
                rec.Furigana = ex.GetString(currRow, 19);
                rec.Daihyo = ex.GetString(currRow, 20);
                rec.GenName = ex.GetString(currRow, 21);
                rec.GenJusho = ex.GetString(currRow, 22);

                list.Add(rec.GetValues());

                currRow++;
            }
            Dapper.SqliteRW.BulkInsert("Kyoyu", list);
        }

        private static string KyoyuKey(ExIO ex, int currRow)
        {
            var key = "";
            key = ex.GetString(currRow, 1);
            key += ex.GetString(currRow, 2).PadLeft(2, '0');
            key += ex.GetString(currRow, 3).PadLeft(2, '0');
            key += ex.GetString(currRow, 4).PadLeft(2, '0');
            key += ex.GetString(currRow, 5).PadLeft(2, '0');
            return key;
        }

        private static void Souzoku(ExIO ex)
        {
            ex.ChangeSheetByName("相続人");

            int startRow = 10;
            int currRow = startRow;
            string shocode;
            Data.Souzoku rec;
            List<string> list = new List<string>();
            while (Data.Util.IsNum(shocode = ex.GetString(currRow, 1)))
            {
                rec = new Souzoku();
                rec.Key = KyoyuKey(ex, currRow);
                rec.ShoCode = shocode;
                rec.Shimei = ex.GetString(currRow, 6);
                rec.Jusho = ex.GetString(currRow, 7);
                rec.SeinenYMD = ex.GetString(currRow, 17);
                rec.Seibetu = ex.GetString(currRow, 18);
                rec.Furigana = ex.GetString(currRow, 19);
                rec.Daihyo = ex.GetString(currRow, 20);
                rec.Tel = ex.GetString(currRow, 30);

                list.Add(rec.GetValues());

                currRow++;
            }
            Dapper.SqliteRW.BulkInsert("Souzoku", list);
        }

        private static void Kanchi(ExIO ex)
        {
            ex.ChangeSheetByName("整理後データ");
            var dic = new Data.DicKanchi().dic;
            //var hoDic = new Data.DicHoryu().dic;
            List<Data.Kaishi> kaishiList = new List<Kaishi>();
            List<Data.Horyu> horyuList = new List<Horyu>();
            int startRow = 10;
            int currRow = startRow;
            int sokochiMax = 39;
            string bl = "";
            string lot = "";
            string choaza = "";
            string chiban = "";
            List<string> list = new List<string>();
            //TukehoDwg tkDwg = new TukehoDwg();
            while (Data.Util.IsNum(bl = ex.GetString(currRow, 1)))
            {
                var rec = new Data.Kanchi();
                lot = ex.GetString(currRow, 2);
                rec.KanCode = Data.Util.BL2code(bl, lot);
                rec.ShoCode = ex.GetString(currRow, 4);
                rec.Menseki = ex.GetString(currRow, 3);
                rec.HeibeiSisu = ex.GetInt(currRow, 5);
                rec.Memo = ex.GetString(currRow, 6);
                rec.HyoteiSisu = ex.GetInt(currRow, 12);

                //底地の編集
                Func<int, ExIO, string> f = (max, ex_) =>
                {
                    string fudeCode = "";
                    int col_ini = 60;
                    int col = col_ini;
                    int cnt = 0;
                    List<string> fList = new List<string>();
                    while (cnt <= max)
                    {
                        choaza = ex_.GetString(currRow, col);
                        chiban = ex_.GetString(currRow, col + 1);
                        fudeCode = Data.Util.Chiban2code(choaza, chiban);
                        if (choaza == "") { break; }
                        fList.Add(fudeCode);
                        cnt++;
                        col = col_ini + cnt * 2;

                    }
                    if (fList.Count == 0) { return "*NoData*"; }
                    fList.Sort();
                    return string.Join("/", fList);
                };
                rec.Sokochi = f(sokochiMax, ex);

                int kaishiStartCol = 30;
                int kaishiMax = 5;
                int kaishiCols = 5;
                Action<ExIO> a_kaishi = (ExIO ex_) =>
                {
                    string kDate;
                    int col;
                    for (int i = 0; i < kaishiMax; i++)
                    {
                        col = kaishiStartCol + kaishiCols * i;
                        kDate = ex_.GetString(currRow, col);
                        if (kDate != "")
                        {
                            var kai = new Data.Kaishi();
                            kai.KanCode = rec.KanCode;
                            kai.Renban = (i + 1).ToString("00");
                            kai.Id = kai.KanCode + kai.Renban;
                            kai.KaishiDate = kDate;
                            kai.Hatuban = ex_.GetString(currRow, ++col);
                            kai.KoryokuDate = ex_.GetString(currRow, ++col);
                            kai.Menseki = ex_.GetString(currRow, ++col);
                            kai.Comment = ex_.GetString(currRow, ++col);

                            kaishiList.Add(kai);
                        }

                    }
                };
                a_kaishi(ex);


                //dic.Add(rec.KanCode, rec);
                list.Add(rec.GetValues());

                if (CodeTables.IsHoryuchi(rec.ShoCode))
                {
                    var hoRec = new Data.Horyu();
                    hoRec.KanCode = rec.KanCode;
                    hoRec.Shubetu = ex.GetString(currRow, 7);
                    hoRec.Nendo = ex.GetString(currRow, 8);
                    hoRec.Kakaku = ex.GetString(currRow, 9);
                    hoRec.ShoCode = ex.GetString(currRow, 10);
                    hoRec.Comment = ex.GetString(currRow, 11);
                    //hoRec.TukeKancode = tkDwg.getTukeKancode(rec.KanCode);
                    hoRec.TukeKancode = "";

                    horyuList.Add(hoRec);

                }

                //Debug.WriteLine(rec.GetValues());

                currRow++;
            }
            Dapper.SqliteRW.BulkInsert("Kanchi", list);
            list.Clear();
            foreach(Kaishi k in kaishiList)
            {
                list.Add(k.GetValues());
            }
            Dapper.SqliteRW.BulkInsert("Kaishi", list);
            list.Clear();
            foreach(Horyu h in horyuList)
            {
                list.Add(h.GetValues());
            }
            Dapper.SqliteRW.BulkInsert("Horyu", list);
        }

        private static void Juzen(ExIO ex)
        {
            ex.ChangeSheetByName("従前宅地データ");
            //var dic = new Data.DicJuzen().dic;
            int startRow = 10;
            int currRow = startRow;
            string choaza = "";
            string chiban = "";
            string fudeCode = "";
            List<string> list = new List<string>();
            while (Data.Util.IsNum(choaza = ex.GetString(currRow, 1)))
            {
                chiban = ex.GetString(currRow, 2);
                fudeCode = Data.Util.Chiban2code(choaza, chiban);
                var rec = new Data.Juzen();
                rec.FudeCode = fudeCode;
                rec.ChimokuCD = ex.GetString(currRow, 6);
                rec.ShoCode = ex.GetString(currRow, 8);
                rec.TokiMen = ex.GetString(currRow, 7);
                rec.KijunMen = ex.GetString(currRow, 9);
                rec.HeibeiSisu = Int32.Parse( ex.GetString(currRow, 13));
                //dic.Add(rec.FudeCode, rec);
                list.Add(rec.GetValues());

                
                //Debug.WriteLine($"zen:{rec.FudeCode}:{rec.TokiMen}:{rec.HeibeiSisu}");

                currRow++;
            }
            Dapper.SqliteRW.BulkInsert("Juzen", list);
        }

        private static void Kumi(ExIO ex)
        {
            ex.ChangeSheetByName("組合せデータ");
            //var dic = new Data.DicKumi().dic;
            int startRow = 10;
            int currRow = startRow;
            string shoCode = "";
            string bl = "";
            string lot = "";
            string choaza = "";
            string chiban = "";
            string fudeCode = "";
            string kanCode = "";
            string key = "";
            string seq = "";
            string preKey = "";
            string shiteiDate = "";
            string memo = "";
            bool flgFirst = true;
            Data.Kumi kumi = new Data.Kumi();
            List<string> list = new List<string>();
            while (Data.Util.IsNum(shoCode = ex.GetString(currRow, 2)))
            {
                seq = ex.GetString(currRow, 3);
                if(seq.Length == 1)
                {
                    seq = "0" + seq;
                }
                key = shoCode + seq;
                if(preKey != key)
                {
                    if(flgFirst == true)
                    {
                        flgFirst = false;
                    }
                    else
                    {
                        list.Add(kumi.GetValues());
                        //dic.Add(preKey, kumi);
                        Debug.WriteLine($"<{preKey}>{kumi.Key}:{kumi.Fcodes}:{kumi.Kanchis}:{kumi.Shitei}:{kumi.Memo}");
                        kumi = new Data.Kumi();
                    }
                    preKey = key;
                    kumi.Key = key;
                    kumi.ShoCode = shoCode;
                }

                choaza = ex.GetString(currRow, 5);
                chiban = ex.GetString(currRow, 6);
                fudeCode = Data.Util.Chiban2code(choaza, chiban);
                if (fudeCode != "")
                {
                    if (kumi.Fcodes == "")
                    {
                        kumi.Fcodes = fudeCode;
                    }
                    else
                    {
                        kumi.Fcodes += ("/" + fudeCode);
                    }
                }
                bl = ex.GetString(currRow, 7);
                lot = ex.GetString(currRow, 8);
                kanCode = Data.Util.BL2code(bl, lot); 
                if(kanCode != "")
                {
                    if(kumi.Kanchis == "")
                    {
                        kumi.Kanchis = kanCode;
                    }
                    else
                    {
                        kumi.Kanchis += ("/" + kanCode);
                    }
                }
                shiteiDate = ex.GetString(currRow, 14);
                if(shiteiDate != "")
                {
                    var str = "";
                    str += (shiteiDate + "=");
                    str += (ex.GetString(currRow, 15) + "=");
                    str += ex.GetString(currRow, 16);
                    if(kumi.Shitei != "")
                    {
                        kumi.Shitei += "/";
                        Debug.WriteLine($"Duplicate shitei error at {shoCode}");
                    }
                    kumi.Shitei += str;
                }
                memo = ex.GetString(currRow, 9); 
                if(memo != "")
                {
                    kumi.Memo += $"<{memo}>";
                }


                currRow++;
            }
            list.Add(kumi.GetValues());


            //Debug.WriteLine($"<{preKey}>{kumi.Key}:{kumi.Fcodes}:{kumi.Kanchis}:{kumi.Shitei.ShiteiDate}:{kumi.Memo}");
            //dic.Add(preKey, kumi);
            //Debug.WriteLine("***********************");
            Dapper.SqliteRW.BulkInsert("Kumi", list);

        }
    }
}
