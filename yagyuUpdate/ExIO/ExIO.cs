using System;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;



namespace yg_chohyo
{
/// <summary>
/// Excelファイルを読むためのクラス
/// </summary>
public partial class ExIO {
    private IFormulaEvaluator _eval = null;
    protected IWorkbook _book = null;
    protected ISheet _sheet = null;
    private Border _border;

    /// <summary>
    /// 新規Excelファイルを開く。
    /// </summary>
    public ExIO() {
        _book = WorkbookFactory.Create("newBook.xls");
        _sheet = _book.CreateSheet("Sheet1");
        _eval = _book.GetCreationHelper().CreateFormulaEvaluator();
    }

    /// <summary>
    /// Excelファイルを読み込む。
    /// </summary>
    /// <param name="path">Excelファイルのパス</param>
    public ExIO(string path) {
        Read(path);
    }

    /// <summary>
    /// Excelファイルを読み込む。
    /// </summary>
    /// <param name="path">Excelファイルのパス</param>
    public void Read(string path) {
        using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
            _book = WorkbookFactory.Create(fs, ImportOption.All);
        }
        _eval = _book.GetCreationHelper().CreateFormulaEvaluator();
        _sheet = _book.GetSheetAt(0);
        _border = new Border(_book);
    }

    /// <summary>
    /// Excelファイルを書き込む。
    /// </summary>
    /// <param name="path">Excelファイルのパス</param>
    public void Write(string path) {
        using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None)) {
            _book.Write(fs);
        }
    }

    /// <summary>
    /// 読み書きするシートを変更する。
    /// </summary>
    /// <param name="idx">シートの番号 0,1,2...</param>
    public void ChangeSheet(int idx) {
        _sheet = _book.GetSheetAt(idx);
        _y = -1;
        _row = null;
    }

    public void ChangeSheetByName(string name)
        {
        _sheet = _book.GetSheet(name);
        _y = -1;
        _row = null;

    }

    /// <summary>
    /// シートの行数
    /// </summary>
    public int NumRow {
        get {
            return _sheet.LastRowNum - 1;
        }
    }

    private int _y = -1;
    private IRow _row = null;
    /// <summary>
    /// ある行の列数
    /// </summary>
    /// <param name="y">何行目</param>
    public int Width(int y) {
        var row = _y == y ? _row : _sheet.GetRow(y);
        _y = y;
        _row = row;
        return row == null ? 0 : row.LastCellNum - 1;
    }

    /// <summary>
    /// セルを取得。なければnull
    /// </summary>
    /// <param name="y">行</param>
    /// <param name="x">列</param>
    public ICell GetCell(int y, int x) {
        var row = _y == y ? _row : _sheet.GetRow(y);
        _y = y;
        _row = row;
        return row == null ? null : row.GetCell(x);
    }

    public int GetInt(int y, int x)
        {
            var cell = GetCell(y, x);
            int num = (int) cell.NumericCellValue;
            return num;
        }




    /// <summary>
    /// セルの内容を文字列で取得。なければ""
    /// </summary>
    /// <param name="y">行</param>
    /// <param name="x">列</param>
    public string GetString(int y, int x) {
        var cell = GetCell(y, x);
        return CellToString(cell);
    }

    /// <summary>
    /// セルを取得。なければ新規に作成される。
    /// </summary>
    /// <param name="y">行</param>
    /// <param name="x">列</param>
    public ICell CreateCell(int y, int x) {
        var cell = GetCell(y, x);
        if (cell != null) return cell;
        _row = _sheet.GetRow(y) ?? _sheet.CreateRow(y);
        return _row.GetCell(x) ?? _row.CreateCell(x);
    }

    /// <summary>
    /// セルを評価する。
    /// </summary>
    public CellValue Evaluate(ICell cell) {
        return _eval.Evaluate(cell);
    }

    /// <summary>
    /// セルの内容を文字列にする。
    /// </summary>
    private string CellToString(ICell cell) {
        if (cell == null) {
            return "";
        }
        if (cell.CellType != CellType.Formula) {
            switch (cell.CellType) {
                case CellType.Blank: return "";
                case CellType.Boolean: return cell.BooleanCellValue.ToString();
                case CellType.Error: return cell.ErrorCellValue.ToString();
                case CellType.Numeric: return cell.NumericCellValue.ToString();
                case CellType.String: return cell.StringCellValue;
                default: return "";
            }
        }
        var value = Evaluate(cell);
        switch (value.CellType) {
            case CellType.Blank: return "";
            case CellType.Boolean: return value.BooleanValue.ToString();
            case CellType.Error: return value.ErrorValue.ToString();
            case CellType.Numeric: return value.NumberValue.ToString();
            case CellType.String: return value.StringValue;
            default: return "";
        }
    }

    public void EvaluateAll()
    {
            HSSFFormulaEvaluator.EvaluateAllFormulaCells(_book);
            //EvaluateAll();
    }
}


    
}