﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using yg_chohyo.Data;
using yg_chohyo.Dapper;
using System.Linq;
using ChkKasanezu;

namespace Sokochi
{
    class JuzenChk
    {
        /// <summary>
        /// 1.底地データ用区域図の地番突合せ
        /// 2.重ね図中の区域図の氏名チェック
        /// </summary>
        public JuzenChk()
        {
            Dictionary<string, bool> dicJuzen = new Dictionary<string, bool>();
            foreach(string fudecode in DapperJuzen.GetAllFcodes())
            {
                dicJuzen[fudecode] = false;
            }

            string juzenPath = "c:/yagyu/sokochi/juzenList.txt";
            //区域図からの底地リスト(s-jis)を読み込む
            string[] juzenData = ChkKasanezu.FileUtil.GetByLines(juzenPath);

            //従前公共用地辞書を作成
            Dictionary<string, Juzen> dicKokyo = new Dictionary<string, Juzen>();
            foreach(Juzen j in JuzenKokyo.getAll())
            {
                dicKokyo[j.FudeCode] = j;
            }


            foreach(string r in juzenData)
            {
                string wk = r.Trim('字').Replace(":","");
                string fcode = Util.Shozai2Fcode(wk);

                //区域図の従前地が管理データになければエラー出力
                if (!dicJuzen.ContainsKey(fcode))
                {
                    if (!dicKokyo.ContainsKey(fcode))
                    {
                        Debug.WriteLine($"not in kanri fcode={r}");
                    }
                }
                dicJuzen[fcode] = true;
            }

            foreach(var d in dicJuzen)
            {
                //区域図になかった筆を表示
                if (d.Value) { continue; }
                string shozai = Util.Fcode2shozai(d.Key).ToString().Replace(" ", "");
                Debug.WriteLine($"kanri juzen not in 区域図 :{shozai}");
            }

            //区域図の氏名チェック
            ChkShimei();
        }

        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 区域図の氏名をチェック
        /// </summary>
        void ChkShimei()
        {
            //組合せデータから[筆コード->所有者コード]の辞書
            Dictionary<string, string> dicFcode2Sho = yg_chohyo.Data.CodeTables.SetDicFcode2Sho();

            //初期化　地番の存在チェック用
            Dictionary<string, bool> dicFcodesExist = new Dictionary<string, bool>();
            foreach (var d in dicFcode2Sho)
            {
                dicFcodesExist[d.Key] = false;
            }

            //従前地データから筆コード、所有者コードの辞書を作成
            //Dictionary<string, string> dicFude2Shocode = new Dictionary<string, string>();
            //string[] codes;
            //foreach (string pair in yg_chohyo.Dapper.DapperJuzen.GetAllFcShoPair())
            //{
            //    codes = pair.Split(":");
            //    dicFude2Shocode[codes[0]] = codes[1];
            //}


            //氏名辞書
            Dictionary<string, string> dicShimei = yg_chohyo.Data.CodeTables.DicShoName;

            //区域図データ
            Dictionary<string, string> dicKuikiShimei = new Dictionary<string, string>();
            SetKuikiData(dicKuikiShimei);

            string kuikiShimei;
            string kanriShimei = "";
            string shocode;
            string shozai = "";
            foreach(var d in dicKuikiShimei.OrderBy(x => x.Key))
            {
                kuikiShimei = d.Value;
                kuikiShimei = kuikiShimei.Replace("　", "");
                if (dicFcode2Sho.ContainsKey(d.Key))
                {
                    shocode = dicFcode2Sho[d.Key];
                    dicFcodesExist[d.Key] = true;
                }
                else
                {
                    shocode = $"データ無";
                }
                if (dicShimei.ContainsKey(shocode))
                {
                    kanriShimei = dicShimei[shocode].Replace(" ", "").Replace("　","");
                }
                else
                {
                    kanriShimei = shocode;
                }
                if(kuikiShimei != kanriShimei)
                {
                    if(!CompareShimei.compPair(kanriShimei, kuikiShimei)){
                        shozai = Util.Fcode2shozai(d.Key).ToString().Replace(" ", "");
                        Debug.WriteLine($"{shozai}\tkuiki={kuikiShimei}:kanri={kanriShimei}");
                    }
                }
            }

            //区域図にない筆を出力
            Debug.WriteLine("****** 区域図にない筆");
            foreach(var d in dicFcodesExist.OrderBy(x => x.Key))
            {
                shozai = "";
                if (!d.Value)
                {
                    shozai = Util.Fcode2shozai(d.Key).ToString().Replace(" ","");
                    Debug.WriteLine(shozai);
                }
            }

        }

        /// <summary>
        ///区域図データを[筆コード->氏名]の辞書にセット
        /// </summary>
        void SetKuikiData(Dictionary<string,string> dicKuikiShimei)
        {
            string juShimeiPath = "c:/yagyu/sokochi/juShimei.txt";
            //s-jisのファイルを読み込む
            string[] juzenShimei = ChkKasanezu.FileUtil.GetByLines(juShimeiPath);

            string[] wk;
            string Fcode;
            string Fcode2;
            string Shimei;
            //区域図データを[筆コード->氏名]の辞書にセット（２パス）
            foreach (string r in juzenShimei)
            {
                wk = r.Split(":");

                //地番：氏名の組を先に処理するため、メガネはスキップ
                if (wk[0] == "megane") { continue; }
                Fcode = Util.Shozai2Fcode(wk[1].Trim('字'));
                if (string.IsNullOrEmpty(Fcode))
                {
                    Debug.WriteLine(r);
                }
                Shimei = wk[2];
                dicKuikiShimei[Fcode] = Shimei;
            }
            foreach (string r in juzenShimei)
            {
                wk = r.Split(":");
                //メガネ地を処理
                if (wk[0] == "name") { continue; }
                Fcode = Util.Shozai2Fcode(wk[1].Trim('字'));
                if (string.IsNullOrEmpty(Fcode))
                {
                    Debug.WriteLine(r);
                }
                Fcode2 = Util.Shozai2Fcode(wk[2].Trim('字'));
                if (string.IsNullOrEmpty(Fcode2))
                {
                    Debug.WriteLine(r);
                }
                //メガネ対象地の氏名をセット
                dicKuikiShimei[Fcode] = dicKuikiShimei[Fcode2];

            }


        }


    }

}
