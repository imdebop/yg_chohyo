﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace Sokochi
{
    /// <summary>
    /// 許容できる底地を判断
    /// </summary>
    class SokochiAllow
    {
        Dictionary<string, string> dicAllow = new Dictionary<string, string>();

        /// <summary>
        /// 重ね図の底地に許容される筆を加除する。
        /// </summary>
        public void ModifyDwgSoko(string kancode, ref List<string> fcodes)
        {
            if (dicAllow.ContainsKey(kancode))
            {
                string[] ar = dicAllow[kancode].Split(":");

                switch(ar[0]){
                    case "add":
                        fcodes.AddRange(ar[1].Split("/"));
                        break;
                    case "del":
                        fcodes = fcodes.Except(ar[1].Split("/")).ToList();
                        break;
                }
            }
        }


        public SokochiAllow(string allowText)
        {
            string[] wkArr;
            string[] arKanri = new string[0];
            string[] arDwg = new string[0];
            string kancode = "";

            var regKancode = new Regex(@":(?<kancd>\d+)-");

            string[] allowData = ChkKasanezu.FileUtil.GetByLines(allowText);
            int i;
            int rem;
            bool allow = false;
            for (i = 0; i < allowData.Length; i++)
            {
                rem = i % 3;
                switch (rem)
                {
                    case 0:
                        kancode = First_kancode(allowData[i], regKancode);
                        break;
                    case 1:
                        arKanri = Second_kanri(allowData[i]);
                        break;
                    case 2:
                        arDwg = Third_dwg(allowData[i], ref allow);
                        break;
                }

                if(rem == 2 && allow)
                {
                    string sAllow = CreateExceptData(arKanri, arDwg);
                    dicAllow[kancode] = sAllow;
                }
            }
        }

        /// <summary>
        /// 許容の差分データを作成
        /// </summary>
        string CreateExceptData(string[] arkanri, string[] arDwg)
        {
            string[] arKanriExtra = arkanri.Except(arDwg).ToArray();
            string[] arDwgExtra = arDwg.Except(arkanri).ToArray();
            if(arKanriExtra.Length > 0)
            {
                return $"add:{string.Join("/", arKanriExtra)}";
            }
            else
            {
                return $"del:{string.Join("/", arDwgExtra)}";
            }
        }

        string First_kancode(string s, Regex regkan)
        {
            Match m = regkan.Match(s);
            return m.Groups["kancd"].Value;
        }

        string[] Second_kanri(string s)
        {
            string wk = s.Split("=")[1];
            if(wk.Substring(0,1) == "*")
            {
                return new string[0];
            }
            else
            {
                return wk.Split("\t")[0].Split("/");
            }

        }

        string[] Third_dwg(string s, ref bool allow)
        {
            allow = s.Substring(0, 2) == "許容" ? true : false;

            return s.Split("=")[1].Split("\t")[0].Split("/");
        }


    }
}
