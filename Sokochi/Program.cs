﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;

namespace Sokochi
{
    class Program
    {
        static void Main(string[] args)
        {

            if(args.Length == 1)
            {
                if(args[0] == "testJuzen")
                {
                    Debug.WriteLine("testJuzen");
                    JuzenChk ju = new JuzenChk();
                    //return;
                }
            }

            //チェック結果出力ファイル
            string SokochiChkPath = @"c:\yagyu_settings\checkResults\SokochiCheck.txt"; 
            string SokochiAllowPath = @"c:\yagyu_settings\checkResults\Sokochi_allow.txt"; 
            SokochiAllow skAllow = new SokochiAllow(SokochiAllowPath);

            //管理データの底地辞書を作成
            Dictionary<string, string> DicKan2sokochi = new Dictionary<string, string>();
            dicKan2sokochi_set(DicKan2sokochi);

            //管理データとsimaデータの突合せ
            Dictionary<string, bool> dicKanriKancode = new Dictionary<string, bool>();
            foreach(string kcode in yg_chohyo.Dapper.DapperKanchi.GetAll().Select(x => x.KanCode))
            {   //管理データの換地コードをセット
                dicKanriKancode[kcode] = false;
            }

            //入力データ
            string SokochiTextPath = @"c:\yagyu\sokochi\sokochi.txt";
            string[] sokoLines = ChkKasanezu.FileUtil.GetByLines(SokochiTextPath);

            List<string> reclst = new List<string>();

            string kancode = "";
            string fcode = "";
            string dwgSoko;
            string kanriSoko;
            string kakuchi;
            string keisen = new string('-', 100);
            List<string> resList = new List<string>();
            List<string> fcodes = new List<string>();

            //重ね図の底地データをセット
            foreach(string r in sokoLines)
            {
                reclst = r.Split("&").ToList();
                kancode = yg_chohyo.Data.Util.BL2code(reclst[0]);
                dicKanriKancode[kancode] = true;
                for(int i = 1; i < reclst.Count(); i++)
                {
                    fcode = yg_chohyo.Data.Util.Shozai2Fcode(reclst[i].TrimStart('字').Replace(":", "").Replace("*",""));

                    if (string.IsNullOrEmpty(fcode))
                    {
                        Debug.WriteLine(r);
                    }

                    fcodes.Add(fcode);
                }

                skAllow.ModifyDwgSoko(kancode, ref fcodes);

                //Debug.WriteLine(kancode + ":" + string.Join("/", fcodes ));

                dwgSoko = string.Join('/', fcodes.OrderBy(x=>x));
                fcodes.Clear();

                if (!DicKan2sokochi.ContainsKey(kancode))
                {
                    kanriSoko = "no data";
                }
                else
                {
                    kanriSoko = DicKan2sokochi[kancode];
                }

                if (kanriSoko != dwgSoko)
                {
                    kakuchi = yg_chohyo.Data.Util.Kancode2B_L_str(kancode);
                    resList.Add($"[{kakuchi}] :{kancode}" + keisen);
                    resList.Add($"kanri={kanriSoko}");
                    resList.Add($"dwg  ={dwgSoko}");
                    //Debug.WriteLine(kancode);
                    //Debug.WriteLine($"kanri={kanriSoko}");
                    //Debug.WriteLine($"dwg  ={dwgSoko}");
                }

            }

            File.WriteAllLines(SokochiChkPath, resList);
            
            //底地データの不足分を表示
            foreach(var r in dicKanriKancode.OrderBy(x=>x.Key))
            {
                if (!r.Value)
                {
                    string bk = yg_chohyo.Data.Util.Kancode2B_L_str(r.Key);
                    bk = bk.Replace(" ", "").Replace("L", "");
                    Debug.WriteLine($"missing kancode in sokochi={r.Key}:{bk}");
                }
            }

        }

        /// <summary>
        /// 管理データの換地＋底地データの辞書を作成
        /// </summary>
        static void dicKan2sokochi_set(Dictionary<string, string> dk2s)
        {
            string soko;
            List<string> sokoL;
            foreach(yg_chohyo.Data.Kanchi kan in yg_chohyo.Dapper.DapperKanchi.GetAll())
            {
                soko = kan.Sokochi;
                sokoL = soko.Split("/").ToList();
                soko = string.Join('/', sokoL.OrderBy(x => x));
                dk2s[kan.KanCode] = soko;
            }    
        }


    }
}
