﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Data.SQLite;

namespace Sokochi
{
    class Dapper
    {
    }

    class DapperRead
    {
        public static SQLiteConnection GetConnection()
        {
            var config = new SQLiteConnectionStringBuilder()
            {
                DataSource = @"c:/yagyu_settings/errorChk.sqlite"
            };
            return new SQLiteConnection(config.ToString());
        }

        /// <summary>
        /// 全「エラー許可レコード」を返す
        /// </summary>
        public static IEnumerable<SokochiErr> sokochiErrs()
        {
            using(var conn = GetConnection())
            {
                conn.Open();
                return conn.Query<SokochiErr>("select * from SokochiErr");
            }
        }


    }

    /// <summary>
    /// 正しいとするエラーレコード
    /// </summary>
    class SokochiErr
    {
        public string KanchiCode { get; set; }
        public string CaseAllow { get; set; }

    }
}
