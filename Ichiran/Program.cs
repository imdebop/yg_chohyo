﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using yg_chohyo;
using yg_chohyo.Data;
using yg_chohyo.Dapper;
using yg_chohyo.Repo;
using System.Linq;
using NPOI.SS.UserModel;

namespace Ichiran
{
    class Program
    {
        private static yg_chohyo.ExIO ex;
        public static Dictionary<string, KanJuRec> DicKan2Ju = new Dictionary<string, KanJuRec>();
        public static Dictionary<string, string> DicShimei = yg_chohyo.Data.CodeTables.DicShoName;

        static void Main(string[] args)
        {
            //２ﾊﾟｽで換地－＞従前地の組み合わせを作成
            string[] kanCodes;
            string[] juCodes;
            RepoKanchi repoKan;
            RepoJuzen repoju;
            foreach(Kumi kumi in DapperKumi.GetAll())
            {
                Debug.WriteLine(kumi.Key);

                //キー（換地コード)を準備
                kanCodes = kumi.Kanchis.Split("/");
                foreach(string kanCode in kanCodes)
                {
                    if (!(yg_chohyo.Data.Util.IsNum(kanCode))){ continue; }
                    RepoKanchi rpKan = RepoKanchiEdit.Get(kanCode);

                    if (DicKan2Ju.ContainsKey(kanCode))
                    {
                        Debug.WriteLine($"duplicate kan code={kanCode}");
                    }
                    else
                    {
                        KanJuRec kjr = new KanJuRec();
                        kjr.kumiKey = kumi.Key;
                        kjr.KanCode = kanCode;
                        kjr.ShoCode = kumi.ShoCode;
                        kjr.Shimei = Program.DicShimei[kjr.ShoCode];
                        kjr.IchiKanRec = new IchiKanRec(rpKan);
                        kjr.BunkatuKancode = kanCodes.ToList();
                        kjr.BunkatuKancode.Remove(kanCode);

                        foreach(string fcode in kumi.Fcodes.Split("/"))
                        {
                            RepoJuzen ju = RepoJuzenEdit.Get(fcode);
                            IchiJuRec iju = new IchiJuRec(ju);
                            kjr.ichiJuRecs.Add(iju);
                        }
                        DicKan2Ju[kanCode] = kjr;
                    }
                }
            }
            string fname = "街区・画地・従前一覧_様式";
            string fpath = $"c:/yagyu_settings/{fname}.xls";
            ex = new ExIO(fpath);

            editIchiran(ex, DicKan2Ju);

            string ofpath = $"c:/yagyu_settings/{"街区・画地・従前一覧out"}.xls";
            ex.Write(ofpath);



            Console.WriteLine("Hello World!");
        }

        private static void editIchiran(ExIO ex, Dictionary<string, KanJuRec> kjRecs)
        {
            int row = 6;
            int numJuzen = 0;
            foreach( var kjrec in kjRecs.OrderBy((x) => x.Key))
            {
                string bunkatu = "";
                IchiKanRec kr = kjrec.Value.IchiKanRec;
                List<IchiJuRec> ijs = kjrec.Value.ichiJuRecs;
                ex.SetCellStr(kr.Block, row, 2);
                ex.SetCellStr(kr.Lot, row, 3);
                ex.SetCellDoubule(kr.Menseki, row, 4);
                ex.SetCellDoubule(kr.HeibeiSisu, row, 5);
                ex.SetCellDoubule(kr.HyokaSisu, row, 6);
                ex.SetCellStr(kjrec.Value.ShoCode, row, 7);
                ex.SetCellStr(kjrec.Value.Shimei, row, 8);
                foreach(string kancode in kjrec.Value.BunkatuKancode)
                {
                    if(bunkatu != "") { bunkatu += ","; }
                    bunkatu += yg_chohyo.Data.Util.Kancode2B_L_str(kancode);
                }
                ex.SetCellStr(bunkatu, row, 14);

                numJuzen = ijs.Count();
                foreach(IchiJuRec ju in ijs)
                {
                    ex.SetCellStr(ju.Choaza, row, 9);
                    ex.SetCellStr(ju.Chiban, row, 10);
                    ex.SetCellStr(ju.Chimoku, row, 11);
                    ex.SetCellDoubule(ju.Menseki, row, 12);
                    ex.SetCellDoubule(ju.HyokaSisu, row, 13);
                    //if(numJuzen > 1) { ExKeisenEraseTop(ex, row); }
                    if(numJuzen > 1)
                    {
                        numJuzen = 0;
                    }
                    else if(numJuzen == 0)
                    {
                        ex.SetCellStr("***", row, 2);
                        ex.SetCellStr("*** 〃 ***", row, 8);
                    }
                    row++;
                }
            }
        }
        private static void ExKeisenEraseTop(ExIO ex, int row)
        {
            ex.WriteBorder(row, 1, 8, "AB");
        }

    }


    class KanJuRec
    {
        public string kumiKey { get; set; }
        public string KanCode { get; set; }
        public string ShoCode { get; set; }
        public string Shimei { get; set; }
        public string Memo { get; set; }
        public IchiKanRec IchiKanRec { get; set; }
        public List<IchiJuRec> ichiJuRecs { get; set; } = new List<IchiJuRec>();
        public List<string> BunkatuKancode { get; set; } = new List<string>();
    }

    class IchiKanRec
    {
        public string Block { get; set; } = "***";
        public string Lot { get; set; } = "***";
        public float Menseki { get; set; }
        public int HeibeiSisu { get; set; }
        public int HyokaSisu { get; set; }

        public IchiKanRec( RepoKanchi rpkan)
        {
            if(rpkan is null)
            {
                return;
            }
            Block = rpkan.Block;
            Lot = rpkan.Lot;
            Menseki = float.Parse(rpkan.Chiseki);
            HeibeiSisu = rpkan.Kanchi.HeibeiSisu;
            HyokaSisu = rpkan.Kanchi.HyoteiSisu;
        }


    }

    class IchiJuRec
    {
        public string Choaza { get; set; } = "***";
        public string Chiban { get; set; } = "***";
        public string Chimoku { get; set; }
        public float Menseki { get; set; }
        public int HyokaSisu { get; set; }

        public IchiJuRec(RepoJuzen rpj)
        {
            if(rpj is null) { return; }
            Choaza = rpj.ChoAza;
            Chiban = rpj.Chiban;
            Chimoku = rpj.Chimoku;
            Menseki = float.Parse(rpj.TokiChiseki);
            HyokaSisu = rpj.HyoteiSisu;
        }

    }
}
