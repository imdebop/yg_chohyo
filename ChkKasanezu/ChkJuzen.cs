﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace ChkKasanezu
{
    static class ChkJuzen
    {
        public static void DoCheck(Dictionary<string, ChkKumiKanDwg> chkKumiKan)
        {
            string ku;//筆コード比較作業用
            string ka;
            string[] wkarr;
            string[] wfkasane;
            foreach(ChkKumiKanDwg ckkd in chkKumiKan.Values.OrderBy(x => x.KanCode))
            {
                Func<string, string> sortFcodes = (fudes) =>
                 {
                    if(fudes is null) { fudes = ""; }
                    wkarr = fudes.Split("/");
                    Array.Sort(wkarr);
                    return String.Join("/", wkarr);
                 };
                ku = sortFcodes(ckkd.FcodesKumi);
                ka = sortFcodes(ckkd.FcodesDwg);
                if(ku != ka)
                {
                    Debug.WriteLine($"従前地不一致 {ckkd.KanCode}:kumi{ku} dwg{ka}");                    
                }
            }

        }
    }

    /// <summary>
    /// 組合せデータの従前地チェック用レコード
    /// </summary>
    class ChkJuzen2KanRec
    {
        public string FudeCode { get; set; }
        //組合せデータの換地リスト
        public List<string> KanchiListKumi { get; set; } = new List<string>();
        //重ね図データの換地リスト
        public List<string> KanchiListKasane { get; set; } = new List<string>();
        //従前地データとの存在チェックフラグ
        public bool Ju2KumiOK { get; set; } = false;
    }

}
