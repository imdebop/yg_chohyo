﻿using NPOI.HSSF.Record.PivotTable;
using Org.BouncyCastle.Bcpg.OpenPgp;
using System;
using System.Collections.Generic;
using System.Text;
using yg_chohyo;
using System.Diagnostics;
using NPOI.OpenXmlFormats.Dml;
using System.Linq;

namespace ChkKasanezu
{
    /// <summary>
    /// 管理データと重ね図の整合チェックのメインレコード
    /// </summary>
    class ChkKumiKanDwg
    {
        public string KanCode { get; set; }
        public string Menseki { get; set; }
        public string ShocodeKumi { get; set; } = "NA";
        public string ShocodeKanchi { get; set; } = "NA";
        public string ShimeiKumi { get; set; }
        public string ShimeiDwg { get; set; }
        public string KumiCode { get; set; }
        public string FcodesKumi { get; set; }
        public string FcodesDwg { get; set; }

    }

    class ChkKumiKanDwg_SetData
    {
        /// <summary>
        /// 組合せデータをセット
        /// </summary>
        public static void Set(Dictionary<string,ChkKumiKanDwg> dic, yg_chohyo.Data.Kumi kumi)
        {
            string[] kanList = kumi.Kanchis.Split("/");
            if(String.IsNullOrWhiteSpace(kumi.Kanchis) || !yg_chohyo.Data.Util.IsNum(kumi.Kanchis.Substring(0,1))){
                Debug.WriteLine($"kumiData set 不交付等={kumi.Key}:{kumi.Fcodes}:{kumi.Memo}");
                return; 
            }//不交付を除外


            foreach(string kancode in kanList)
            {
                ChkKumiKanDwg rec = new ChkKumiKanDwg();
                rec.KanCode = kancode;
                rec.ShocodeKumi = kumi.ShoCode;
                rec.ShimeiKumi = ChkKanchi.DicShoName[kumi.ShoCode].Replace("　", "").Replace(" ","").Replace("(亡)", "");
                rec.KumiCode = kumi.Key;
                rec.FcodesKumi = kumi.Fcodes;
                dic[kancode] = rec;
            }

        }

        /// <summary>
        /// 換地データをセット
        /// </summary>
        public static void Set(Dictionary<string,ChkKumiKanDwg> dic, yg_chohyo.Data.Kanchi kan)
        {
            if (dic.ContainsKey(kan.KanCode))
            {
                ChkKumiKanDwg rec = dic[kan.KanCode];
                rec.ShocodeKanchi = kan.ShoCode;
                rec.Menseki = kan.Menseki;
            }
            else if(kan.ShoCode == "5000")
            {
                ChkKumiKanDwg rec = new ChkKumiKanDwg();
                rec.KanCode = kan.KanCode;
                rec.Menseki = kan.Menseki;
                rec.ShimeiKumi = "horyu";
                dic[kan.KanCode] = rec;
            }
            else
            {
                Debug.WriteLine($"no data in ChkKumiKanDwg for kanchi {kan.KanCode}:{kan.ShoCode}");
            }

        }

        /// <summary>
        /// 重ね図データをセット
        /// </summary>
        public static void Set(Dictionary<string, ChkKumiKanDwg> dic, KasaneAssemble kas)
        {
            foreach( AssembleRec ar in kas.dicKasane.Values.OrderBy(x => x.KanchiCode))
            {
                if (!(dic.ContainsKey(ar.KanchiCode))) {
                    Debug.WriteLine($"** kanri no data for kasane **:{ar.KanchiCode}");
                    continue; 
                }
                dic[ar.KanchiCode].ShimeiDwg = ar.ShimeiKasane;
                dic[ar.KanchiCode].FcodesDwg = String.Join("/", ar.Fcodes.Select(x => x.Split(":")[0]));
                //Debug.WriteLine(ar.ShimeiKasane);
            }
        }

    }

    /// <summary>
    /// 重ね図の換地データをチェック用にまとめたレコード
    /// </summary>
    class AssembleRec
    {
        public string KanchiCode { get; set; }
        public string ShoCode { get; set; }
        //public string ShimeiKumi { get; set; }
        public string ShimeiKasane { get; set; } = "";
        public List<string> Fcodes { get; set; } = new List<string>();
        public string Menseki { get; set; }
        public string Kabusoku { get; set; }
        public string SokuryoMen { get; set; }
        public string Target { get; set; } = "";
    }



}
