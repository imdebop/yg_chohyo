﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace ChkKasanezu
{
    public static class CompareShimei
    {
        public static bool compPair(string s1, string s2)
        {
            string ws1 = Replace(s1);
            string ws2 = Replace(s2);
            if(ws1 == ws2) { return true; }
            return Ignore($"{ws1}:{ws2}");
        }


        public static string Replace(string s)
        {
            string wk_s;
            wk_s = s.Replace("　", "");
            wk_s = wk_s.Replace("(亡)", "");
            wk_s = wk_s.Replace("(株)", "株式会社");
            wk_s = wk_s.Replace("㈱", "株式会社");
            wk_s = wk_s.Replace("(有)", "有限会社");
            wk_s = wk_s.Replace("㈲", "有限会社");

            return wk_s;
        }

        public static bool Ignore(string s){
            foreach(string ws in StrIgnore)
            {
                if (s == ws)
                {
                    return true;
                }
            }

            return false;
        }

        private static string[] StrIgnore = new string[]
        {
            "沼田美子:沼田美子",
            "株式会社チュウチク:株式会社ﾁｭｳﾁｸ",
            "ジャンスクパッチャリン:ｼﾞｬﾝｽｸﾊﾟｯﾁｬﾘﾝ",
            "伊藤アレシャンドレ:伊藤ｱﾚｼｬﾝﾄﾞﾚ",
            "サバンガンニコアバド:ｻﾊﾞﾝｶﾞﾝﾆｺｱﾊﾞﾄ",
            "株式会社ウッドフレンズ:株式会社ｳｯﾄﾞﾌﾚﾝｽﾞ",
            "遠藤譲一:遠藤讓一",
            "バンドイジェイリサルカリノ外1名:バンドイジェイ リサルカリノ外1名",
            @"靳業林:\U+9773業林",
            "小野司:小野広司",
            "加藤久恵外1名:加藤久惠外1名",
            "伊東美恵子外1名:伊東美惠子外1名",
            "サントスビトル:ｻﾝﾄｽﾋﾞﾄﾙ",
            "有限会社マル:（有）マル隆",
            "双元気:双原元気",
            "關康人外1名:関康人外1名",
            "高津貴一外1名:髙津貴一外1名",
            "田口國廣:田口国広",

        };


    }
}
