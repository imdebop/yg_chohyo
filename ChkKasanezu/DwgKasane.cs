﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using yg_chohyo.Data;

namespace ChkKasanezu
{
    /// <summary>
    /// 重ね図から抽出した個別データの種類別レコード
    /// </summary>
    class DwgKasaneRec
    {
        public string LotHandle { get; set; }//ロット番号のハンドル：詳細データはこれに紐づく
        public string ObjHandle { get; set; }//個々の詳細データのハンドル
        public string KanchiCode { get; set; }
        public string Layer { get; set; }
        public string Data { get; set; }//ロット、氏名、従前地、地積等
        public string Target { get; set; }//付け保、メガネ地の相手地
        public string Ycoord { get; set; }//Y座標
        public Shubetu shubetu { get; set; }//dataの種別
        public enum Shubetu
        {
            Lot,
            Eda,//枝番があって分かれている場合
            Fcode,//従前地の場合:layer = go_abr
            Shimei,// layer = go_shoyu
            Menseki,// 通常の数字 layer = kanchi_men
            Kabusoku,// 数字の先頭が"+,-"
            SokuryoMen,//数字が()付き
            HoryuMark,//保留地のマーク
            Tukeho,//付け保
            Megane,//メガネマーク
            Error
        }

    }

    /// <summary>
    /// 重ね図のデータの編集メソッドのクラス
    /// </summary>
    class DwgKasaneInit
    {
        public static MainWindow MainWin = (Application.Current.MainWindow as MainWindow);

        public static DwgKasaneRec Set(string rec)
        {
            //rec is like "7B489/7B48E:07001000/go_abr/東里29-1a"
            DwgKasaneRec dkData = new DwgKasaneRec();
            string[] recs = rec.Split("/");
            dkData.LotHandle = recs[0];
            string[] wkArr = recs[1].Split(":");
            dkData.ObjHandle = wkArr[0];
            dkData.KanchiCode = wkArr[1];
            dkData.Layer = recs[2];
            string textData = recs[3];
            string target = recs[4];
            dkData.Ycoord = recs[5];
            string firstChar = "";
            switch (dkData.Layer)
            {
                case "lot":
                    if (target == "")
                    {
                        dkData.shubetu = DwgKasaneRec.Shubetu.Lot;
                        dkData.Data = Util.Kancode2B_L(dkData.KanchiCode).Lot;
                    }
                    else
                    {
                        dkData.shubetu = DwgKasaneRec.Shubetu.Tukeho;
                        dkData.Target = target;
                    }
                    break;
                case "go_abr"://従前地のコードをセット
                    dkData.shubetu = DwgKasaneRec.Shubetu.Fcode;
                    dkData.Data = Util.Shozai2Fcode(textData);
                    break;
                case "go_shoyu":
                    if (textData == "megane")
                    {
                        dkData.shubetu = DwgKasaneRec.Shubetu.Megane;
                        dkData.Target = target;//相手の換地コード
                    }
                    else
                    {
                        dkData.shubetu = DwgKasaneRec.Shubetu.Shimei;
                        dkData.Data = textData;//氏名
                    }
                    break;
                case "kanchi_men":
                    firstChar = textData.Substring(0, 1);
                    switch (firstChar)
                    {
                        case "+"://過不足地積
                        case "-":
                            dkData.shubetu = DwgKasaneRec.Shubetu.Kabusoku;
                            dkData.Data = textData;
                            break;
                        case "("://測量計算地積
                            dkData.shubetu = DwgKasaneRec.Shubetu.SokuryoMen;
                            dkData.Data = textData;
                            break;
                        case "h"://ho_mark
                            dkData.shubetu = DwgKasaneRec.Shubetu.HoryuMark;
                            dkData.Data = "";
                            break;
                        default://換地地積
                            dkData.shubetu = DwgKasaneRec.Shubetu.Menseki;
                            dkData.Data = textData;
                            break;
                    }
                    break;
                default:
                    MainWin.LogError($"dwg_error={rec}");
                    //MessageBox.Show($"error={rec}");
                    dkData.shubetu = DwgKasaneRec.Shubetu.Error;
                    dkData.Data = "";
                    break;
            }
            return dkData;
        }
    }



}

