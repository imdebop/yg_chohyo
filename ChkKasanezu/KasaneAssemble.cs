﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Diagnostics;

namespace ChkKasanezu
{
    /// <summary>
    /// 重ね図から換地別レコードを作り、後に管理データにマージされてチェック
    /// </summary>
    class KasaneAssemble
    {
        public static MainWindow MainWin = (Application.Current.MainWindow as MainWindow);
        public Dictionary<string, AssembleRec> dicKasane { get; set; } = new Dictionary<string, AssembleRec>();
        /// <summary>
        /// 重ね図の表示データを換地レコードにまとめる
        /// </summary>
        /// <param name="dkRec"></param>
        public KasaneAssemble(List<DwgKasaneRec> dkRec)
        {
            //分割表示された氏名をつなげるためのストレージ
            Dictionary<string, List<string>> dicShimeiBunkatu = new Dictionary<string, List<string>>();

            //２パスの１回目
            foreach (DwgKasaneRec r in dkRec)
            {
                //換地コード毎の枠を作成
                if (!dicKasane.ContainsKey(r.KanchiCode))
                {
                    dicKasane.Add(r.KanchiCode, new AssembleRec()
                    {
                        KanchiCode = r.KanchiCode
                    });
                    dicShimeiBunkatu.Add(r.KanchiCode, new List<string>());
                }

            }
            //２回目は編集
            foreach (DwgKasaneRec r in dkRec)
            {
                switch (r.shubetu)
                {
                    case DwgKasaneRec.Shubetu.Lot:
                    case DwgKasaneRec.Shubetu.Eda:
                        break;
                    case DwgKasaneRec.Shubetu.HoryuMark:
                        dicKasane[r.KanchiCode].ShimeiKasane = "horyu";
                        break;
                    case DwgKasaneRec.Shubetu.Fcode:
                        dicKasane[r.KanchiCode].Fcodes.Add(r.Data);
                        break;
                    case DwgKasaneRec.Shubetu.Shimei:
                        //dicKasane[r.KanchiCode].Sehimei = r.Data;
                        dicShimeiBunkatu[r.KanchiCode].Add($"{r.Ycoord}:{r.Data}");
                        break;
                    case DwgKasaneRec.Shubetu.Menseki:
                        dicKasane[r.KanchiCode].Menseki = r.Data;
                        break;
                    case DwgKasaneRec.Shubetu.Kabusoku:
                        dicKasane[r.KanchiCode].Kabusoku = r.Data;
                        break;
                    case DwgKasaneRec.Shubetu.SokuryoMen:
                        dicKasane[r.KanchiCode].SokuryoMen = r.Data;
                        break;
                    case DwgKasaneRec.Shubetu.Tukeho:
                        dicKasane[r.KanchiCode].ShimeiKasane = "horyu";
                        dicKasane[r.KanchiCode].Target = r.Target;
                        break;
                    case DwgKasaneRec.Shubetu.Megane:
                        dicKasane[r.KanchiCode].ShimeiKasane = "Megane";
                        dicKasane[r.KanchiCode].Target = r.Target;
                        break;
                    default:
                        //MessageBox.Show($"shubetu erro kancode={r.KanchiCode}");
                        MainWin.LogError($"shubetu erro kancode={r.KanchiCode}");
                        break;
                }
            }

            //分割表示の氏名を結合
            string wkstr;
            foreach (KeyValuePair<string, List<string>> shimeiBun in dicShimeiBunkatu)
            {
                wkstr = "";
                shimeiBun.Value.Sort();
                //shimeiBun.Value.Reverse();
                foreach (string s in shimeiBun.Value)
                {
                    wkstr += s.Split(":")[1];
                }
                if(dicKasane[shimeiBun.Key].ShimeiKasane != "")
                {
                    //"horyu"がセットされていればスキップ
                    continue;
                }
                dicKasane[shimeiBun.Key].ShimeiKasane = CompareShimei.Replace(wkstr);
                //Debug.WriteLine(shimeiBun.Key + wkstr);
            }
            //メガネ地の氏名を編集
            string targetKan = "";
            foreach(AssembleRec kas in dicKasane.Values)
            {
                if (kas.ShimeiKasane == "Megane") {
                    targetKan = kas.Target.Split(":")[1];
                    kas.ShimeiKasane = dicKasane[targetKan].ShimeiKasane;
                    Debug.WriteLine($"megane to ={targetKan}:{kas.ShimeiKasane}");
                }
            }
        }
    }




}
