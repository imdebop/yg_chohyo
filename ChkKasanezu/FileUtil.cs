﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows;

namespace ChkKasanezu
{
    public class FileUtil
    {
        public static string[] GetByLines(string path)
        {
            string[] lines;
            try
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

                lines = File.ReadAllLines(path, Encoding.GetEncoding("shift_jis"));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                lines = new string[0];
            }
            return lines;
        }
    }
}
