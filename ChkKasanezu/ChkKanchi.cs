﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using yg_chohyo.Repo;
using System.Text.RegularExpressions;
using yg_chohyo.Data;
using System.Windows;
using System.Linq;
using NPOI.HSSF.Record.PivotTable;
using yg_chohyo;
using System.IO;
using Microsoft.VisualBasic;
using NPOI.OpenXmlFormats.Dml;

namespace ChkKasanezu
{
    class ChkKanchi
    {
        public static MainWindow MainWin = (Application.Current.MainWindow as MainWindow);
        public static string DwgtextPath = @"c:\yagyu\sokochi\kanDataOut.txt";
        public static Dictionary<string, string> DicShoName = yg_chohyo.Data.CodeTables.DicShoName;

        public static void DoTest()
        {
            ///////////////////////////////////////////////////////////////////
            //比較用のメインデータ  ///////////////////////////////////////
            Dictionary<string, ChkKumiKanDwg> DicChkKumiKanDwg = new Dictionary<string, ChkKumiKanDwg>();
            //従前－＞換地のチェックデータ
            Dictionary<string, ChkJuzen2KanRec> DicJuzen2Kan = new Dictionary<string, ChkJuzen2KanRec>();

         //＊＊＊組合せデータをセット＊＊＊＊＊
            foreach(yg_chohyo.Data.Kumi kumi in yg_chohyo.Dapper.DapperKumi.GetAll())
            {
                ChkKumiKanDwg_SetData.Set(DicChkKumiKanDwg, kumi);
            }
         //＊＊＊換地データをセット＊＊＊＊＊
            foreach (yg_chohyo.Data.Kanchi kan in yg_chohyo.Dapper.DapperKanchi.GetAll())
            {
                ChkKumiKanDwg_SetData.Set(DicChkKumiKanDwg, kan);
            }

            //////////////////////////////////////////////////////////////
            //重ね図の換地データを整理
            string[] lines = FileUtil.GetByLines(DwgtextPath);
            List<DwgKasaneRec> kasaneData = new List<DwgKasaneRec>();
            foreach (string line in lines)
            {
                kasaneData.Add(DwgKasaneInit.Set(line));
            }
            //Dwgの図形を換地ごとにまとめる
            KasaneAssemble kasaneAssemble = new KasaneAssemble(kasaneData);
         //＊＊＊重ね図データをセット＊＊＊＊＊
            ChkKumiKanDwg_SetData.Set(DicChkKumiKanDwg, kasaneAssemble);

            //氏名をチェック
            foreach(ChkKumiKanDwg ck in DicChkKumiKanDwg.Values.OrderBy(x => x.KanCode))
            {
                string ws = "";
                if(ck.ShimeiKumi != ck.ShimeiDwg)
                {
                    ws = $"{ck.ShimeiKumi}:{ck.ShimeiDwg}";
                    if (!CompareShimei.Ignore(ws))
                    {
                        Debug.WriteLine($"shimei error = {ck.KanCode}:{ws}");
                    }
                }
            }


            ///////////////////////////////////////////////////////////////////
            //[換地コード：所有者コード]のペアを組合せデータと換地データで比較
            foreach (ChkKumiKanDwg ck in DicChkKumiKanDwg.Values.OrderBy(x => x.KanCode))
            {
                if (ck.ShocodeKumi != ck.ShocodeKanchi)
                {
                    string s = $"{ ck.KanCode }:{ ck.ShocodeKanchi}:{ck.ShocodeKumi}";
                    Debug.WriteLine($"shocode error = {s}");
                }
            }


            //////////////////////////////////////////////////////////////
            //チェック１：換地、地積が正しいか。 /////////////////////////////////
            //string ChisekiChkRoot= @"c:\yagyu_settings\checkResults\"; 
            string ChisekiChkRoot = PathInfo.GetPath("ChisekiChkRoot");
            string ChisekiChkPath = @$"{ChisekiChkRoot}chisekiCheck.txt"; //チェック結果
            List<string> ChisekiOutList = new List<string>();
            List<string> ShimeiOutList = new List<string>();
            Dictionary<string, float> dicKanMen = new Dictionary<string, float>();
            foreach(yg_chohyo.Data.Kanchi rk in yg_chohyo.Dapper.DapperKanchi.GetAll())
            {
                dicKanMen[rk.KanCode] = float.Parse(rk.Menseki);
            }
            float kanriMen=0;
            float dwgMen=0;
            string wkstr="";
            string wkLine = "";
            foreach(AssembleRec ckrec in kasaneAssemble.dicKasane.Values)
            {
                //地積に差異のあるレコードをパネルとファイルに出力
                if(ckrec.Menseki is null)
                {
                    dwgMen = 0;
                }
                else
                {
                    dwgMen = float.Parse(ckrec.Menseki);
                }
                if (dicKanMen.ContainsKey(ckrec.KanchiCode))
                {
                    kanriMen = dicKanMen[ckrec.KanchiCode];
                    wkstr = kanriMen.ToString("#####.#0");
                    if(dwgMen != kanriMen)
                    {
                        wkLine = $"{ckrec.KanchiCode}:dwgMen={dwgMen.ToString("#####.#0")}:kanriMen={kanriMen.ToString("#####.#0")}";
                        MainWin.LogChiseki(wkLine);
                        ChisekiOutList.Add(wkLine);
                    }
                }
                else
                {
                    wkLine = $"{ckrec.KanchiCode}:dwgMen={dwgMen.ToString("#####.#0")}:kanri no data";
                    MainWin.LogChiseki(wkLine);
                    ChisekiOutList.Add(wkLine);
                }
            }
            ChisekiOutList.Sort();
            File.WriteAllLines(ChisekiChkPath, ChisekiOutList);


            //付け保、メガネのデータ表示
            var wkdic = kasaneAssemble.dicKasane.OrderBy((x) => x.Key);
            var tukeList = new List<string>();
            foreach(var ckd in wkdic)
            {
                if(ckd.Value.Target != "")
                {
                    var s = $"{ckd.Value.KanchiCode}:{ckd.Value.Target}";
                    MainWin.LogTukeho( s );
                    tukeList.Add(s);
                }

            }

            string TukehoPath = @$"{ChisekiChkRoot}dwgTukeho.txt"; //付保・メガネデータのテキスト出力
            File.WriteAllLines(TukehoPath, tukeList);

            //チェック２：従前地が正しいか。
            ChkJuzen.DoCheck(DicChkKumiKanDwg);

        }
    }


}

