﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChkKasanezu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ChkKanchi.DoTest();
            //MessageBox.Show("hit return");
            //this.Close();
            this.btnClose.Focus();
        }

        /// <summary>
        /// 面積差のあるレコードを表示
        /// </summary>
        /// <param name="message"></param>
        public void LogChiseki(string message)
        {
            OutChiseki.Inlines.Add(message + "\n");
            ScrChiseki.ScrollToBottom();
        }

        /// <summary>
        /// 付け保・メガネのデータを表示
        /// </summary>
        /// <param name="message"></param>
        public void LogTukeho(string message)
        {
            OutputBlock.Inlines.Add(message + "\n");
            Scroller.ScrollToBottom();
        }

        public void LogError(string message)
        {
            OutputError.Inlines.Add(message + "\n");
            ScrolError.ScrollToBottom();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

}
